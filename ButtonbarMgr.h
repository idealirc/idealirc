#ifndef BUTTONBARMGR_H
#define BUTTONBARMGR_H

#include <IWin/IWin.h>
#include <QObject>
#include <QHash>
#include <QMdiSubWindow>
#include <IConnection/IConnection.h>
#include <QMenu>
#include <QToolBar>
#include <QHash>
#include <list>

class ButtonbarMgr : public QObject
{
	Q_OBJECT

	struct Button
	{
		explicit Button(){}
		Button(QAction* button_, QMdiSubWindow* parent);
		QMdiSubWindow* subwin{ nullptr };
		QAction* button{ nullptr };
		QMenu* menu{ nullptr };
		QAction* menuHead{ nullptr };
		QAction* menuSep{ nullptr };
		QAction* menuClose{ nullptr };
	};

	struct ButtonGroup
	{
		QList<Button>& operator[](int i) {
			switch (i) {
			case 0: return status;
			case 1: return channel;
			case 2: return privmsg;

			default: return blank;
			}
		}

		QList<Button> status;
		QList<Button> channel;
		QList<Button> privmsg;
		const int size = 3;

		QAction* rightSep;
		QList<Button> blank;
	};

	static int WindowTypeToGroupPos(const IWin* subwin);

public:
	explicit ButtonbarMgr(QToolBar *parent);
	void addButton(IWin* subwin, QMdiSubWindow* mdiwin);
	void delButton(IWin* subwin);
	void reloadButtonName(IWin* subwin, const QString& buttonText);
	void subwinActivated(IWin* prev, IWin* next);

private:
	void buttonBarContextMenu(const QPoint& pos);
	static int buttonPosition(QList<ButtonbarMgr::Button>& list, IWin* sw);
	ButtonGroup* tryFindGroup(IWin* subwin);
	QList<Button>& buttons(IWin* subwin);
	QAction* findNextNeighbour(IWin* subwin);
	Button* findButton(IWin* subwin);
	void deleteGroup(IWin* statuswin);
	QList<Button> m_others;
	QAction* m_others_rightSep;
	std::list<ButtonGroup> m_groups;
	QHash<IWin*,QAction*> m_winbtn;
	QToolBar& m_buttonBar;

signals:
	void changeWindow(QMdiSubWindow* mdiwin);
};

#endif // BUTTONBARMGR_H
