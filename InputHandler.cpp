/*
 * IdealIRC - Internet Relay Chat client
 * Copyright (C) 2019  Tom-Andre Barstad
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "InputHandler.h"
#include "IWin/IWin.h"
#include "ICommand.h"
#include "IConnection/IConnection.h"
#include "Commands.h"
#include "ConfigMgr.h"

InputHandler::InputHandler(IConnection* connection)
	: connection(connection)
	, cmdHndl(connection)
{

}

void InputHandler::parse(IWin& sender, const QString& text, bool handleCommand)
{
	if (text.isEmpty())
		return;

	if (text[0] == '/' && handleCommand)
		cmdHndl.parse(text);
	else {
		if (sender.getType() == IWin::Type::Status) {
			sender.print(PrintType::ProgramInfo, "You're not in a chat window!");
			return;
		}
		else {
			sockwrite(QStringLiteral("%1 %2 :%3").arg(Command::IRC::PRIVMSG).arg(sender.getButtonText()).arg(text));
			QString me = connection->getServer()->getMyNickname();
			if (ConfigMgr::instance().common("ShowModeInMessage") == "1") {
				IServer* server = connection->getServer();
				std::optional<IMember> member = server->getMemberByNickname(me);
				if (member) {
					QString modes = member->getChannelModes(sender.getButtonText());
					if (!modes.isEmpty()) {
						QString prefixes = server->toMemberPrefix(modes);
						QChar mp = server->mostSignificantUserModePrefix(prefixes);
						me.prepend(mp);
					}
				}
			}
			sender.print(PrintType::OwnText, QStringLiteral("<%1> %2")
												.arg(me)
												.arg(text));
		}
	}
}

void InputHandler::sockwrite(const QString& line)
{
	connection->sockwrite(line);
}
