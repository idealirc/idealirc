/*
 * IdealIRC - Internet Relay Chat client
 * Copyright (C) 2019  Tom-Andre Barstad
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef COMMANDS_H
#define COMMANDS_H

namespace Command {
namespace IRC {
	constexpr auto* PASS		= "PASS";
	constexpr auto* NICK		= "NICK";
	constexpr auto* USER		= "USER";
	constexpr auto* OPER		= "OPER";
	constexpr auto* MODE		= "MODE";
	constexpr auto* QUIT		= "QUIT";
	constexpr auto* SQUIT		= "SQUIT";
	constexpr auto* JOIN		= "JOIN";
	constexpr auto* PART		= "PART";
	constexpr auto* TOPIC		= "TOPIC";
	constexpr auto* NAMES		= "NAMES";
	constexpr auto* LIST		= "LIST";
	constexpr auto* INVITE		= "INVITE";
	constexpr auto* KICK		= "KICK";
	constexpr auto* PRIVMSG		= "PRIVMSG";
	constexpr auto* NOTICE		= "NOTICE";
	constexpr auto* MOTD		= "MOTD";
	constexpr auto* LUSERS		= "LUSERS";
	constexpr auto* VERSION		= "VERSION";
	constexpr auto* STATS		= "STATS";
	constexpr auto* LINKS		= "LINKS";
	constexpr auto* TIME		= "TIME";
	constexpr auto* CONNECT		= "CONNECT";
	constexpr auto* TRACE		= "TRACE";
	constexpr auto* ADMIN		= "ADMIN";
	constexpr auto* INFO		= "INFO";
	constexpr auto* SERVLIST	= "SERVLIST";
	constexpr auto* SQUERY		= "SQUERY";
	constexpr auto* WHO			= "WHO";
	constexpr auto* WHOIS		= "WHOIS";
	constexpr auto* WHOWAS		= "WHOWAS";
	constexpr auto* KILL		= "KILL";
	constexpr auto* PING		= "PING";
	constexpr auto* PONG		= "PONG";
	constexpr auto* ERROR		= "ERROR";
	constexpr auto* AWAY		= "AWAY";
	constexpr auto* REHASH		= "REHASH";
	constexpr auto* DIE			= "DIE";
	constexpr auto* RESTART		= "RESTART";
	constexpr auto* SUMMON		= "SUMMON";
	constexpr auto* USERS		= "USERS";
	constexpr auto* WALLOPS		= "WALLOPS";
	constexpr auto* USERHOST	= "USERHOST";
	constexpr auto* ISON		= "ISON";
}

namespace Internal {
	constexpr auto* CTCP	= "CTCP";
	constexpr auto* ME		= "ME";
	constexpr auto* ECHO	= "ECHO";
}

namespace IRCv3 {
	constexpr auto* CAP		= "CAP";
	constexpr auto* LS		= "LS";
	constexpr auto* LIST	= "LIST";
	constexpr auto* REQ		= "REQ";
	constexpr auto* ACK		= "ACK";
	constexpr auto* NAK		= "NAK";
	constexpr auto* END		= "END";
	constexpr auto* NEW		= "NEW";
	constexpr auto* DEL		= "DEL";
} // namespace IRCv3
} // namespace command

#endif // COMMANDS_H
