/*
 *   IdealIRC - Internet Relay Chat client
 *   Copyright (C) 2019  Tom-Andre Barstad
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License along
 *   with this program; if not, write to the Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
*/

#include <QStringList>
#include <iostream>
#include "IniFile.h"

IniFile::IniFile(QString filename)
{
  file = new QFile(filename);
  if (QFile::exists(filename) == false) {
    file->open(QIODevice::WriteOnly);
    file->close();
  }
}

void IniFile::clearNewline(char *data)
{
    int i = 0;
    while (true) {
        if (data[i] == '\0')
            break;

        if (data[i] == '\n') {
            data[i] = '\0';
            break;
        }
        i++;
    }
}

QString IniFile::value(QString Section, QString Item, QString defaultValue)
{
    if (! file->open(QIODevice::ReadOnly | QIODevice::Text))
		return defaultValue;

	Section = QStringLiteral("[%1]").arg(Section);

    Item.append('=');

    char buf[1024];
    memset(buf, 0, sizeof(buf));
    bool sectFound = false;

    while (file->readLine(buf, sizeof(buf)) != -1) {
        clearNewline(buf);
        QString line(buf);

        if (line.isEmpty())
            continue;

        if (line.left(Section.length()).toUpper() == Section.toUpper()) {
            sectFound = true;
            continue;
        }
        if ((sectFound) && (line.left(Item.length()).toUpper() == Item.toUpper())) {
            file->close();
            return line.mid(Item.length());
        }
    }

    file->close();
	return defaultValue;
}

QString IniFile::value(QString Section, int ItemPos, QString defaultValue)
{
    if (! file->open(QIODevice::ReadOnly | QIODevice::Text))
		return defaultValue;

	Section = QStringLiteral("[%1]").arg(Section);

    char buf[1024];
    memset(buf, 0, sizeof(buf));
    bool sectFound = false;
	int i = 0;

    while (file->readLine(buf, sizeof(buf)) != -1) {
        clearNewline(buf);
        QString line(buf);

        if (line.isEmpty())
            continue;

        if (line.left(Section.length()).toUpper() == Section.toUpper()) {
            sectFound = true;
            continue;
        }

        if ((sectFound) && (line.contains(QChar('=')))) {
            if (i == ItemPos) {
                // Read out value.
                i = line.indexOf('=');
                file->close();
                return line.mid(i+1);
            }
            i++;

        }
    }

    file->close();
	return defaultValue;
}

QString IniFile::section(int SectionPos, QString defaultSectionName)
{
    if (! file->open(QIODevice::ReadOnly | QIODevice::Text))
		return defaultSectionName;

    char buf[1024];
    memset(buf, 0, sizeof(buf));

	int i = 0;

    while (file->readLine(buf, sizeof(buf)) != -1) {
        clearNewline(buf);
        QString line(buf);

        if (line == "")
            continue;


        if (line.startsWith('[') && line.endsWith(']')) {
            if (i == SectionPos) {
                file->close();
                return line.mid(1, line.length()-2);
            }

            i++;
        }
    }

    file->close();
	return defaultSectionName;
}

QString IniFile::key(QString Section, int ItemPos, QString defaultKey)
{
    if (! file->open(QIODevice::ReadOnly | QIODevice::Text))
		return defaultKey;

	Section = QStringLiteral("[%1]").arg(Section);

    char buf[1024];
    memset(buf, 0, sizeof(buf));
    bool sectFound = false;
	int i = 0;

    while (file->readLine(buf, sizeof(buf)) != -1) {
        clearNewline(buf);
        QString line(buf);

        if (line.isEmpty())
            continue;

        if (line.left(Section.length()).toUpper() == Section.toUpper()) {
            sectFound = true;
            continue;
        }
        if ((sectFound) && (line.contains(QChar('=')))) {
            if (i == ItemPos) {
                // Read out item.
                i = line.indexOf('=');
                file->close();
                return line.mid(0,i);
            }
            i++;
        }
    }

    file->close();
	return defaultKey;
}

bool IniFile::write(QString Section, QString Item, QString Value)
{
	Section = QStringLiteral("[%1]").arg(Section);

    Item.append('=');

    QStringList sl;
    char buf[1024];
    memset(buf, 0, sizeof(buf));
    bool sectFound = false;
    bool finished = false;

    if (! file->open(QIODevice::ReadOnly | QIODevice::Text))
        return false;

    while (file->readLine(buf, sizeof(buf)) != -1) {
        clearNewline(buf);
        QString line(buf);

        if (line.isEmpty())
            continue;

        sl.append(line);

        if (finished)
            continue;

        if (line.left(Section.length()).toUpper() == Section.toUpper()) {
            sectFound = true;
            continue;
        }

        // Found existing item, overwriting.
        if ((sectFound) && (line.left(Item.length()).toUpper() == Item.toUpper())) {
            sl.removeAt(sl.count()-1); // Remove the last insertion, we get a new one here...
            sl.append(Item + Value);
            finished = true;
            continue;
        }

        if ((sectFound) && (! finished) && (line.left(1)) == "[") {
            // We have found our section, but not the item, as we reached end of the section.
            sl.removeAt(sl.count()-1); // Remove the last insertion, we get a new one here...
            sl.append(Item + Value);
            sl.append(line);
            finished = true;
            continue;
        }
    }

    if ((sectFound == true) && (finished == false))
        sl.append(Item + Value); // We have found our section, but we reached EOF. Insert new item.

    if (sectFound == false) {
        // Section weren't found, we make a new one at the end, and our item there.
        sl.append(Section);
        sl.append(Item + Value);
    }

    file->close();
    if (! file->open(QIODevice::WriteOnly | QIODevice::Truncate | QIODevice::Text))
        return false;

    for (int i = 0; i <= sl.count()-1; i++) {
        QByteArray out;
        out.append(sl[i]);
        out.append('\n');
        file->write(out);
    }
    file->close();
    return true;
}

int IniFile::countItems(QString Section)
{
    if (! file->open(QIODevice::ReadOnly | QIODevice::Text))
        return 0;

	Section = QStringLiteral("[%1]").arg(Section);

    char buf[1024];
    memset(buf, 0, sizeof(buf));
    bool sectFound = false;
    int count = 0;

    while (file->readLine(buf, sizeof(buf)) != -1) {
        clearNewline(buf);
        QString line(buf);

        if (line.isEmpty())
            continue;

        if (line.left(Section.length()).toUpper() == Section.toUpper()) {
            sectFound = true;
            continue;
        }

        if (sectFound) {
            if (line.left(1) == "[")
                break;
            if (line.contains('='))
                count++;
        }
    }

    file->close();
    return count;
}

int IniFile::countSections()
{
    if (! file->open(QIODevice::ReadOnly | QIODevice::Text))
        return 0;

    char buf[1024];
    memset(buf, 0, sizeof(buf));
    int count = 0;

    while (file->readLine(buf, sizeof(buf)) != -1) {
        clearNewline(buf);
        QString line(buf);
        if (line.isEmpty())
            continue;

        if (line.startsWith('[') && line.endsWith(']'))
            count++;
    }

    file->close();
    return count;
}

bool IniFile::delSection(QString Section)
{
	Section = QStringLiteral("[%1]").arg(Section);

    QStringList sl;
    char buf[1024];
    memset(buf, 0, sizeof(buf));
    bool sectFound = false;

    if (! file->open(QIODevice::ReadOnly | QIODevice::Text))
        return false;

    while (file->readLine(buf, sizeof(buf)) != -1) {
        clearNewline(buf);
        QString line(buf);

        if (line.isEmpty())
            continue;

        if (line.left(Section.length()).toUpper() == Section.toUpper()) {
            sectFound = true;
            continue;
        }

        if (sectFound) {
            if (line.left(1) == "[")
            sectFound = false;
        }

        if (! sectFound)
            sl.push_back(line);
    }

    file->close();
    if (! file->open(QIODevice::WriteOnly | QIODevice::Truncate | QIODevice::Text))
        return false;

    for (int i = 0; i <= sl.count()-1; i++) {
        QByteArray out;
        out.append(sl[i]);
        out.append('\n');
        file->write(out);
    }
    file->close();
    return true;
}

bool IniFile::delItem(QString Section, QString Item)
{
	Section = QStringLiteral("[%1]").arg(Section);

    Item.append('=');

    QStringList sl;
    char buf[1024];
    memset(buf, 0, sizeof(buf));
    bool sectFound = false;
    bool finished = false;

    if (! file->open(QIODevice::ReadOnly | QIODevice::Text))
        return false;

    while (file->readLine(buf, sizeof(buf)) != -1) {
        clearNewline(buf);
        QString line(buf);

        if (line.isEmpty())
        continue;

        sl.append(line);

        if (finished)
            continue;

        if (line.left(Section.length()).toUpper() == Section.toUpper()) {
            sectFound = true;
            continue;
        }

        // Found item
        if ((sectFound) && (line.left(Item.length()).toUpper() == Item.toUpper())) {
            sl.removeAt(sl.count()-1); // Remove the last insertion
            finished = true;
            continue;
        }

        if ((sectFound) && (! finished) && (line.left(1)) == "[") {
            // We have found our section, but not the item, as we reached end of the section.
            // Just close file reading and do not touch the file at all.
            file->close();
            return false; // False because we didn't do anything
        }
    }

    if ((sectFound == true) && (finished == false)) {
        // We have found our section, but we reached EOF. Don't do anything
        file->close();
        return false;
    }

    if (sectFound == false) {
        // Section weren't found, just stop.
        file->close();
        return false;
    }

    file->close();
    if (! file->open(QIODevice::WriteOnly | QIODevice::Truncate | QIODevice::Text))
        return false;

    for (int i = 0; i <= sl.count()-1; i++) {
        QByteArray out;
        out.append(sl[i]);
        out.append('\n');
        file->write(out);
    }
    file->close();
    return true;
}

bool IniFile::sectionExists(QString section)
{
	section = QStringLiteral("[%1]").arg(section);

    if (! file->open(QIODevice::ReadOnly | QIODevice::Text))
      return false;

    char d[64];
    memset(d, 0, 64);
    while (file->readLine(d, 64) > -1) {
        clearNewline(d);
        QString ln(d);
        if (section.toUpper() == ln.toUpper()) {
            file->close();
            return true;
        }
    }

    file->close();
    return false;
}

bool IniFile::appendSection(QString Section)
{
	if (sectionExists(Section))
        return false;

    if (! file->open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Append))
        return false;

	QString out = QStringLiteral("[%1]\n")
                   .arg(Section);

    file->write(out.toLocal8Bit());
    file->close();

    return true;
}

bool IniFile::renameSection(QString OldName, QString NewName)
{
	OldName = QStringLiteral("[%1]").arg(OldName);

	NewName = QStringLiteral("[%1]").arg(NewName);

    QStringList sl;

    char buf[1024];
    memset(buf, 0, sizeof(buf));
    bool finished = false;

    if (! file->open(QIODevice::ReadOnly | QIODevice::Text))
        return false;

    while (file->readLine(buf, sizeof(buf)) != -1) {
        clearNewline(buf);
        QString line(buf);

        if (line.isEmpty())
            continue;

        sl.append(line);

        if (finished)
            continue;

        if (line.toUpper() == OldName.toUpper()) {
            sl.pop_back(); // The very last item inserted is actually OldName. Remove.
            sl.append(NewName);
            finished = true;
        }

    }

    if (finished == false) {
        // Section weren't found, just stop.
        file->close();
        return false;
    }



    file->close();
    if (! file->open(QIODevice::WriteOnly | QIODevice::Truncate | QIODevice::Text))
        return false;

    for (int i = 0; i <= sl.count()-1; i++) {
        QByteArray out;
        out.append(sl[i]);
        out.append('\n');
        file->write(out);
    }
    file->close();
    return true;
}
