/*
 * IdealIRC - Internet Relay Chat client
 * Copyright (C) 2019  Tom-Andre Barstad
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef INPUTHANDLER_H
#define INPUTHANDLER_H

#include "ICommand.h"
#include <QObject>

class IWin;
class IConnection;
class MdiManager;

class InputHandler
{
public:
	InputHandler(IConnection* connection);

	void parse(IWin& sender, const QString& text, bool handleCommand = true);

private:
	inline void sockwrite(const QString& line);
	IConnection* connection;
	ICommand cmdHndl;
};

#endif // INPUTHANDLER_H
