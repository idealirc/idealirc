#include "ButtonbarMgr.h"

int ButtonbarMgr::WindowTypeToGroupPos(const IWin* subwin)
{
	switch (subwin->getType()) {
	case IWin::Type::Status:
		return 1;
	case IWin::Type::Channel:
		return 2;
	case IWin::Type::Private:
		return 3;
	default:
		return 0;
	}
}

ButtonbarMgr::ButtonbarMgr(QToolBar *parent)
	: QObject(parent)
	, m_buttonBar(*parent)
{
	m_buttonBar.setContextMenuPolicy(Qt::CustomContextMenu);
	connect(&m_buttonBar, &QToolBar::customContextMenuRequested, this, &ButtonbarMgr::buttonBarContextMenu);
	m_others_rightSep = m_buttonBar.addSeparator();
}

void ButtonbarMgr::addButton(IWin* subwin, QMdiSubWindow* mdiwin)
{
	QAction* nn = findNextNeighbour(subwin);
	QAction *actn = new QAction(subwin->getButtonText(), &m_buttonBar);
	m_buttonBar.insertAction(nn, actn);
	m_winbtn.insert(subwin, actn);
	actn->setCheckable(true);
	buttons(subwin).push_back(Button(actn, mdiwin));
	connect(actn, &QAction::triggered, [this,actn,mdiwin](bool triggered){
		if (!triggered) {
			actn->setChecked(true);
			return;
		}
		emit changeWindow(mdiwin);
	});
}

void ButtonbarMgr::delButton(IWin* subwin)
{
	auto clear = [this](Button& button){
		m_buttonBar.removeAction(button.button);
		button.menu->deleteLater();
		button.button->deleteLater();
	};

	m_winbtn.remove(subwin);
	if (subwin->getType() != IWin::Type::Status
		&& subwin->getType() != IWin::Type::Channel
		&& subwin->getType() != IWin::Type::Private)
	{
		int otherPos = buttonPosition(m_others, subwin);
		if (otherPos > -1) {
			clear(m_others[otherPos]);
			m_others.removeAt(otherPos);
		}
		return;
	}

	if (subwin->getType() == IWin::Type::Status) {
		deleteGroup(subwin);
		return;
	}

	ButtonGroup* group = tryFindGroup(subwin);
	if (!group) {
		return;
	}

	for (int i = 0; i < group->size; ++i) {
		QList<Button>& buttons = (*group)[i];
		int buttonPos = buttonPosition(buttons, subwin);
		if (buttonPos < 0) continue;

		Button& button = buttons[buttonPos];
		if (button.subwin->widget() == subwin) {
			clear(button);
			buttons.removeAt(buttonPos);
			break;
		}
	}
}

void ButtonbarMgr::reloadButtonName(IWin* subwin, const QString& buttonText)
{
	Button* button = findButton(subwin);
	if (button) {
		button->button->setText(buttonText);
		button->menuHead->setText(buttonText);
	}
}

void ButtonbarMgr::subwinActivated(IWin* prev, IWin* next)
{
	if (prev) {
		Button* button{ findButton(prev) };
		if (button)
			button->button->setChecked(false);
	}
	if (next) {
		Button* button{ findButton(next) };
		if (button)
			button->button->setChecked(true);
	}
}

void ButtonbarMgr::buttonBarContextMenu(const QPoint& pos)
{
	QAction* action = m_buttonBar.actionAt(pos);
	if (!action)
		return;

	IWin *subwin = m_winbtn.key(action);
	if (!subwin)
		return;

	Button* button = findButton(subwin);
	if (!button)
		return;

	QPoint gpos = m_buttonBar.mapToGlobal(pos);
	button->menu->popup(gpos);
}

ButtonbarMgr::ButtonGroup* ButtonbarMgr::tryFindGroup(IWin* subwin)
{
	auto lcontains = [](const QList<Button>& list, IWin* sw){
		for (const auto& item : list) {
			if (item.subwin->widget() == sw)
				return true;
		}
		return false;
	};

	ButtonGroup* null_grp{ nullptr };
	for (auto& group : m_groups) {
		for (int i = 0; i < group.size; ++i) {
			auto& list = group[i];
			if (lcontains(list, subwin))
				return &group;
		}
	}
	return null_grp;
}

QList<ButtonbarMgr::Button>& ButtonbarMgr::buttons(IWin* subwin)
{
	if (subwin->getType() != IWin::Type::Status
		&& subwin->getType() != IWin::Type::Channel
		&& subwin->getType() != IWin::Type::Private)
	{
		return m_others;
	}

	ButtonGroup* group = tryFindGroup(subwin);
	if (group)
		return (*group)[static_cast<int>(subwin->getType())-1];

	if (subwin->getType() == IWin::Type::Status) {
		m_groups.emplace_back();
		m_groups.back().rightSep = m_buttonBar.addSeparator();
		return m_groups.back().status;
	}

	IWin* parent = subwin->getParent();
	group = tryFindGroup(parent);
	if (!group)
		return m_others;

	if (subwin->getType() == IWin::Type::Channel)
		return group->channel;

	else // Implicit "Private"
		return group->privmsg;
}

QAction* ButtonbarMgr::findNextNeighbour(IWin* subwin)
{
	if (subwin->getType() == IWin::Type::Status || subwin->getType() == IWin::Type::Private) {
		ButtonGroup* group = tryFindGroup((subwin->getType() == IWin::Type::Status) ? subwin : subwin->getParent());
		return group ? group->rightSep : nullptr;
	}

	else if (subwin->getType() == IWin::Type::Channel) {
		ButtonGroup* group = tryFindGroup(subwin->getParent());
		if (!group)
			return nullptr;
		if (group->privmsg.isEmpty())
			return group->rightSep;
		else
			return group->privmsg[0].button;
	}
	else
		return m_others_rightSep;
}

ButtonbarMgr::Button* ButtonbarMgr::findButton(IWin* subwin)
{
	if (!subwin)
		return nullptr;
	else {
		ButtonGroup* group = tryFindGroup(subwin);
		if (group) {
			for (int i = 0; i < group->size; ++i) {
				QList<Button>& list = (*group)[i];
				int bpos = buttonPosition(list, subwin);
				if (bpos > -1)
					return &list[bpos];
			}
		}
	}

	int otherPos = buttonPosition(m_others, subwin);
	if (otherPos < 0)
		return nullptr;
	else {
		Button& button = m_others[otherPos];
		return &button;
	}
}

void ButtonbarMgr::deleteGroup(IWin* statuswin)
{
	auto it = m_groups.begin();
	for (; it != m_groups.end(); ++it) {
		if ((*it).status[0].subwin->widget() == qobject_cast<QWidget*>(statuswin))
			break;
	}
	ButtonGroup& group = *it;
	for (int i = 0; i < group.size; ++i) {
		QList<Button>& list = group[i];
		for (auto& item : list) {
			item.menuHead->deleteLater();
			item.menuSep->deleteLater();
			item.menuClose->deleteLater();
			item.menu->deleteLater();
			item.button->deleteLater();
		}
	}
	group.rightSep->deleteLater();
	m_groups.erase(it);
}

int ButtonbarMgr::buttonPosition(QList<ButtonbarMgr::Button>& list, IWin* sw)
{
	for (int i = 0; i < list.count(); ++i) {
		ButtonbarMgr::Button& item = list[i];
		if (item.subwin->widget() == sw)
			return i;
	}
	return -1;
}

ButtonbarMgr::Button::Button(QAction* button_, QMdiSubWindow* parent)
	: subwin(parent)
	, button(button_)
{
	const QString& buttonText = qobject_cast<IWin*>(parent->widget())->getButtonText();
	menu = new QMenu(parent);
	menuHead = menu->addAction(buttonText);
	QFont f = menuHead->font();
	f.setBold(true);
	menuHead->setFont(f);
	menuHead->setDisabled(true);
	menuSep = menu->addSeparator();
	menuClose = menu->addAction(tr("Close"));
	connect(menuClose, &QAction::triggered, [parent](bool){
		parent->close();
	});
}
