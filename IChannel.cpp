/*
 * IdealIRC - Internet Relay Chat client
 * Copyright (C) 2019  Tom-Andre Barstad
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "IChannel.h"

IChannel::IChannel(const QString& name)
	: m_name(name)
{
}

void IChannel::addMember(std::shared_ptr<IMember> member, bool notify)
{
	m_members.push_back(member);
}

void IChannel::delMember(std::shared_ptr<IMember> member)
{
	m_members.removeAll(member);
}

bool IChannel::hasNickname(const QString& nickname) const
{
	for (auto member : m_members) {
		if (member->getNickname() == nickname)
			return true;
	}
	return false;
}

const QList<std::shared_ptr<IMember>>& IChannel::getMembers() const
{
	return m_members;
}

void IChannel::reset()
{
	m_topic.clear();
	m_members.clear();
}

bool IChannel::operator==(const IChannel& other)
{
	return m_name.compare(other.m_name, Qt::CaseInsensitive) == 0;
}

bool IChannel::operator==(const QString& channelname)
{
	return m_name.compare(channelname, Qt::CaseInsensitive) == 0;
}

IChannel& IChannel::operator=(const IChannel& other)
{
	m_name = other.m_name;
	m_topic = other.m_topic;
	m_members = other.m_members;
	return *this;
}
