#include "IListWidget.h"
#include "ConfigMgr.h"

IListWidget::IListWidget(QWidget* parent)
	: QListWidget(parent)
{
	ConfigMgr& conf = ConfigMgr::instance();
	connect(&conf, &ConfigMgr::saved, this, &IListWidget::loadConfig);
	loadConfig();
}

void IListWidget::loadConfig()
{
	ConfigMgr& conf = ConfigMgr::instance();

	const QString style = QStringLiteral("QListWidget { "
							"background-color: %1;"
							"color: %2;"
							"}")
							.arg(conf.color("ListboxBackground"))
							.arg(conf.color("ListboxForeground"));
	setStyleSheet(style);
	QFont newfont(conf.common("Font"));
	newfont.setPixelSize(conf.common("FontSize").toInt());
	setFont(newfont);
}
