#ifndef ILISTWIDGET_H
#define ILISTWIDGET_H

#include <QListWidget>

class IListWidget : public QListWidget
{
public:
	IListWidget(QWidget* parent = nullptr);

private:
	void loadConfig();
};

#endif // ILISTWIDGET_H
