/*
 * IdealIRC - Internet Relay Chat client
 * Copyright (C) 2019  Tom-Andre Barstad
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "IIRCView.h"
#include <QScrollBar>
#include <QFontMetrics>
#include <QDateTime>
#include <QRegularExpression>
#include <QDebug>
#include <ConfigMgr.h>
#include <unordered_map>

namespace {
const std::unordered_map<PrintType, QString> PrintTypeMap = {
	{ PrintType::Normal, "Normal" },
	{ PrintType::ProgramInfo, "ProgramInfo" },
	{ PrintType::ServerInfo, "ServerInfo" },
	{ PrintType::Nick, "Nick" },
	{ PrintType::Mode, "Mode" },
	{ PrintType::Quit, "Quit" },
	{ PrintType::Join, "Join" },
	{ PrintType::Part, "Part" },
	{ PrintType::Topic, "Topic" },
	{ PrintType::Invite, "Invite" },
	{ PrintType::Kick, "Kick" },
	{ PrintType::Wallops, "Wallops" },
	{ PrintType::Notice, "Notice" },
	{ PrintType::Action, "Action" },
	{ PrintType::CTCP, "CTCP" },
	{ PrintType::OwnText, "OwnText" },
	{ PrintType::Highlight, "Highlight" }
};
}


QColor colorCode(ColorCode cc)
{
	switch (cc) {
	case ColorCode::White:
		return QColor(0xFF, 0xFF, 0xFF);
	case ColorCode::Black:
		return QColor(0x00, 0x00, 0x00);
	case ColorCode::Blue:
		return QColor(0x00, 0x00, 0x84);
	case ColorCode::Green:
		return QColor(0x00, 0x84, 0x00);
	case ColorCode::BrightRed:
		return QColor(0xFF, 0x00, 0x00);
	case ColorCode::Red:
		return QColor(0x84, 0x00, 0x00);
	case ColorCode::Magenta:
		return QColor(0x84, 0x00, 0x84);
	case ColorCode::Brown:
		return QColor(0x84, 0x84, 0x00);
	case ColorCode::Yellow:
		return QColor(0xFF, 0xFF, 0x00);
	case ColorCode::Brightgreen:
		return QColor(0x00, 0xFF, 0x00);
	case ColorCode::Cyan:
		return QColor(0x00, 0x84, 0x84);
	case ColorCode::BrightCyan:
		return QColor(0x00, 0xFF, 0xFF);
	case ColorCode::BrightBlue:
		return QColor(0x00, 0x00, 0xFF);
	case ColorCode::BrightMagenta:
		return QColor(0xFF, 0x00, 0xFF);
	case ColorCode::DarkGray:
		return QColor(0x84, 0x84, 0x84);
	case ColorCode::LightGray:
		return QColor(0xC6, 0xC6, 0xC6);
	default:
	{
		ConfigMgr& conf = ConfigMgr::instance();
		return QColor(conf.color("Normal"));
	}
	}
}

bool isCtrlCode(char c)
{
	switch (c) {
	case CtrlBold:
	case CtrlUnderline:
	case CtrlColor:
	case CtrlItalic:
	case CtrlReverse:
	case CtrlStriketrhough:
	case CtrlReset:
		return true;
	default:
		return false;
	}
}

IIRCView::IIRCView()
{
	setOpenExternalLinks(true);
	resetView();
	ConfigMgr& conf = ConfigMgr::instance();
	connect(&conf, &ConfigMgr::saved, this, &IIRCView::newConfiguration);
}

void IIRCView::print(const PrintType ptype, const QString& text)
{
	QString textFormatted = stripTags(text);
	QString styleclass = typeToString(ptype, "Normal");
	textFormatted = formatType(ptype, textFormatted);

	ConfigMgr& conf = ConfigMgr::instance();
	if (conf.common("ShowTimestamp") == "1" && !conf.common("TimestampFormat").isEmpty()) {
		QDateTime dt = QDateTime::currentDateTime();
		QString ts = dt.toString(conf.common("TimestampFormat")) + " ";
		textFormatted.prepend(ts);
	}

	linkFormat(textFormatted);
	ctrlFormat(textFormatted);
	QString textToPrint = QStringLiteral("<div class=\"%1\">%2</div>").arg(styleclass).arg(textFormatted);
	cachedText += textToPrint;
	append(textToPrint);

	bool scrollToBottom = (verticalScrollBar()->value() == verticalScrollBar()->maximum());
	if (scrollToBottom)
		verticalScrollBar()->setSliderPosition(verticalScrollBar()->maximum());
}

QString IIRCView::stripTags(const QString& text)
{
	QString textFormatted = text;
	textFormatted.replace("&", "&amp;");
	textFormatted.replace("<", "&lt;");
	textFormatted.replace(">", "&gt;");
	return textFormatted;
}

QString IIRCView::formatType(PrintType ptype, const QString& text)
{
	QString ret;
	switch (ptype) {
	case PrintType::ProgramInfo:
	case PrintType::ServerInfo:
	case PrintType::Nick:
	case PrintType::Mode:
	case PrintType::Quit:
	case PrintType::Join:
	case PrintType::Part:
	case PrintType::Topic:
	case PrintType::Invite:
	case PrintType::Kick:
	case PrintType::Wallops:
		ret = QStringLiteral("*** %1").arg(text);
		break;

	case PrintType::Action:
		ret = QStringLiteral("* %1").arg(text);
		break;

	default:
		ret = text;
		break;
	}

	return ret;
}

QString IIRCView::typeToString(PrintType ptype, const QString& defaultValue)
{
	auto typeit = PrintTypeMap.find(ptype);
	if (typeit == PrintTypeMap.end())
		return defaultValue;
	return typeit->second;
}


void IIRCView::resizeEvent(QResizeEvent* evt)
{
	bool scrollToBottom = (verticalScrollBar()->value() == verticalScrollBar()->maximum());
	QTextBrowser::resizeEvent(evt);

	if (topMargin) {
		QTextFrameFormat ff;
		ff.setWidth(width() - verticalScrollBar()->width() - 3);
		ff.setTopMargin(height() - reduceMargin());
		topMargin->setFrameFormat(ff);
		document()->adjustSize();
	}

	if (scrollToBottom)
		verticalScrollBar()->setSliderPosition(verticalScrollBar()->maximum());
}

int IIRCView::reduceMargin() const
{
	// 4x seems like a constant that wont make scroll bar appear out of the wild, when there's no content.
	// Tested with fixed and non-fixed fonts of varying sizes up to 30px, should in theory work with any size and font.
	QFontMetrics fm(font());
	return fm.height() * 4;
}

void IIRCView::newConfiguration()
{
	bool scrollToBottom = (verticalScrollBar()->value() == verticalScrollBar()->maximum());
	resetView();
	append(cachedText);
	if (scrollToBottom)
		verticalScrollBar()->setSliderPosition(verticalScrollBar()->maximum());
}

void IIRCView::resetView()
{
	ConfigMgr& conf = ConfigMgr::instance();
	topMargin->deleteLater();
	topMargin = nullptr;
	clear();
	QFont fnt(conf.common("Font"));
	fnt.setPixelSize(conf.common("FontSize").toInt());
	setFont(fnt);

	const QString documentStyle = QStringLiteral(
					".Normal { color: %1; }"
					".ProgramInfo { color: %2; }"
					".ServerInfo { color: %3; }"
					".Nick { color: %4; }"
					".Mode { color: %5; }"
					".Quit { color: %6; }"
					".Join { color: %7; }"
					".Part { color: %8; }"
					".Topic { color: %9; }"
					".Invite { color: %10; }"
					".Kick { color: %11; }"
					".Wallops { color: %12; }"
					".Action { color: %13; }"
					".CTCP { color: %14; }"
					".Notice { color: %15; }"
					".OwnText { color: %16; }"
					".Highlight { color: %17; }"
					"a { color: %18 }")
					.arg(conf.color("Normal"))
					.arg(conf.color("ProgramInfo"))
					.arg(conf.color("ServerInfo"))
					.arg(conf.color("Nick"))
					.arg(conf.color("Mode"))
					.arg(conf.color("Quit"))
					.arg(conf.color("Join"))
					.arg(conf.color("Part"))
					.arg(conf.color("Topic"))
					.arg(conf.color("Invite"))
					.arg(conf.color("Kick"))
					.arg(conf.color("Wallops"))
					.arg(conf.color("Action"))
					.arg(conf.color("CTCP"))
					.arg(conf.color("Notice"))
					.arg(conf.color("OwnText"))
					.arg(conf.color("Highlight"))
					.arg(conf.color("Links"));
	document()->setDefaultStyleSheet(documentStyle);

	QString widgetBgImage;
	if (conf.common("BgImageEnabled") == "1") {
		widgetBgImage = QStringLiteral("background-image: url('%1'); background-repeat: none; background-attachment: fixed; background-position: center center;")
							.arg(conf.common("BgImagePath"));

	}

	const QString widgetStyle = QStringLiteral("QTextEdit { "
						"background-color: %1;"
						"%2"
						" }")
						.arg(conf.color("TextviewBackground"))
						.arg(widgetBgImage);
	setStyleSheet(widgetStyle);

	setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

	QTextCursor tc = textCursor();
	QTextFrameFormat ff;
	ff.setWidth(width() - verticalScrollBar()->width() - 3);
	ff.setTopMargin(height() - reduceMargin());
	topMargin = tc.insertFrame(ff);
	setTextCursor(tc);

}

void IIRCView::ctrlFormat(QString& text)
{
	struct {
		bool bold{ false };
		bool underline{ false };
		bool italic{ false };
		bool reverse{ false };
		bool strikethrough{ false };
		std::optional<QColor> fgColor;
		std::optional<QColor> bgColor;
		bool has() {
			return bold | underline | strikethrough | italic
					| fgColor.has_value() | bgColor.has_value();
		}
		void reset() {
			bold = false;
			underline = false;
			italic = false;
			reverse = false;
			strikethrough = false;
			fgColor.reset();
			bgColor.reset();
		}
	} state;

	auto parseColor = [&text,&state](int pos){
		QString code;
		bool hasComma{ false };
		for (int i = pos + 1; i < text.length(); ++i) {
			QChar c = text[i];
			if (c == ',' && !hasComma) {
				code += c;
				hasComma = true;
			}
			else if (isdigit(c.toLatin1()))
				code += c;
			else
				break;
		}

		if (code.isEmpty() || code == ",") {
			state.fgColor.reset();
			state.bgColor.reset();
			return;
		}

		QStringList col = code.split(',');
		if (col.length() < 2)
			col << "" << "";
		const QString& fg = col[0];
		const QString& bg = col[1];
		if (!fg.isEmpty())
			state.fgColor.emplace(colorCode(static_cast<ColorCode>(fg.toInt())));
		else if (hasComma)
			state.fgColor.reset();
		if (!bg.isEmpty())
			state.bgColor.emplace(colorCode(static_cast<ColorCode>(bg.toInt())));
		else if (hasComma)
			state.bgColor.reset();
		text.remove(pos + 1, code.length());
	};

	auto endMark = [&text](int& pos){
		QString tag{ "</span>" };
		text.replace(pos, 1, tag);
		pos += tag.length();
	};

	bool isParsing{ false };
	for (int i = 0; i < text.length(); ++i) {
		const char c = text[i].toLatin1();
		switch (c) {
		case CtrlBold:
			state.bold = !state.bold;
			break;
		case CtrlUnderline:
			state.underline = !state.underline;
			break;
		case CtrlColor:
			parseColor(i);
			break;
		case CtrlItalic:
			state.italic = !state.italic;
			break;
		case CtrlReverse:
			state.reverse = !state.reverse;
			break;
		case CtrlStriketrhough:
			state.strikethrough = !state.strikethrough;
			break;
		case CtrlReset:
			state.reset();
			endMark(i);
			isParsing = false;
			continue;

		default:
			continue;
		}

		if (i < text.length()-1 && isCtrlCode(text[i+1].toLatin1())) {
			text.remove(i, 1);
			--i;
			continue;
		}

		if (!isParsing)
			isParsing = true;
		else
			endMark(i);

		if (!state.has()) {
			isParsing = false;
			continue;
		}

		QString style;
		QStringList textDecoration;
		QString fgColor;
		QString bgColor;
		if (state.bold)
			style += "font-weight:bold;";
		if (state.underline)
			textDecoration << "underline";
		if (state.strikethrough)
			textDecoration << "line-through";
		if (state.italic)
			style += "font-style:italic;";
		if (state.fgColor)
			fgColor = QStringLiteral("color:%1;").arg(state.fgColor->name());
		if (state.bgColor)
			bgColor = QStringLiteral("background-color:%1;").arg(state.bgColor->name());
		if (state.reverse) {
			ConfigMgr& conf = ConfigMgr::instance();
			fgColor = conf.color("TextviewBackground");
			bgColor = conf.color("Normal");
		}

		if (!textDecoration.isEmpty())
			style += "text-decoration:" + textDecoration.join(' ') + ";";
		if (!fgColor.isEmpty())
			style += fgColor;
		if (!bgColor.isEmpty())
			style += bgColor;

		QString tag{ "<span style=\""+style+"\">" };
		if (isCtrlCode(text[i].toLatin1()))
			text.replace(i, 1, tag);
		else
			text.insert(i, tag);
		i += tag.length();
	}

	if (isParsing)
		text.append("</span>");
}

void IIRCView::linkFormat(QString& text)
{
	QRegExp rx("((htt(p|ps))|ftp):\\/\\/.+\\s?"); // ((htt(p|ps))|ftp):\/\/.+\s?
	int mp = text.indexOf(rx);
	while (mp > -1) {
		int idx = text.indexOf(' ', mp);
		const int len = (idx > -1) ? idx - mp : text.length() - mp;
		const QString link = text.mid(mp, len);

		const QString startTag = QStringLiteral("<a href=\"%1\">").arg(link);
		const QString endTag{ "</a>" };

		text.insert(mp, startTag);
		mp += startTag.length();
		if (idx > -1)
			idx += startTag.length();

		text.insert(mp + len, endTag);
		if (idx == -1)
			break; // Last one done.
		mp += endTag.length();
		idx += endTag.length();

		mp = text.indexOf(rx, idx);
	}
}

