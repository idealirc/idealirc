/*
 * IdealIRC - Internet Relay Chat client
 * Copyright (C) 2019  Tom-Andre Barstad
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef IIRCVIEW_H
#define IIRCVIEW_H

#include <QTextBrowser>
#include <QResizeEvent>
#include <QTextFrame>

enum class PrintType {
	Normal = 0, //!< Normal text
	ProgramInfo, //!< Information text from IdealIRC
	ServerInfo, //!< Uncategorized messages from the server
	Nick, //!< Nickname changes
	Mode, //!< Mode setting on local user and channel
	Quit, //!< Quit messages
	Join, //!< Channel join messages
	Part, //!< Channel part messages
	Topic, //!< Topic messages (covers also the numerics)
	Invite, //!< Invitation to a channel
	Kick, //!< Channel kick messages
	Wallops, //!< Wallop messages
	Notice, //!< Notice messages
	Action, //!< Action/Role-play styled message ("/me does a thing")
	CTCP, //!< CTCP request and reply
	OwnText, //!< Messages sent from us
	Highlight //!< Messages marked as highlighted
};

enum class ColorCode {
	White,
	Black,
	Blue,
	Green,
	BrightRed,
	Red,
	Magenta,
	Brown,
	Yellow,
	Brightgreen,
	Cyan,
	BrightCyan,
	BrightBlue,
	BrightMagenta,
	DarkGray,
	LightGray
};

QColor colorCode(ColorCode cc);

enum CtrlCode : char {
	CtrlBold			= 0x02,
	CtrlUnderline		= 0x1F,
	CtrlColor			= 0x03,
	CtrlItalic			= 0x09,
	CtrlReverse			= 0x16,
	CtrlStriketrhough	= 0x13,
	CtrlReset			= 0x0F
};

bool isCtrlCode(char c);

class IIRCView : public QTextBrowser
{
	Q_OBJECT

public:
	IIRCView();
	void print(const PrintType ptype, const QString& text);
	static QString stripTags(const QString& text);
	static QString formatType(PrintType ptype, const QString& text);
	static QString typeToString(PrintType ptype, const QString& defaultValue = "");

private:
	QTextFrame* topMargin{ nullptr };
	void resizeEvent(QResizeEvent* evt);
	int reduceMargin() const;
	void newConfiguration();
	void resetView();
	static void ctrlFormat(QString& text);
	static void linkFormat(QString& text);

	 /** @brief
		 Since Qt is a good boy and formats the "HTML" by inlining the "CSS" styles right in the tags,
		 we need to cache the text we append as "HTML", in order to re-set colors upon
		 style re-definitions (via configuration).
	*/
	QString cachedText;
};

#endif // IIRCVIEW_H
