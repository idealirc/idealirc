#ifndef ILINEEDIT_H
#define ILINEEDIT_H

#include <QLineEdit>
#include <QKeyEvent>
#include <QFocusEvent>
#include <QDebug>
#include <QStringList>

class TabEvent : public QObject
{
	Q_OBJECT

public:
	TabEvent(QObject* parent)
		: QObject(parent)
	{}

private:
	bool eventFilter(QObject* obj, QEvent *evt) override;
};

class ILineEdit : public QLineEdit
{
	Q_OBJECT
	friend class TabEvent;

public:
	ILineEdit(QWidget* parent = nullptr);

private:
	void loadConfig();
	void keyTab();
	void keyUp();
	void keyDown();
	void keyPressEvent(QKeyEvent* evt) override;
	QString currentLine;
	QStringList prevLines;
	int logidx{ -1 };
};

#endif // ILINEEDIT_H
