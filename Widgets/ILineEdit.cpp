#include "ILineEdit.h"
#include "ConfigMgr.h"

ILineEdit::ILineEdit(QWidget* parent)
	: QLineEdit(parent)
{
	ConfigMgr& conf = ConfigMgr::instance();
	connect(&conf, &ConfigMgr::saved, this, &ILineEdit::loadConfig);
	loadConfig();

	TabEvent* tabevt = new TabEvent(this);
	installEventFilter(tabevt);
}

void ILineEdit::loadConfig()
{
	ConfigMgr& conf = ConfigMgr::instance();

	const QString style = QStringLiteral("QLineEdit { "
							"background-color: %1;"
							"color: %2;"
							"}")
							.arg(conf.color("InputBackground"))
							.arg(conf.color("InputForeground"));
	setStyleSheet(style);
	QFont newfont(conf.common("Font"));
	newfont.setPixelSize(conf.common("FontSize").toInt());
	setFont(newfont);
}

void ILineEdit::keyTab()
{
}

void ILineEdit::keyUp()
{
	if (logidx == -1)
		currentLine = text();
	if (logidx+1 >= prevLines.size())
		return;

	++logidx;
	setText(prevLines[logidx]);
}

void ILineEdit::keyDown()
{
	if (logidx == -1)
		return;

	--logidx;
	if (logidx == -1) {
		setText(currentLine);
		return;
	}
	else
		setText(prevLines[logidx]);
}

void ILineEdit::keyPressEvent(QKeyEvent* evt)
{
	QLineEdit::keyPressEvent(evt);
}

bool TabEvent::eventFilter(QObject* obj, QEvent* evt)
{
	auto* input = qobject_cast<ILineEdit*>(obj);
	if (!input)
		qWarning() << "Expected parent for TabEvent is not an ILineEdit!";
	else if (evt->type() == QEvent::KeyPress) {
		QKeyEvent* kevt = dynamic_cast<QKeyEvent*>(evt);
		if (kevt->key() == Qt::Key_Tab) {
			input->keyTab();
			return true;
		}
		else if (kevt->key() == Qt::Key_Up) {
			input->keyUp();
			return true;
		}
		else if (kevt->key() == Qt::Key_Down) {
			input->keyDown();
			return true;
		}
		else if (kevt->key() == Qt::Key_Return || kevt->key() == Qt::Key_Enter)
			input->prevLines.push_front(input->text());
		else {
			input->logidx = -1;
		}
	}
	return QObject::eventFilter(obj, evt);
}
