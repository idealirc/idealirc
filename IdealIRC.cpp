/*
 * IdealIRC - Internet Relay Chat client
 * Copyright (C) 2019  Tom-Andre Barstad
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "IdealIRC.h"
#include "ui_IdealIRC.h"
#include "config.h"
#include "IWin/IWinStatus.h"
#include <ConfigMgr.h>
#include <QDebug>
#include <QDir>
#include <QMessageBox>

namespace {
void copyRecursively(const QString& from, const QString& to)
{
	QDir srcDir(from);
	if (!srcDir.exists()) {
		qWarning() << "Unable to copy" << from << "to" << to;
		return;
	}

	QDir destDir(to);
	if (!destDir.exists())
		destDir.mkdir(to);

	auto sep = QDir::separator();
	QStringList files = srcDir.entryList(QDir::Files);
	for (const QString& fileName : files) {
		QString srcName = from + sep + fileName;
		QString destName = to + sep + fileName;
		if (!QFile::copy(srcName, destName))
			qWarning() << "Unable to copy" << srcName << "to" << destName;
	}

	files = srcDir.entryList(QDir::AllDirs | QDir::NoDotAndDotDot);
	for (const QString& fileName : files) {
		QString srcName = from + sep + fileName;
		QString destName = to + sep + fileName;
		copyRecursively(srcName, destName);
	}
}

void runtimeEnvironmentSetup()
{
	if (BUILD_TYPE == BuildType::packaged) {
		QDir skelDir(GLOBAL_PATH+"/skel");
		if (!skelDir.exists())
			return; // Nothing more to do.

		QDir destination(LOCAL_PATH);
		if (!destination.exists()) {
			destination.mkdir(LOCAL_PATH);
			copyRecursively(skelDir.path(), destination.path());
		}
	}
}
}

IdealIRC::IdealIRC(QWidget* parent)
	: QMainWindow(parent)
	, ui(new Ui::IdealIRC)
	, confDlg(this)
	, aboutDlg(this)
{
	ui->setupUi(this);

	qInfo() << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~";
	qInfo() << "    IdealIRC version" << VERSION_STRING << "started.";
	qInfo() << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~";
	runtimeEnvironmentSetup();
	qDebug() << "Global path:" << GLOBAL_PATH;
	qDebug() << "Local path:" << LOCAL_PATH;

	QString title = QString("%1 %2")
					.arg(windowTitle())
					.arg(VERSION_STRING_NO_BUILD);
	setWindowTitle(title);
	trayIcon = new QSystemTrayIcon(QIcon(QStringLiteral(":/Icons/iconcutted.png")), this);
	mdiManager = new MdiManager(*ui->mdiArea, *ui->windowButtons, *trayIcon);
	trayIcon->show();
	trayIcon->setToolTip(title);

	connect(mdiManager, &MdiManager::readyForExit, this, &QMainWindow::close);
	connect(mdiManager, &MdiManager::subwindowSwitched, this, &IdealIRC::subwindowSwitched);
	connect(mdiManager, &MdiManager::connectionStateChange, this, &IdealIRC::updateConnectButtonState);
	connect(&confDlg, &IConfig::connectToServer, this, &IdealIRC::connectToServer);
	connect(&confDlg, &IConfig::disconnectFromServer, this, &IdealIRC::disconnectFromServer);
	setConnectButtonState(ConnectButtonState::Connect);
}

IdealIRC::~IdealIRC()
{
	delete mdiManager;
	delete trayIcon;
	delete ui;
}

void IdealIRC::showEvent(QShowEvent*)
{
	static bool FirstShow = true;
	if (!FirstShow) return;
	FirstShow = false;

	IWin* statusw = mdiManager->createSubwindow(nullptr, "Status", IWin::Type::Status);
	statusw->refreshWindowTitle();

	ConfigMgr& conf = ConfigMgr::instance();
	if (conf.common("ShowOptions") == "1")
		confDlg.show();
}

void IdealIRC::closeEvent(QCloseEvent* evt)
{
	int serversWaiting = mdiManager->connectionsOnlineCount();
	/* Initialize closing */
	if (serversWaiting > 0) {
		auto btn = QMessageBox::question(this, tr("Close IdealIRC"), tr("There are %1 connections active.\nDo you really want to exit IdealIRC?").arg(serversWaiting));
		if (btn == QMessageBox::Yes)
			mdiManager->broadcastProgramExit();
		evt->ignore();
	}
	else {
		evt->accept();
	}
}

void IdealIRC::subwindowSwitched()
{
	updateConnectButtonState();
}

void IdealIRC::updateConnectButtonState()
{
	IWinStatus* status = mdiManager->currentStatus();
	bool isConnected = status->getConnection()->isConnected();
	setConnectButtonState(isConnected ? ConnectButtonState::Disconnect : ConnectButtonState::Connect);
}

void IdealIRC::setConnectButtonState(IdealIRC::ConnectButtonState state)
{
	switch (state) {
	case ConnectButtonState::Connect:
		ui->actionConnect->setText(tr("Connect"));
		break;
	case ConnectButtonState::Disconnect:
		ui->actionConnect->setText(tr("Disconnect"));
		break;
	}
	ui->actionConnect->setData(static_cast<int>(state));
}

void IdealIRC::connectToServer(bool newStatus)
{
	IWinStatus* status{ nullptr };
	if (newStatus)
		status = static_cast<IWinStatus*>(mdiManager->createSubwindow(nullptr, "Status", IWin::Type::Status));
	else
		status = mdiManager->currentStatus();
	ConfigMgr& conf = ConfigMgr::instance();
	QString server = conf.connection("Server");

	{
		QString nickname = conf.connection("Nickname");
		QString realname = conf.connection("Realname");
		QString username = conf.connection("Username");

		if (server.isEmpty() || nickname.isEmpty() || realname.isEmpty() || username.isEmpty()) {
			confDlg.show();
			return;
		}
	}

	bool ssl = conf.connection("SSL").toInt();
	QString host;
	quint16 port;
	if (server.contains(':')) {
		QString strport = server.split(':')[1];
		port = strport.toUShort();
		server = server.remove(":"+strport);
	}
	else
		port = 6667;
	host = server;

	if (status->getConnection()->isConnected())
		status->getConnection()->reConnect(host, port, ssl);
	else {
		setConnectButtonState(ConnectButtonState::Disconnect);
		status->getConnection()->tryConnect(host, port, ssl);
	}
}

void IdealIRC::disconnectFromServer()
{
	IWinStatus* status = mdiManager->currentStatus();
	status->getConnection()->quit();
}

void IdealIRC::on_actionConnect_triggered()
{
	ConnectButtonState state = static_cast<ConnectButtonState>(ui->actionConnect->data().toInt());
	if (state == ConnectButtonState::Connect)
		connectToServer(false);

	else if (state == ConnectButtonState::Disconnect) {
		IConnection* connection = mdiManager->currentStatus()->getConnection();
		if (connection->isOnline())
			connection->quit();
		else {
			connection->close();
			setConnectButtonState(ConnectButtonState::Connect);
		}
	}
}

void IdealIRC::on_actionOptions_triggered()
{
	confDlg.show();
	if (mdiManager->currentStatus()->getConnection()->isConnected())
		confDlg.showDisconnectButton();
	else
		confDlg.hideDisconnectButton();
}

void IdealIRC::on_actionAbout_IdealIRC_triggered()
{
	aboutDlg.show();
}
