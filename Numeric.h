/*
 * IdealIRC - Internet Relay Chat client
 * Copyright (C) 2019  Tom-Andre Barstad
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef NUMERICS_H
#define NUMERICS_H

namespace Numeric {

/*
 * Error reply numerics
*/
constexpr auto* ERR_NOSUCHNICK		  = "401";
constexpr auto* ERR_NOSUCHSERVER	  = "402";
constexpr auto* ERR_NOSUCHCHANNEL	  = "403";
constexpr auto* ERR_CANNOTSENDTOCHAN  = "404";
constexpr auto* ERR_TOOMANYCHANNELS   = "405";
constexpr auto* ERR_WASNOSUCHNICK	  = "406";
constexpr auto* ERR_TOOMANYTARGETS	  = "407";
constexpr auto* ERR_NOORIGIN		  = "409";
constexpr auto* ERR_NORECIPIENT		  = "411";
constexpr auto* ERR_NOTEXTTOSEND	  = "412";
constexpr auto* ERR_NOTOPLEVEL		  = "413";
constexpr auto* ERR_WILDTOPLEVEL	  = "414";
constexpr auto* ERR_UNKNOWNCOMMAND	  = "421";
constexpr auto* ERR_NOMOTD			  = "422";
constexpr auto* ERR_NOADMININFO		  = "423";
constexpr auto* ERR_FILEERROR		  = "424";
constexpr auto* ERR_NONICKNAMEGIVEN   = "431";
constexpr auto* ERR_ERRORNEUSNICKNAME = "432";
constexpr auto* ERR_NICKNAMEINUSE	  = "433";
constexpr auto* ERR_NICKCOLLISION	  = "436";
constexpr auto* ERR_USERNOTINCHANNEL  = "441";
constexpr auto* ERR_NOTONCHANNEL	  = "442";
constexpr auto* ERR_USERONCHANNEL	  = "443";
constexpr auto* ERR_NOLOGIN			  = "444";
constexpr auto* ERR_SUMMONDISABLED	  = "445";
constexpr auto* ERR_USERSDISABLED	  = "446";
constexpr auto* ERR_NOTREGISTERED	  = "451";
constexpr auto* ERR_NEEDMOREPARAMS	  = "461";
constexpr auto* ERR_ALREADYREGISTERED = "462";
constexpr auto* ERR_NOPERMFORHOST	  = "463";
constexpr auto* ERR_PASSWDMISMATCH	  = "464";
constexpr auto* ERR_YOUREBANNEDCREEP  = "465";
constexpr auto* ERR_KEYSET			  = "467";
constexpr auto* ERR_CHANNELISFULL	  = "471";
constexpr auto* ERR_UNKNOWNMODE		  = "472";
constexpr auto* ERR_INVITEONLYCHAN	  = "473";
constexpr auto* ERR_BANNEDFROMCHAN	  = "474";
constexpr auto* ERR_BADCHANNELKEY	  = "475";
constexpr auto* ERR_NOPRIVILEGES	  = "481";
constexpr auto* ERR_CHANOPRIVSNEEDED  = "482";
constexpr auto* ERR_CANTKILLSERVER	  = "483";
constexpr auto* ERR_NOOPERHOST		  = "491";
constexpr auto* ERR_UMODEUNKNOWNFLAG  = "501";
constexpr auto* ERR_USERSDONTMATCH	  = "502";

/*
 * Command reply numerics
*/
constexpr auto* RPL_NONE			= "300";
constexpr auto* RPL_USERHOST		= "302";
constexpr auto* RPL_ISON			= "303";
constexpr auto* RPL_AWAY			= "301";
constexpr auto* RPL_UNAWAY			= "305";
constexpr auto* RPL_NOWAWAY			= "306";
constexpr auto* RPL_WHOISUSER		= "311";
constexpr auto* RPL_WHOISSERVER		= "312";
constexpr auto* RPL_WHOISOPERATOR   = "313";
constexpr auto* RPL_WHOISIDLE		= "317";
constexpr auto* RPL_ENDOFWHOIS		= "318";
constexpr auto* RPL_WHOISCHANNELS   = "319";
constexpr auto* RPL_WHOWASUSER		= "314";
constexpr auto* RPL_ENDOFWHOWAS		= "369";
constexpr auto* RPL_LISTSTART		= "321";
constexpr auto* RPL_LIST			= "322";
constexpr auto* RPL_LISTEND			= "323";
constexpr auto* RPL_CHANNELMODEIS   = "324";
constexpr auto* RPL_NOTOPIC			= "331";
constexpr auto* RPL_TOPIC			= "332";
constexpr auto* RPL_INVITING		= "341";
constexpr auto* RPL_SUMMONING		= "342";
constexpr auto* RPL_VERSION			= "351";
constexpr auto* RPL_WHOREPLY		= "352";
constexpr auto* RPL_ENDOFWHO		= "315";
constexpr auto* RPL_NAMREPLY		= "353";
constexpr auto* RPL_ENDOFNAMES		= "366";
constexpr auto* RPL_LINKS			= "364";
constexpr auto* RPL_ENDOFLINKS		= "365";
constexpr auto* RPL_BANLIST			= "367";
constexpr auto* RPL_ENDOFBANLIST	= "368";
constexpr auto* RPL_INFO			= "371";
constexpr auto* RPL_ENDOFINFO		= "374";
constexpr auto* RPL_MOTDSTART		= "375";
constexpr auto* RPL_MOTD			= "372";
constexpr auto* RPL_ENDOFMOTD		= "376";
constexpr auto* RPL_YOUREOPER		= "381";
constexpr auto* RPL_REHASHING		= "382";
constexpr auto* RPL_TIME			= "391";
constexpr auto* RPL_USERSSTART		= "392";
constexpr auto* RPL_USERS			= "393";
constexpr auto* RPL_ENDOFUSERS		= "394";
constexpr auto* RPL_NOUSERS			= "395";
constexpr auto* RPL_TRACELINK		= "200";
constexpr auto* RPL_TRACECONNECTING = "201";
constexpr auto* RPL_TRACEHANDSHAKE  = "202";
constexpr auto* RPL_TRACEUNKNOWN	= "203";
constexpr auto* RPL_TRACEOPERATOR   = "204";
constexpr auto* RPL_TRACEUSER		= "205";
constexpr auto* RPL_TRACESERVER		= "206";
constexpr auto* RPL_TRACENEWTYPE	= "208";
constexpr auto* RPL_TRACELOG		= "261";
constexpr auto* RPL_STATSLINKINFO   = "211";
constexpr auto* RPL_STATSCOMMANDS   = "212";
constexpr auto* RPL_STATSCLINE		= "213";
constexpr auto* RPL_STATSNLINE		= "214";
constexpr auto* RPL_STATSILINE		= "215";
constexpr auto* RPL_STATSKLINE		= "216";
constexpr auto* RPL_STATSYLINE		= "218";
constexpr auto* RPL_ENDOFSTATS		= "219";
constexpr auto* RPL_STATSLLINE		= "241";
constexpr auto* RPL_STATSUPTIME		= "242";
constexpr auto* RPL_STATSOLINE		= "243";
constexpr auto* RPL_STATSHLINE		= "244";
constexpr auto* RPL_UMODEIS			= "221";
constexpr auto* RPL_LUSERCLIENT		= "251";
constexpr auto* RPL_LUSEROP			= "252";
constexpr auto* RPL_LUSERUNKNOWN	= "253";
constexpr auto* RPL_LUSERCHANNELS   = "254";
constexpr auto* RPL_LUSERME			= "255";
constexpr auto* RPL_ADMINME			= "256";
constexpr auto* RPL_ADMINLOC1		= "257";
constexpr auto* RPL_ADMINLOC2		= "258";
constexpr auto* RPL_ADMINEMAIL		= "259";
constexpr auto* RPL_INVITELIST		= "346";
constexpr auto* RPL_ENDOFINVITELIST = "347";
constexpr auto* RPL_EXCEPTLIST		= "348";
constexpr auto* RPL_ENDOFEXCEPTLIST = "349";

/*
 * Reserved reply numerics
 *
 * From RFC1459: These numerics are not described above since they fall into
 * one of the following categories: \n
 * 1. no longer in use \n
 * 2. reserved for future planned use \n
 * 3. in current use but are part of a non-generic 'feature' of the current IRC server.
*/
constexpr auto* RPL_WELCOME			= "001";
constexpr auto* RPL_MYINFO			= "004";
constexpr auto* RPL_ISUPPORT		= "005";
constexpr auto* RPL_TRACECLASS		= "209";
constexpr auto* RPL_SERVICEINFO		= "231";
constexpr auto* RPL_SERVICE			= "233";
constexpr auto* RPL_SERVLISTEND		= "235";
constexpr auto* RPL_WHOISCHANOP		= "316";
constexpr auto* RPL_CLOSING			= "362";
constexpr auto* RPL_INFOSTART		= "373";
constexpr auto* ERR_YOUWILLBEBANNED = "466";
constexpr auto* ERR_NOSERVICEHOST   = "492";
constexpr auto* RPL_STATSQLINE		= "217";
constexpr auto* RPL_ENDOFSERVICES   = "232";
constexpr auto* RPL_SERVLIST		= "234";
constexpr auto* RPL_KILLDONE		= "361";
constexpr auto* RPL_CLOSEEND		= "363";
constexpr auto* RPL_MYPORTIS		= "384";
constexpr auto* RPL_BADCHANMASK		= "476";
constexpr auto* RPL_CREATION		= "329";
constexpr auto* RPL_TOPICBY			= "333";
constexpr auto* RPL_WHOISACTUALHOST = "378";
constexpr auto* RPL_WHOISMODES		= "379";
constexpr auto* RPL_WHOISIDENTIFIED = "307";
constexpr auto* RPL_WHOISLOGGEDIN   = "330";
constexpr auto* RPL_WHOISHELP		= "310";
constexpr auto* RPL_DISPLAYEDHOST   = "396";

} // namespace Numeric

#endif // NUMERICS_H
