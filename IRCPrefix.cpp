/*
 * IdealIRC - Internet Relay Chat client
 * Copyright (C) 2019  Tom-Andre Barstad
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "IRCPrefix.h"
#include <QStringList>

IRCPrefix::IRCPrefix(const QString& prefix)
{
	if (prefix.contains('@')) {
		m_type = Type::user;
		QStringList nickname_userhost = prefix.split('!');
		QStringList user_host = nickname_userhost[1].split('@');
		m_nickname = nickname_userhost[0];
		m_user = user_host[0];
		m_host = user_host[1];
	}
	else {
		m_type = Type::server;
		m_servername = prefix;
	}
}

QString IRCPrefix::toString() const
{
	if (m_type == Type::server)
		return m_servername;
	else
		return m_nickname;
}

const QString& IRCPrefix::getServername() const
{
	return m_servername;
}

const QString& IRCPrefix::getNickname() const
{
	return m_nickname;
}

const QString& IRCPrefix::getUser() const
{
	return m_user;
}

const QString& IRCPrefix::getHost() const
{
	return m_host;
}

QString IRCPrefix::compositeUserinfo() const
{
	return QString("%1!%2@%3")
			.arg(m_nickname)
			.arg(m_user)
			.arg(m_host);
}

IRCPrefix::Type IRCPrefix::getType() const
{
	return m_type;
}
