/*
 * IdealIRC - Internet Relay Chat client
 * Copyright (C) 2019  Tom-Andre Barstad
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef IDEALIRC_H
#define IDEALIRC_H

#include "MdiManager.h"
#include "IConfig/IConfig.h"
#include "AboutIIRC.h"
#include <QMainWindow>
#include <QShowEvent>
#include <QCloseEvent>
#include <QSystemTrayIcon>

namespace Ui {
class IdealIRC;
}

class IdealIRC : public QMainWindow
{
	Q_OBJECT

public:
	explicit IdealIRC(QWidget* parent = nullptr);
	~IdealIRC();

private slots:
	void on_actionConnect_triggered();
	void on_actionOptions_triggered();
	void on_actionAbout_IdealIRC_triggered();

private:
	enum class ConnectButtonState {
		Connect,
		Disconnect
	};

	void showEvent(QShowEvent*);
	void closeEvent(QCloseEvent* evt);
	void subwindowSwitched();
	void updateConnectButtonState();
	void setConnectButtonState(ConnectButtonState state);
	void connectToServer(bool newStatus);
	void disconnectFromServer();

	Ui::IdealIRC* ui;
	MdiManager* mdiManager;
	QSystemTrayIcon* trayIcon;
	IConfig confDlg;
	AboutIIRC aboutDlg;
};

#endif // IDEALIRC_H
