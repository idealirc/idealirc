#ifndef COLORCONFIG_H
#define COLORCONFIG_H

#include <QWidget>
#include <QPaintEvent>
#include <QMouseEvent>
#include <QHBoxLayout>
#include <QToolButton>
#include <QLineEdit>
#include <QRect>
#include <QHash>
#include <QColorDialog>
#include <optional>

class ColorConfig : public QWidget
{
	Q_OBJECT
public:
	explicit ColorConfig(QWidget* parent = nullptr);
	void save();
	bool isChanged() const;
	void reset();

private:
	void paintEvent(QPaintEvent*);
	void mouseReleaseEvent(QMouseEvent *evt);
	void createMessageBoxTexts(QPainter& paint);
	void createMemberListTexts(QPainter& paint, int listboxW);
	void createInputBoxTexts(QPainter& paint, int inputH);
	void recalculateBB();
	QString descriptiveColorText(const QString& key) const;
	void chooseColorFor(const QString& item);
	void colorSelected(const QColor& color);
	std::optional<QColor> getPrefixColor(QChar prefix);
	void setPrefixColor(QChar prefix, const QColor& color);
	void addPrefixClicked();
	void askDeletePrefix(QChar prefix);

	QWidget* addPrefixContainer;
	QHBoxLayout* addPrefixLayout;
	QToolButton* btnAddPrefix;
	QLineEdit* edAddPrefix;
	QHash<QString,QString> palette;

	QHash<QString,QString> textTypeMap;
	QHash<QString,QString> colorTypeMap;
	QVector<std::pair<QChar,QColor>> prefixColor; // Use vector for displaying prefixes in insertion order.

	QHash<QString,QRect> textTypeBB;
	QHash<QString,QRect> listboxItemBB;
	QHash<QChar,QRect> prefixDeleteBB;
	QRect textViewBB;
	QRect inputBB;
	QRect inputTextBB;
	QRect listboxBB;

	QColorDialog colorDlg;
	QString colorDlgItem;
	QString originalColor;

	bool m_isChanged{ false };
};

#endif // COLORCONFIG_H
