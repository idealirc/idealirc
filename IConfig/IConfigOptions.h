/*
 * IdealIRC - Internet Relay Chat client
 * Copyright (C) 2019  Tom-Andre Barstad
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef ICONFIGOPTIONS_H
#define ICONFIGOPTIONS_H

#include "ColorConfig.h"
#include <QWidget>
#include <QHBoxLayout>
#include <QVBoxLayout>

namespace Ui {
class IConfigOptions;
}

class IConfigOptions : public QWidget
{
	Q_OBJECT

public:
	explicit IConfigOptions(QWidget *parent = nullptr);
	~IConfigOptions();
	bool isChanged() const;
	void save();
	void reset();

private slots:
	void on_chkSSLExpired_toggled(bool checked);

	void on_btnImageBrowse_clicked();

private:
	void reload();
	Ui::IConfigOptions *ui;
	QVBoxLayout *layout;
	ColorConfig *colorCfg;
	bool cf_ShowOptions;
	bool cf_Reconnect;
	bool cf_RejoinChannelsOnConnect;
	bool cf_ShowWhoisActiveWindow;
	bool cf_ShowModeInMessage;
	bool cf_TrayNotify;
	int cf_TrayNotifyDelay;
	bool cf_ShowTimestamp;
	QString cf_TimestampFormat;
	QString cf_QuitMessage;
	QFont cf_Font;
	int cf_FontSize;
	bool cf_BgImageEnabled;
	QString cf_BgImagePath;
	int cf_BgImageOpacity;
	// TODO background image scaling combo box
	bool cf_SSLSelfSigned;
	bool cf_SSLExpired;
	bool cf_SSLCNMismatch;
};

#endif // ICONFIGOPTIONS_H
