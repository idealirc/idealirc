/*
 * IdealIRC - Internet Relay Chat client
 * Copyright (C) 2019  Tom-Andre Barstad
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "IConfigOptions.h"
#include "ui_IConfigOptions.h"
#include "ConfigMgr.h"
#include "config.h"
#include <QMessageBox>
#include <QFileDialog>

IConfigOptions::IConfigOptions(QWidget *parent) :
	QWidget(parent),
	ui(new Ui::IConfigOptions)
{
	ui->setupUi(this);
	layout = new QVBoxLayout(ui->tabColors);
	colorCfg = new ColorConfig;
	{
		QLabel* bgcolorhintlabel = new QLabel("Click on a text type to change its color.\nClick anywhere on the backgrounds to change background color.");
		QFont labelFont = font();
		bgcolorhintlabel->setAlignment(Qt::AlignCenter);
		labelFont.setPointSize(8);
		bgcolorhintlabel->setFont(labelFont);
		layout->addWidget(bgcolorhintlabel);
	}
	layout->addWidget(colorCfg);
	colorCfg->setSizePolicy(QSizePolicy::Policy::Expanding, QSizePolicy::Policy::Expanding);
	ui->tabColors->setLayout(layout);

	/* Background opacity is not yet determined */
	ui->label_4->hide();
	ui->hsImageOpacity->hide();

	/* Background scaling is not yet determined */
	ui->label_5->hide();
	ui->edImageScaling->hide();

	reload();
	reset();
}

IConfigOptions::~IConfigOptions()
{
	delete ui;
}

bool IConfigOptions::isChanged() const
{
	bool changed = cf_ShowOptions != ui->chkShowOptions->isChecked()
					|| cf_Reconnect != ui->chkReconnect->isChecked()
					|| cf_RejoinChannelsOnConnect != ui->chkRejoinConnect->isChecked()
					|| cf_ShowWhoisActiveWindow != ui->chkWhoisActive->isChecked()
					|| cf_ShowModeInMessage != ui->chkShowModeMsg->isChecked()
					|| cf_TrayNotify != ui->chkTrayNotify->isChecked()
					|| cf_TrayNotifyDelay != ui->edTrayDelay->value()
					|| cf_ShowTimestamp != ui->chkTimestamp->isChecked()
					|| cf_TimestampFormat != ui->edTimestamp->text()
					|| cf_QuitMessage != ui->edQuit->text()
					|| cf_Font.family() != ui->edFont->currentFont().family()
					|| cf_FontSize != ui->edFontSize->value()
					|| cf_BgImageEnabled != ui->chkEnableBgImage->isChecked()
					|| cf_BgImagePath != ui->edImage->text()
					// TODO background image scaling combo box
					|| cf_BgImageOpacity != ui->hsImageOpacity->value()
					|| cf_SSLSelfSigned != ui->chkSSLSelfsigned->isChecked()
					|| cf_SSLExpired != ui->chkSSLExpired->isChecked()
					|| cf_SSLCNMismatch != ui->chkSSLCNMismatch->isChecked();

	if (changed)
		return true;
	else
		return colorCfg->isChanged();
}

void IConfigOptions::save()
{
	ConfigMgr& conf = ConfigMgr::instance();
	conf.setCommon("ShowOptions", QString::number(ui->chkShowOptions->isChecked()));
	conf.setCommon("Reconnect", QString::number(ui->chkReconnect->isChecked()));
	conf.setCommon("RejoinChannelsOnConnect", QString::number(ui->chkRejoinConnect->isChecked()));
	conf.setCommon("ShowWhoisActiveWindow", QString::number(ui->chkWhoisActive->isChecked()));
	conf.setCommon("ShowModeInMessage", QString::number(ui->chkShowModeMsg->isChecked()));
	conf.setCommon("TrayNotify", QString::number(ui->chkTrayNotify->isChecked()));
	conf.setCommon("TrayNotifyDelay", QString::number(ui->edTrayDelay->value()));
	conf.setCommon("ShowTimestamp", QString::number(ui->chkTimestamp->isChecked()));
	conf.setCommon("TimestampFormat", ui->edTimestamp->text());
	conf.setCommon("QuitMessage", ui->edQuit->text());
	conf.setCommon("Font", ui->edFont->currentFont().family());
	conf.setCommon("FontSize", QString::number(ui->edFontSize->value()));
	conf.setCommon("BgImageEnabled", QString::number(ui->chkEnableBgImage->isChecked()));
	conf.setCommon("BgImagePath", ui->edImage->text());
	conf.setCommon("BgImageOpacity", QString::number(ui->hsImageOpacity->value()));
	conf.setCommon("SSLSelfsigned", QString::number(ui->chkSSLSelfsigned->isChecked()));
	conf.setCommon("SSLExpired", QString::number(ui->chkSSLExpired->isChecked()));
	conf.setCommon("SSLCNMismatch", QString::number(ui->chkSSLCNMismatch->isChecked()));
	reload();
	colorCfg->save();
}

void IConfigOptions::reset()
{
	ui->chkShowOptions->setChecked(cf_ShowOptions);
	ui->chkReconnect->setChecked(cf_Reconnect);
	ui->chkRejoinConnect->setChecked(cf_RejoinChannelsOnConnect);
	ui->chkWhoisActive->setChecked(cf_ShowWhoisActiveWindow);
	ui->chkShowModeMsg->setChecked(cf_ShowModeInMessage);
	ui->chkTrayNotify->setChecked(cf_TrayNotify);
	ui->edTrayDelay->setValue(cf_TrayNotifyDelay);
	ui->chkTimestamp->setChecked(cf_ShowTimestamp);
	ui->edTimestamp->setText(cf_TimestampFormat);
	ui->edQuit->setText(cf_QuitMessage);
	ui->edFont->setCurrentFont(cf_Font);
	ui->edFontSize->setValue(cf_FontSize);
	ui->chkEnableBgImage->setChecked(cf_BgImageEnabled);
	ui->edImage->setText(cf_BgImagePath);
	ui->hsImageOpacity->setValue(cf_BgImageOpacity);
	ui->chkSSLSelfsigned->setChecked(cf_SSLSelfSigned);
	ui->chkSSLExpired->setChecked(cf_SSLExpired);
	ui->chkSSLCNMismatch->setChecked(cf_SSLCNMismatch);

	colorCfg->reset();
}

void IConfigOptions::on_chkSSLExpired_toggled(bool checked)
{
	if (checked)
		QMessageBox::warning(this, tr("Expired SSL certificates"),
							 tr("Allowing expired certificates is dangerous!\nThis option will revert after next connection."));
}

void IConfigOptions::reload()
{
	ConfigMgr& conf = ConfigMgr::instance();
	cf_ShowOptions = conf.common("ShowOptions").toInt();
	cf_Reconnect = conf.common("Reconnect").toInt();
	cf_RejoinChannelsOnConnect = conf.common("RejoinChannelsOnConnect").toInt();
	cf_ShowWhoisActiveWindow = conf.common("ShowWhoisActiveWindow").toInt();
	cf_ShowModeInMessage = conf.common("ShowModeInMessage").toInt();
	cf_TrayNotify = conf.common("TrayNotify").toInt();
	cf_TrayNotifyDelay = conf.common("TrayNotifyDelay").toInt();
	cf_ShowTimestamp = conf.common("ShowTimestamp").toInt();
	cf_TimestampFormat = conf.common("TimestampFormat");
	cf_QuitMessage = conf.common("QuitMessage");
	cf_Font = conf.common("Font");
	cf_FontSize = conf.common("FontSize").toInt();
	cf_BgImageEnabled = conf.common("BgImageEnabled").toInt();
	cf_BgImagePath = conf.common("BgImagePath");
	cf_BgImageOpacity = conf.common("BgImageOpacity").toInt();
	// TODO  background image scaling combo box from config
	cf_SSLSelfSigned = conf.common("SSLSelfsigned").toInt();
	cf_SSLExpired = conf.common("SSLExpired").toInt();
	cf_SSLCNMismatch = conf.common("SSLCNMismatch").toInt();
}

void IConfigOptions::on_btnImageBrowse_clicked()
{
	QString path = QFileDialog::getOpenFileName(this, tr("Choose background image"), LOCAL_PATH, tr("Image Files (*.png *.jpg *.bmp)"));
	ui->edImage->setText(path);
}
