/*
 * IdealIRC - Internet Relay Chat client
 * Copyright (C) 2019  Tom-Andre Barstad
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "IConfigLogging.h"
#include "ui_IConfigLogging.h"
#include "ConfigMgr.h"
#include "config.h"
#include <QFileDialog>
#include <QDebug>

IConfigLogging::IConfigLogging(QWidget *parent) :
	QWidget(parent),
	ui(new Ui::IConfigLogging)
{
	ui->setupUi(this);
	ui->splitter->setStretchFactor(0, 1);
	ui->splitter->setStretchFactor(1, 4);

	ConfigMgr& conf = ConfigMgr::instance();
	cf_Chnanels = conf.logging("Channels").toInt();
	cf_Privates = conf.logging("Privates").toInt();
	cf_Path = conf.logging("Path");

	ui->chkChannels->setChecked(cf_Chnanels);
	ui->chkPrivates->setChecked(cf_Privates);
	ui->edPath->setText(cf_Path);
}

IConfigLogging::~IConfigLogging()
{
	delete ui;
}

bool IConfigLogging::isChanged() const
{
	return cf_Chnanels != ui->chkChannels->isChecked()
			|| cf_Privates != ui->chkPrivates->isChecked()
			|| cf_Path != ui->edPath->text();
}

void IConfigLogging::save()
{
	ConfigMgr& conf = ConfigMgr::instance();
	conf.setLogging("Channels", QString::number(ui->chkChannels->isChecked()));
	conf.setLogging("Privates", QString::number(ui->chkPrivates->isChecked()));
	conf.setLogging("Path", ui->edPath->text());

	cf_Chnanels = conf.logging("Channels").toInt();
	cf_Privates = conf.logging("Privates").toInt();
	cf_Path = conf.logging("Path");
}

void IConfigLogging::reset()
{
	ui->chkChannels->setChecked(cf_Chnanels);
	ui->chkPrivates->setChecked(cf_Privates);
	ui->edPath->setText(cf_Path);
}

void IConfigLogging::showEvent(QShowEvent* evt)
{
	QWidget::showEvent(evt);
	if (!ui->edPath->text().isEmpty())
		loadFiles(ui->edPath->text());
}

void IConfigLogging::on_btnBrowse_clicked()
{
	QString dirstr = QFileDialog::getExistingDirectory(this, tr("Log directory"), LOCAL_PATH);
	loadFiles(dirstr);
}

void IConfigLogging::loadFiles(const QString& path)
{
	ui->fileList->clear();
	ui->edPath->setText(path);
	if (path.isEmpty())
		return;
	QDir dir(path);
	QStringList files = dir.entryList(QDir::Files, QDir::Name);
	ui->fileList->addItems(files);
}

void IConfigLogging::on_edPath_textChanged(const QString &arg1)
{
	loadFiles(ui->edPath->text());
}

void IConfigLogging::on_fileList_itemClicked(QListWidgetItem *item)
{
	if (!item)
		return;

	QString filePath = ui->edPath->text() + "/" + item->text();
	QFile f(filePath);
	if (!f.open(QIODevice::ReadOnly)) {
		qWarning() << "Unable to open log file" << filePath;
		return;
	}
	QByteArray data = f.readAll();
	f.close();
	ui->logView->setPlainText(data);
}
