/*
 * IdealIRC - Internet Relay Chat client
 * Copyright (C) 2019  Tom-Andre Barstad
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef SERVEREDITOR_H
#define SERVEREDITOR_H

#include "ServerModel.h"
#include <QDialog>
#include <QMenu>

namespace Ui {
class ServerEditor;
}

class ServerEditor : public QDialog
{
	Q_OBJECT

public:
	explicit ServerEditor(ServerModel& model, QWidget *parent = nullptr);
	~ServerEditor();

private slots:
	void on_addServerAction_triggered();
	void on_addNetworkAction_triggered();
	void on_btnDel_clicked();
	void on_btnShowPassword_toggled(bool checked);
	void on_btnSave_clicked();
	void on_serverView_clicked(const QModelIndex &index);

private:
	enum class EditMode {
		Off,
		Server,
		Network
	} editMode = EditMode::Off;

	void enableForServer();
	void enableForNetwork();
	void enableAll();
	void disableAll();

	void saveNetworkEdit();
	void saveServerEdit();

	void selectItem(const QModelIndex& index);

	Ui::ServerEditor* ui;
	QMenu* addMenu;
	QAction* addServerAction;
	QAction* addNetworkAction;
	ServerModel& smodel;
};

#endif // SERVEREDITOR_H
