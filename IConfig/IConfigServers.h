/*
 * IdealIRC - Internet Relay Chat client
 * Copyright (C) 2019  Tom-Andre Barstad
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef ICONFIGSERVERS_H
#define ICONFIGSERVERS_H

#include "ServerEditor.h"
#include "ServerModel.h"
#include <QWidget>

namespace Ui {
class IConfigServers;
}

class IConfigServers : public QWidget
{
	Q_OBJECT

public:
	explicit IConfigServers(QWidget *parent = nullptr);
	~IConfigServers();
	bool isChanged() const;
	bool connectToNewStatus() const;
	void unsetConnectToNewStatus();
	void save();
	void reset();

private slots:
	void on_btnShowPassword_toggled(bool checked);
	void on_btnEditServers_clicked();
	void on_servers_clicked(const QModelIndex &index);

private:
	Ui::IConfigServers *ui;
	ServerModel smodel;
	ServerEditor *editor;
	QString cf_Realname;
	QString cf_Username;
	QString cf_Nickname;
	QString cf_AltNickame;
	QString cf_Server;
	QString cf_Password;
	bool cf_SSL;
};

#endif // ICONFIGSERVERS_H
