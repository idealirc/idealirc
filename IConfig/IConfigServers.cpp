/*
 * IdealIRC - Internet Relay Chat client
 * Copyright (C) 2019  Tom-Andre Barstad
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "IConfigServers.h"
#include "ui_IConfigServers.h"
#include "ConfigMgr.h"
#include <QDebug>

IConfigServers::IConfigServers(QWidget *parent) :
	QWidget(parent),
	ui(new Ui::IConfigServers)
{
	ui->setupUi(this);

	ConfigMgr& conf = ConfigMgr::instance();
	cf_Realname = conf.connection("Realname");
	cf_Username = conf.connection("Username");
	cf_Nickname = conf.connection("Nickname");
	cf_AltNickame = conf.connection("AltNickname");
	cf_Server = conf.connection("Server");
	cf_Password = conf.connection("Password");
	cf_SSL = conf.connection("SSL").toInt();

	ui->edRealName->setText(cf_Realname);
	ui->edUsername->setText(cf_Username);
	ui->edNickname->setText(cf_Nickname);
	ui->edAltNickname->setText(cf_AltNickame);
	ui->edServer->setText(cf_Server);
	ui->edServerPassword->setText(cf_Password);
	ui->chkSSL->setChecked(cf_SSL);

	editor = new ServerEditor(smodel, this);
	ui->servers->setModel(&smodel);

	QModelIndex sindex = smodel.indexFromHost(cf_Server);
	if (sindex.isValid())
		ui->servers->selectionModel()->setCurrentIndex(sindex, QItemSelectionModel::Select | QItemSelectionModel::Rows);
}

IConfigServers::~IConfigServers()
{
	delete ui;
}

bool IConfigServers::isChanged() const
{
	return cf_Realname != ui->edRealName->text()
			|| cf_Username != ui->edUsername->text()
			|| cf_Nickname != ui->edNickname->text()
			|| cf_AltNickame != ui->edAltNickname->text()
			|| cf_Server != ui->edServer->text()
			|| cf_Password != ui->edServerPassword->text()
							  || cf_SSL != ui->chkSSL->isChecked();
}

bool IConfigServers::connectToNewStatus() const
{
	return ui->chkNewStatus->isChecked();
}

void IConfigServers::unsetConnectToNewStatus()
{
	ui->chkNewStatus->setChecked(false);
}

void IConfigServers::save()
{
	ConfigMgr& conf = ConfigMgr::instance();
	conf.setConnection("Realname", ui->edRealName->text());
	conf.setConnection("Username", ui->edUsername->text());
	conf.setConnection("Nickname", ui->edNickname->text());
	conf.setConnection("AltNickname", ui->edAltNickname->text());
	conf.setConnection("Server", ui->edServer->text());
	conf.setConnection("Password", ui->edServerPassword->text());
	conf.setConnection("SSL", QString::number(ui->chkSSL->isChecked()));

	cf_Realname = conf.connection("Realname");
	cf_Username = conf.connection("Username");
	cf_Nickname = conf.connection("Nickname");
	cf_AltNickame = conf.connection("AltNickname");
	cf_Server = conf.connection("Server");
	cf_Password = conf.connection("Password");
	cf_SSL = conf.connection("SSL").toInt();
}

void IConfigServers::reset()
{
	ui->edRealName->setText(cf_Realname);
	ui->edUsername->setText(cf_Username);
	ui->edNickname->setText(cf_Nickname);
	ui->edAltNickname->setText(cf_AltNickame);
	ui->edServer->setText(cf_Server);
	ui->edServerPassword->setText(cf_Password);
}


void IConfigServers::on_btnShowPassword_toggled(bool checked)
{
	ui->edServerPassword->setEchoMode(checked ? QLineEdit::Normal : QLineEdit::Password);
}

void IConfigServers::on_btnEditServers_clicked()
{
	editor->show();
}

void IConfigServers::on_servers_clicked(const QModelIndex &index)
{
	auto spair = smodel.fromIndex(index);
	QString details = smodel.details(spair.second, spair.first);
	QString server;
	QString password;
	if (details.contains('|')) {
		password = details.split('|')[1];
		details.remove("|"+password);
	}
	server = details;

	ui->edServer->setText(server);
	ui->edServerPassword->setText(password);
}
