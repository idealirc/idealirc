/*
 * IdealIRC - Internet Relay Chat client
 * Copyright (C) 2019  Tom-Andre Barstad
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "ServerEditor.h"
#include "ui_ServerEditor.h"
#include <QDebug>
#include <QMessageBox>
#include <QInputDialog>

ServerEditor::ServerEditor(ServerModel& model, QWidget* parent)
	: QDialog(parent)
	, ui(new Ui::ServerEditor)
	, smodel(model)
{
	ui->setupUi(this);
	addMenu = new QMenu(this);
	addServerAction = addMenu->addAction(tr("Server"), this, &ServerEditor::on_addServerAction_triggered);
	addNetworkAction = addMenu->addAction(tr("Network"), this, &ServerEditor::on_addNetworkAction_triggered);
	ui->btnAdd->setMenu(addMenu);
	ui->serverView->setModel(&smodel);
	ui->serverView->header()->setSectionResizeMode(QHeaderView::Interactive);

	QStringList networks = smodel.networkList();
	ui->edNetwork->addItem("");
	for (const QString& network : networks) {
		if (network == "NONE")
			continue;
		ui->edNetwork->addItem(network);
	}
}

ServerEditor::~ServerEditor()
{
	delete ui;
}

void ServerEditor::on_addServerAction_triggered()
{
	QString network = "NONE";
	if (editMode == EditMode::Network) {
		QModelIndex current = ui->serverView->currentIndex();
		auto spair = smodel.fromIndex(current);
		network = spair.first;
	}
	else if (editMode == EditMode::Server) {
		QModelIndex current = ui->serverView->currentIndex();
		if (current.parent().isValid()) {
			auto spair = smodel.fromIndex(current.parent());
			network = spair.first;
		}
	}

	QString name;
	QString host;
	for (int i = 1 ;; ++i) {
		name = QStringLiteral("New server %1").arg(i);
		host = QStringLiteral("host%1.name:6667").arg(i);
		QString det = smodel.details(name);
		if (det.isEmpty())
			break;
	}
	QModelIndex idx = smodel.addServer(name, host, network);
	selectItem(idx);
}

void ServerEditor::on_addNetworkAction_triggered()
{
	QString name;
	QString host;
	for (int i = 1 ;; ++i) {
		name = QStringLiteral("New network %1").arg(i);
		host = QStringLiteral("irc%1.host.name:6667").arg(i);
		QString det = smodel.details("DEFAULT", name);
		if (det.isEmpty())
			break;
	}
	QModelIndex idx = smodel.addNetwork(name, host);
	ui->edNetwork->addItem(name);
	selectItem(idx);
}

void ServerEditor::on_btnDel_clicked()
{
	if (editMode == EditMode::Off)
		 return;

	QModelIndex current = ui->serverView->currentIndex();
	auto spair = smodel.fromIndex(current);
	QString network = spair.first;
	QString name = spair.second;

	if (editMode == EditMode::Server) {
		if (QMessageBox::question(this, tr("Delete server"), tr("Do you want to delete the server '%1'?").arg(name)) == QMessageBox::No)
			return;
		smodel.delServer(name, network);
	}

	if (editMode == EditMode::Network) {
		name = spair.first;
		auto answer = QMessageBox::question(this, tr("Delete network"), tr("Do you want to delete the network '%1'?\nHitting 'Yes' will keep the servers as orphans.").arg(name), QMessageBox::Yes | QMessageBox::YesAll | QMessageBox::No);
		if (answer == QMessageBox::No)
			return;
		bool keepServers = answer != QMessageBox::YesAll;
		smodel.delNetwork(name, keepServers);
	}

	ui->serverView->clearSelection();
	disableAll();
}

void ServerEditor::on_btnShowPassword_toggled(bool checked)
{
	ui->edPassword->setEchoMode(checked ? QLineEdit::Normal : QLineEdit::Password);
}

void ServerEditor::on_btnSave_clicked()
{
	if (editMode == EditMode::Off)
		 return;

	if (ui->edName->text().isEmpty()) {
		QMessageBox::information(this, tr("Field missing"), tr("No name is specified."));
		return;
	}

	if (ui->edHostname->text().isEmpty()) {
		QMessageBox::information(this, tr("Field missing"), tr("No hostname is specified."));
		return;
	}

	if (editMode == EditMode::Server)
		saveServerEdit();

	else if (editMode == EditMode::Network)
		saveNetworkEdit();
}

void ServerEditor::enableForServer()
{
	enableAll();
	ui->lbNetwork->show();
	ui->edNetwork->show();
	editMode = EditMode::Server;
}

void ServerEditor::enableForNetwork()
{
	enableAll();
	ui->lbNetwork->hide();
	ui->edNetwork->hide();
	editMode = EditMode::Network;
}

void ServerEditor::enableAll()
{
	ui->edName->setEnabled(true);
	ui->edHostname->setEnabled(true);
	ui->edPort->setEnabled(true);
	ui->edPassword->setEnabled(true);
	ui->btnShowPassword->setEnabled(true);
	ui->btnShowPassword->setChecked(false);
	ui->edNetwork->setEnabled(true);
	ui->btnSave->setEnabled(true);
}

void ServerEditor::disableAll()
{
	ui->edName->clear();
	ui->edHostname->clear();
	ui->edPort->setValue(6667);
	ui->edPassword->clear();
	ui->btnShowPassword->setChecked(false);
	ui->edNetwork->clearEditText();

	ui->edName->setEnabled(false);
	ui->edHostname->setEnabled(false);
	ui->edPort->setEnabled(false);
	ui->edPassword->setEnabled(false);
	ui->btnShowPassword->setEnabled(false);
	ui->edNetwork->setEnabled(false);
	ui->btnSave->setEnabled(false);
	ui->lbNetwork->show();
	ui->edNetwork->show();

	editMode = EditMode::Off;
}

void ServerEditor::saveNetworkEdit()
{
	qDebug() << "Save network";

	QModelIndex current = ui->serverView->currentIndex();
	auto spair = smodel.fromIndex(current);
	QString network = spair.first;

	/* Network name changed */
	if (network != ui->edName->text()) {
		QString newNetwork = ui->edName->text();
		if (ui->edNetwork->findText(newNetwork) > -1) {
			QMessageBox::information(this, tr("Duplicate network name"), tr("The specified network name already exist."));
			return;
		}
		smodel.renameNetwork(network, newNetwork);
		int networkIdx = ui->edNetwork->findText(network);
		ui->edNetwork->setItemText(networkIdx, newNetwork);
		network = newNetwork;
	}

	QString details = ui->edHostname->text() + ":" + QString::number(ui->edPort->value());
	QString password = ui->edPassword->text();
	if (!password.isEmpty())
		details += "|" + password;
	smodel.setNetworkServer(network, details);
}

void ServerEditor::saveServerEdit()
{
	qDebug() << "Save server";
	QModelIndex current = ui->serverView->currentIndex();
	auto spair = smodel.fromIndex(current);

	QString name = ui->edName->text();
	QString host = ui->edHostname->text();
	QString port = QString::number(ui->edPort->value());
	QString password = ui->edPassword->text();
	QString network = ui->edNetwork->currentText();
	if (ui->edNetwork->currentIndex() == 0)
		network = "NONE";
	if (spair.first != network) {
		/* New network */
		if (network != "NONE" && ui->edNetwork->findText(network) == -1) {
			bool ok = false;
			QString hostname = QInputDialog::getText(this, tr("New network"), tr("Set hostname:port for the new network %1").arg(network), QLineEdit::Normal, "", &ok);
			if (!ok || hostname.isEmpty())
				return;

			smodel.addNetwork(network, hostname);
			ui->edNetwork->addItem(network);
		}

		QString details = smodel.details(spair.second, spair.first);
		smodel.delServer(spair.second, spair.first);
		QModelIndex idx = smodel.addServer(spair.second, details, network);
		selectItem(idx);
	}

	smodel.setServer(spair.second, host + ":" + port, password, network);
	if (spair.second != name) {
		QString details = smodel.details(spair.second, network);
		smodel.delServer(spair.second, network);
		smodel.resetModel();
		QModelIndex idx = smodel.addServer(name, details, network);
		selectItem(idx);
	}
}

void ServerEditor::selectItem(const QModelIndex& index)
{
	ui->serverView->clearSelection();
	ui->serverView->selectionModel()->setCurrentIndex(index, QItemSelectionModel::Select | QItemSelectionModel::Rows);
	on_serverView_clicked(index);
}

void ServerEditor::on_serverView_clicked(const QModelIndex &index)
{
	if (!index.isValid()) {
		disableAll();
		return;
	}

	auto spair = smodel.fromIndex(index);
	if (spair.first.isEmpty()) {
		disableAll();
		return;
	}

	QString name;
	if (spair.second == "DEFAULT") {
		name = spair.first;
		enableForNetwork();
	}
	else {
		name = spair.second;
		enableForServer();
	}

	QString details = smodel.details(spair.second, spair.first);
	QString host, port, password;
	// Value format: host:port|password
	if (details.contains('|')) {
		password = details.split('|')[1];
		details = details.remove("|"+password);
	}
	if (details.contains(':')) {
		port = details.split(':')[1];
		details = details.remove(":"+port);
	}
	host = details;


	ui->edName->setText(name);
	ui->edHostname->setText(host);
	ui->edPort->setValue(port.toInt());
	ui->edPassword->setText(password);
	int networkUiIndex = ui->edNetwork->findText(spair.first);
	if (networkUiIndex == -1)
		networkUiIndex = 0;
	ui->edNetwork->setCurrentIndex(networkUiIndex);
}
