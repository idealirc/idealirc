/*
 * IdealIRC - Internet Relay Chat client
 * Copyright (C) 2019  Tom-Andre Barstad
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "IConfig.h"
#include "ui_IConfig.h"
#include "ConfigMgr.h"
#include <QMessageBox>

IConfig::IConfig(QWidget *parent) :
	QDialog(parent),
	ui(new Ui::IConfig)
{
	ui->setupUi(this);

	layout = new QHBoxLayout(this);
	servers = new IConfigServers;
	options = new IConfigOptions;
	logging = new IConfigLogging;

	layout->addWidget(servers);
	layout->addWidget(options);
	layout->addWidget(logging);
	ui->frame->setLayout(layout);

	connect(ui->toolServers, &QAbstractButton::released, [this](){ showSubDialog(SubDialogType::Servers); });
	connect(ui->toolOptions, &QAbstractButton::released, [this](){ showSubDialog(SubDialogType::Options); });
	connect(ui->toolLogging, &QAbstractButton::released, [this](){ showSubDialog(SubDialogType::Logging); });

	showSubDialog(SubDialogType::Servers);
}

IConfig::~IConfig()
{
	delete ui;
}

void IConfig::showDisconnectButton()
{
	ui->btnDisconnect->show();
}

void IConfig::hideDisconnectButton()
{
	ui->btnDisconnect->hide();
}

void IConfig::showSubDialog(IConfig::SubDialogType dlg)
{
	servers->hide();
	options->hide();
	logging->hide();
	ui->toolServers->setChecked(false);
	ui->toolOptions->setChecked(false);
	ui->toolLogging->setChecked(false);

	switch (dlg) {
	case SubDialogType::Servers:
		servers->show();
		ui->toolServers->setChecked(true);
		break;
	case SubDialogType::Options:
		options->show();
		ui->toolOptions->setChecked(true);
		break;
	case SubDialogType::Logging:
		logging->show();
		ui->toolLogging->setChecked(true);
		break;
	}
}

void IConfig::saveAll()
{
	servers->save();
	options->save();
	logging->save();

	ConfigMgr::instance().save();
}

bool IConfig::askForSave()
{
	bool changed = servers->isChanged() || options->isChanged() || logging->isChanged();
	if (changed) {
		auto btn = QMessageBox::question(this, tr("Changes made"), tr("Do you want to save the changes?"), QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel);
		if (btn == QMessageBox::Yes) {
			saveAll();
		}
		else if (btn == QMessageBox::No) {
			servers->reset();
			options->reset();
			logging->reset();
		}
		else if (btn == QMessageBox::Cancel) {
			return false;
		}
	}
	return true;
}

void IConfig::on_btnSave_clicked()
{
	saveAll();
}

void IConfig::on_btnSaveConnect_clicked()
{
	saveAll();
	ConfigMgr& conf = ConfigMgr::instance();
	if (conf.connection("Realname").isEmpty()) {
		QMessageBox::information(this, tr("Missing field"), tr("Real name must be filled."));
		return;
	}

	if (conf.connection("Username").isEmpty()) {
		QMessageBox::information(this, tr("Missing field"), tr("Username/email must be filled."));
		return;
	}

	if (conf.connection("Nickname").isEmpty()) {
		QMessageBox::information(this, tr("Missing field"), tr("Nickname must be filled"));
		return;
	}

	emit connectToServer(servers->connectToNewStatus());
	servers->unsetConnectToNewStatus();
	close();
}

void IConfig::on_btnDisconnect_clicked()
{
	ui->btnDisconnect->hide();
	emit disconnectFromServer();
}

void IConfig::on_btnClose_clicked()
{
	servers->unsetConnectToNewStatus();
	if (askForSave())
		close();
}

void IConfig::closeEvent(QCloseEvent* evt)
{
	if (askForSave())
		evt->accept();
	else
		evt->ignore();
}
