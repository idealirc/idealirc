#include "ColorConfig.h"
#include "ConfigMgr.h"
#include <QAction>
#include <QPushButton>
#include <QPainter>
#include <QDebug>
#include <QFontMetrics>
#include <QHashIterator>
#include <QMessageBox>

namespace {
constexpr auto* Text_AddPrefix_Full = "Add prefix";
constexpr auto* Text_AddPrefix_Short = "Add";
}

ColorConfig::ColorConfig(QWidget* parent)
	: QWidget(parent)
	, colorDlg(this)
{
	addPrefixContainer = new QWidget(this);
	addPrefixLayout = new QHBoxLayout;
	btnAddPrefix = new QToolButton;
	btnAddPrefix->setText(Text_AddPrefix_Full);
	edAddPrefix = new QLineEdit;
	edAddPrefix->setMaxLength(1);
	edAddPrefix->setAlignment(Qt::AlignmentFlag::AlignCenter);
	edAddPrefix->hide();
	addPrefixLayout->addWidget(edAddPrefix);
	addPrefixLayout->addWidget(btnAddPrefix);
	addPrefixContainer->setLayout(addPrefixLayout);
	connect(btnAddPrefix, &QToolButton::clicked, this, &ColorConfig::addPrefixClicked);
	connect(edAddPrefix, &QLineEdit::returnPressed, this, &ColorConfig::addPrefixClicked);

	ConfigMgr& conf = ConfigMgr::instance();
	palette = conf.color();
	prefixColor = conf.prefixColor();

	textTypeMap = {
		// Config key, Descriptive text
		{ "Action", "Action/role-play message" },
		{ "CTCP", "CTCP message" },
		{ "Highlight", "Highlighted message" },
		{ "Invite", "Invite message" },
		{ "Join", "Join message" },
		{ "Kick", "Kick message" },
		{ "Mode", "Mode message" },
		{ "Nick", "Nick message" },
		{ "Normal", "Normal message" },
		{ "Notice", "Notice message" },
		{ "OwnText", "Own message" },
		{ "Part", "Part message" },
		{ "ProgramInfo", "Program info" },
		{ "Quit", "Quit message" },
		{ "ServerInfo", "General server info" },
		{ "Topic", "Topic message" },
		{ "Wallops", "Wallops message" },
		{ "Links", "Links / anchors" }
	};

	colorTypeMap = {
		{ "TextviewBackground", "Text view background" },
		{ "InputBackground", "Text input background" },
		{ "InputForeground", "Text input" },
		{ "ListboxBackground", "Member list background" },
		{ "ListboxForeground", "Member list default" }
	};

	connect(&colorDlg, &QColorDialog::currentColorChanged,
			this, &ColorConfig::colorSelected);

	connect(&colorDlg, &QColorDialog::rejected, [this](){
		if (colorDlgItem.left(8) == "lbprefix")
			setPrefixColor(colorDlgItem[9], originalColor);
		else
			palette.insert(colorDlgItem, originalColor);
		repaint();
	});
}

void ColorConfig::save()
{
	ConfigMgr& conf = ConfigMgr::instance();
	conf.setColorPalette(palette);
	conf.setPrefixColorPalette(prefixColor);
	m_isChanged = false;
}

bool ColorConfig::isChanged() const
{
	return m_isChanged;
}

void ColorConfig::reset()
{
	ConfigMgr& conf = ConfigMgr::instance();
	palette = conf.color();
	prefixColor = conf.prefixColor();
	m_isChanged = false;
	repaint();
}

void ColorConfig::paintEvent(QPaintEvent*)
{
	QPainter paint(this);
	int w = width();
	int h = height();

	QColor frame(palette.value("TextviewBackground"));
	frame.setRed(frame.red() ^ 255);
	frame.setGreen(frame.green() ^ 255);
	frame.setBlue(frame.blue() ^ 255);

	paint.fillRect(0, 0, w, h, frame);
	w -= 2;
	h -= 2;

	const int listboxW = 150;
	const int inputH = 24;

	/* Text-view background */
	textViewBB = { 1, 1, w - listboxW, h - inputH };
	paint.fillRect(textViewBB, QColor(palette.value("TextviewBackground")));

	/* Member list background */
	listboxBB = { w - listboxW + 2, 1, listboxW - 1, h - inputH };
	paint.fillRect(listboxBB, QColor(palette.value("ListboxBackground"))); // Listbox

	/* Input background */
	inputBB = { 1, h - inputH + 2, w, inputH - 1 };
	paint.fillRect(inputBB, QColor(palette.value("InputBackground"))); // Input

	ConfigMgr& conf = ConfigMgr::instance();
	QFont font(conf.common("Font"));
	paint.setFont(font);

	createMessageBoxTexts(paint);
	createMemberListTexts(paint, listboxW);
	createInputBoxTexts(paint, inputH);
}

void ColorConfig::mouseReleaseEvent(QMouseEvent* evt)
{
	const int X = evt->x();
	const int Y = evt->y();

	/* Check click for text item types */
	{
		QHashIterator<QString,QRect> it(textTypeBB);
		while (it.hasNext()) {
			it.next();
			const QString& key = it.key();
			const QRect& val = it.value();
			if (val.contains(X, Y)) {
				chooseColorFor(key);
				return;
			}
		}
	}

	/* Check click for listbox items */
	{
		QHashIterator<QString,QRect> it(listboxItemBB);
		while (it.hasNext()) {
			it.next();
			const QString& key = it.key();
			const QRect& val = it.value();
			if (val.contains(X, Y)) {
				chooseColorFor(key);
				return;
			}
		}
	}

	/* Check click for delete listbox item */
	{
		QHashIterator<QChar,QRect> it(prefixDeleteBB);
		while (it.hasNext()) {
			it.next();
			const QChar& key = it.key();
			const QRect& val = it.value();
			if (val.contains(X, Y)) {
				askDeletePrefix(key);
				return;
			}
		}
	}

	if (inputTextBB.contains(X, Y))
		chooseColorFor("InputForeground");

	else if (textViewBB.contains(X, Y))
		chooseColorFor("TextviewBackground");

	else if (listboxBB.contains(X, Y))
		chooseColorFor("ListboxBackground");

	else if (inputBB.contains(X, Y))
		chooseColorFor("InputBackground");
}

void ColorConfig::createMessageBoxTexts(QPainter& paint)
{
	const int itemIncr = 19;
	int itemTop = itemIncr;
	QPen originalPen = paint.pen();
	QFontMetrics fm(paint.fontMetrics());

	textTypeBB.clear();
	QHashIterator<QString,QString> it(textTypeMap);
	QStringList sorted;

	while (it.hasNext())
		sorted << it.next().key();

	std::sort(sorted.begin(), sorted.end(),
		[this](const QString& left, const QString& right) {
			QString leftText = descriptiveColorText(left);
			QString rightText = descriptiveColorText(right);
			return leftText < rightText;
		});

	for (const QString& key : sorted) {
		const QString val = descriptiveColorText(key);

		paint.setPen(QColor(palette.value(key)));
		paint.drawText(4, itemTop, val);

		QRect itemRect(4, itemTop - fm.ascent(), fm.width(val), fm.height());
		textTypeBB.insert(key, itemRect);

		itemTop += itemIncr;
	}

	paint.setPen(originalPen);
}

void ColorConfig::createMemberListTexts(QPainter& paint, int listboxW)
{
	const int x = width() - listboxW + 4;
	const int itemIncr = 20;
	int itemTop = itemIncr;
	QPen originalPen = paint.pen();

	QFontMetrics fm(paint.fontMetrics());
	listboxItemBB.clear();
	prefixDeleteBB.clear();
	paint.setPen(QColor(palette.value("ListboxForeground")));
	paint.drawText(x, itemTop, "Default text");
	listboxItemBB.insert("ListboxForeground", QRect(x, itemTop - fm.ascent(), fm.width("Default text"), fm.height()));
	itemTop += itemIncr;

	constexpr auto* DeleteLabel = "[X]";
	for (const auto& item : prefixColor) {
		QString itemText = QStringLiteral("%1member").arg(item.first);
		int itemTextWidth = fm.width(itemText);
		paint.setPen(item.second);
		paint.drawText(x, itemTop, itemText);
		paint.drawText(x + itemTextWidth + 10, itemTop, DeleteLabel);
		listboxItemBB.insert(QStringLiteral("lbprefix %1").arg(item.first), QRect(x, itemTop - fm.ascent(), itemTextWidth, fm.height()));
		prefixDeleteBB.insert(item.first, QRect(x + itemTextWidth + 10, itemTop - fm.ascent(), fm.width(DeleteLabel), fm.height()));

		itemTop += itemIncr;
	}

	paint.setPen(originalPen);
	addPrefixContainer->move(x, itemTop);
}

void ColorConfig::createInputBoxTexts(QPainter& paint, int inputH)
{
	QFontMetrics fm(paint.fontMetrics());
	const int fh = fm.height();
	const int y = height() - inputH + fh;
	QPen originalPen = paint.pen();

	paint.setPen(QColor(palette.value("InputForeground")));
	paint.drawText(4, y, "Input box text");
	inputTextBB = {4, y - fm.ascent(), fm.width("Input box text"), fm.height()};

	paint.setPen(originalPen);
}

QString ColorConfig::descriptiveColorText(const QString& key) const
{
	if (key.left(8) == "lbprefix")
		return QStringLiteral("Prefix '%1'").arg(key[9]);

	QString text = textTypeMap.value(key);
	if (text.isEmpty())
		text = colorTypeMap.value(key);
	return text;
}

void ColorConfig::chooseColorFor(const QString& item)
{
	colorDlgItem = item;
	if (colorDlgItem.left(8) == "lbprefix")
		colorDlg.setCurrentColor(*getPrefixColor(colorDlgItem[9]));
	else
		colorDlg.setCurrentColor(QColor(palette.value(item)));
	originalColor = colorDlg.currentColor().name();

	colorDlg.setWindowTitle(QStringLiteral("Choose color for: %1").arg(descriptiveColorText(item)));
	if (!colorDlg.isVisible())
		colorDlg.show();
}

void ColorConfig::colorSelected(const QColor& color)
{
	if (colorDlgItem.left(8) == "lbprefix")
		setPrefixColor(colorDlgItem[9], color);
	else
		palette.insert(colorDlgItem, color.name());
	m_isChanged = true;
	repaint();
}

std::optional<QColor> ColorConfig::getPrefixColor(QChar prefix)
{
	for (const auto& item : prefixColor)
		if (item.first == prefix)
			return item.second;
	return std::nullopt;
}

void ColorConfig::setPrefixColor(QChar prefix, const QColor& color)
{
	for (auto& item : prefixColor)
		if (item.first == prefix)
			item.second = color;
}

void ColorConfig::addPrefixClicked()
{
	if (edAddPrefix->isVisible()) {
		if (!edAddPrefix->text().isEmpty()) {
			const QChar prefix = edAddPrefix->text()[0];
			if (getPrefixColor(prefix)) {
				QMessageBox::information(this, tr("Prefix exists"), tr("The given prefix '%1' is already defined.").arg(prefix));
				return;
			}
			if (!prefix.isSpace() && !prefix.isLetter() && !prefix.isNumber()) {
				prefixColor.push_back(std::make_pair(prefix, palette.value("ListboxForeground")));
				m_isChanged = true;
				repaint();
			}
		}
		edAddPrefix->clear();
		edAddPrefix->hide();
		btnAddPrefix->setText(Text_AddPrefix_Full);
	}
	else {
		edAddPrefix->show();
		btnAddPrefix->setText(Text_AddPrefix_Short);
	}
}

void ColorConfig::askDeletePrefix(QChar prefix)
{
	const QString title = tr("Delete prefix");
	const QString question = tr("Are you sure you want to delete the prefix '%1'?").arg(prefix);
	if (QMessageBox::question(this, title, question) == QMessageBox::No)
		return;

	for (int i = 0; i < prefixColor.count(); ++i) {
		QChar p = prefixColor[i].first;
		if (p == prefix) {
			prefixColor.removeAt(i);
			break;
		}
	}
	repaint();
}
