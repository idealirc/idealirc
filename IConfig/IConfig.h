/*
 * IdealIRC - Internet Relay Chat client
 * Copyright (C) 2019  Tom-Andre Barstad
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef ICONFIG_H
#define ICONFIG_H

#include "IConfigServers.h"
#include "IConfigOptions.h"
#include "IConfigLogging.h"
#include <QDialog>
#include <QSignalMapper>
#include <QHBoxLayout>
#include <QCloseEvent>

namespace Ui {
class IConfig;
}

class IConfig : public QDialog
{
	Q_OBJECT

public:
	explicit IConfig(QWidget *parent = nullptr);
	~IConfig();
	void showDisconnectButton();
	void hideDisconnectButton();

private slots:
	void on_btnSave_clicked();
	void on_btnSaveConnect_clicked();
	void on_btnDisconnect_clicked();
	void on_btnClose_clicked();

private:
	enum class SubDialogType {
		Servers,
		Options,
		Logging
	};
	void closeEvent(QCloseEvent* evt);
	void showSubDialog(SubDialogType dlg);
	void saveAll();
	bool askForSave();

	Ui::IConfig* ui;
	QHBoxLayout *layout;
	IConfigServers* servers;
	IConfigOptions* options;
	IConfigLogging* logging;

signals:
	void connectToServer(bool newServer);
	void disconnectFromServer();
};

#endif // ICONFIG_H
