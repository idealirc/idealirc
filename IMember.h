/*
 * IdealIRC - Internet Relay Chat client
 * Copyright (C) 2019  Tom-Andre Barstad
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef IMEMBER_H
#define IMEMBER_H

#include <QString>
#include <QHash>

/**
 * @brief Describes a member of a channel.
*/
class IMember
{
public:
	IMember(const QString& nickname, const QString& ident, const QString& hostname);
	IMember(const QString& userinfo);

	const QString& getNickname() const;
	const QString& getIdent() const;
	const QString& getHostname() const;
	const QString getCompsite() const;
	void setChannelMode(const QString& channel, QChar mode);
	void delChannelMode(const QString& channel, QChar mode);
	bool hasChannelMode(const QString& channel, QChar mode) const;
	QString getChannelModes(const QString& channel) const;
	void clearChannelMode(const QString& channel);

	void setNickname(const QString& nickname);
	void setHostname(const QString& hostname);

	bool operator==(const IMember& other);
	bool operator==(const QString& nickname);

	IMember& operator=(const IMember& other);

private:
	QString m_nickname;
	QString m_ident;
	QString m_hostname;
	QHash<QString,QString> m_channelModes;
};

#endif // IMEMBER_H
