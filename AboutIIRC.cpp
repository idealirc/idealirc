/*
 * IdealIRC - Internet Relay Chat client
 * Copyright (C) 2019  Tom-Andre Barstad
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "AboutIIRC.h"
#include "ui_AboutIIRC.h"
#include "config.h"
#include <QDateTime>

AboutIIRC::AboutIIRC(QWidget *parent) :
	QDialog(parent),
	ui(new Ui::AboutIIRC)
{
	ui->setupUi(this);

	QDateTime cur = QDateTime::currentDateTime();
	QString year = QString::number(cur.date().year());

	QString text = ui->textBrowser->toHtml();
	text = text.replace("{year}", year);
	text = text.replace("{ver}", VERSION_STRING);
	ui->textBrowser->setHtml(text);
}

AboutIIRC::~AboutIIRC()
{
	delete ui;
}
