/*
 * IdealIRC - Internet Relay Chat client
 * Copyright (C) 2019  Tom-Andre Barstad
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#pragma once

#include <iostream>
#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>
#include "../../ICommand/CommandData.h"


QVariant prd_AnyWord(int& idx, const QString& line)
{
	QString ret;
	for (; idx < line.count(); ++idx) {
		const QChar c = line[idx];
		if (c == ' ')
			break;
		ret += c;
	}
	if (ret.isEmpty())
		return QVariant();
	return ret;
}

QVariant prd_NotSwitch(int& idx, const QString& line)
{
	if (line[idx] == '-' || line[idx] == '+')
		return QVariant();
	return prd_AnyWord(idx, line);
}

QVariant prd_Switch(int& idx, const QString& line)
{
	if (line[idx] != '-' && line[idx] != '+')
		return QVariant();
	return prd_AnyWord(idx, line);
}

QVariant prd_Message(int& idx, const QString& line)
{
	QString ret = line.mid(idx);
	if (ret.isEmpty())
		return QVariant();
	return ret;
}

TEST(CommandDataTest, Test1)
{
	QString line = "/test first second some message  here   ok.";
	PredicateList pl{ &prd_AnyWord, &prd_AnyWord, &prd_Message };
	CommandData cd(line, pl);

	ASSERT_EQ(3, cd.size());
	EXPECT_EQ("first", cd[0].toString());
	EXPECT_EQ("second", cd[1].toString());
	EXPECT_EQ("some message  here   ok.", cd[2].toString());
}

TEST(CommandDataTest, Test2)
{
	QString line = "/mode #place +nst";
	PredicateList pl{ &prd_AnyWord, &prd_Switch, &prd_Message };
	CommandData cd(line, pl);

	ASSERT_EQ(2, cd.size());
	EXPECT_EQ("#place", cd[0].toString());
	EXPECT_EQ("+nst", cd[1].toString());
	EXPECT_FALSE(cd[2].isValid());
}

TEST(CommandDataTest, Test3)
{
	QString line = "/mode #place +k mykey";
	PredicateList pl{ &prd_AnyWord, &prd_Switch, &prd_Message };
	CommandData cd(line, pl);

	ASSERT_EQ(3, cd.size());
	EXPECT_EQ("#place", cd[0].toString());
	EXPECT_EQ("+k", cd[1].toString());
	EXPECT_EQ("mykey", cd[2].toString());
}

TEST(CommandDataTest, Test4)
{
	QString line = "/test argument";
	PredicateList pl{ &prd_Switch, &prd_Message };
	CommandData cd(line, pl);

	ASSERT_EQ(2, cd.size());
	EXPECT_FALSE(cd[0].isValid());
	EXPECT_EQ("argument", cd[1].toString());
}

TEST(CommandDataTest, Test5)
{
	QString line = "/test argument other";
	PredicateList pl{ &prd_AnyWord, &prd_Switch, &prd_Message };
	CommandData cd(line, pl);

	ASSERT_EQ(3, cd.size());
	EXPECT_EQ("argument", cd[0].toString());
	EXPECT_FALSE(cd[1].isValid());
	EXPECT_EQ("other", cd[2].toString());
}

TEST(CommandDataTest, Test6)
{
	QString line = "/mode +o someone";
	PredicateList pl{ &prd_NotSwitch, &prd_Switch, &prd_Message };
	CommandData cd(line, pl);

	ASSERT_EQ(3, cd.size());
	EXPECT_FALSE(cd[0].isValid());
	EXPECT_EQ("+o", cd[1].toString());
	EXPECT_EQ("someone", cd[2].toString());

/*
	std::cout << cd.name().toStdString() << " " << cd.size() << std::endl;

	std::cout << cd[0].toString().toStdString() << "\n"
			<< cd[1].toString().toStdString() << "\n"
			<< cd[2].toString().toStdString() << "\n"
			<< std::endl;
*/
}
