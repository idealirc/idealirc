/*
 * IdealIRC - Internet Relay Chat client
 * Copyright (C) 2019  Tom-Andre Barstad
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#pragma once

#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>
#include "../../IServer.h"

using namespace testing;

TEST(IServerGeneral, ChannelCount)
{
	IServer server;
	server.addChannel("#TestersLounge");
	server.addChannel("#CodeCorner");
	server.addChannel("#Helpdesk");
	EXPECT_EQ(3, server.channelCount());

	server.delChannel("#Helpdesk");
	EXPECT_EQ(2, server.channelCount());

	server.delChannel("#CodeCorner");
	EXPECT_EQ(1, server.channelCount());

	server.delChannel("#DontExist");
	EXPECT_EQ(1, server.channelCount());

	server.delChannel("#TestersLounge");
	EXPECT_EQ(0, server.channelCount());
}

TEST(IServerGeneral, MemberCount)
{
	IServer server;
	server.addChannel("#TestersLounge");
	server.addMember("#TestersLounge", "John");
	EXPECT_EQ(1, server.memberCount("#TestersLounge"));
	EXPECT_EQ(1, server.totalMemberCount());

	server.addChannel("#Helpdesk");
	server.addMember("#Helpdesk", "John");
	EXPECT_EQ(1, server.totalMemberCount());
	server.addMember("#Helpdesk", "Olaf");
	EXPECT_EQ(2, server.totalMemberCount());
	EXPECT_EQ(2, server.memberCount("#Helpdesk"));

	server.delMember("#Helpdesk", "John");
	EXPECT_EQ(2, server.totalMemberCount());

	server.delMember("#TestersLounge", "John");
	EXPECT_EQ(1, server.totalMemberCount());

	server.delMember("#TestersLounge", "Olaf"); // Note: Olaf is not in this channel...
	EXPECT_EQ(1, server.totalMemberCount());

	server.delMember("#Helpdesk", "Olaf");
	EXPECT_EQ(0, server.totalMemberCount());
}

TEST(IServerGeneral, NonexistantChannel)
{
	IServer server;
	server.addMember("#Helpdesk", "John");
	EXPECT_EQ(0, server.totalMemberCount());
}

TEST(IServerGeneral, MemberRename)
{
	IServer server;
	server.addChannel("#TestersLounge");
	server.addMember("#TestersLounge", "John");
	server.addMember("#TestersLounge", "Olaf");
	EXPECT_EQ(2, server.totalMemberCount());

	IMember john = *server.getMemberByNickname("John");
	john.setNickname("Jane");
	server.updateMemberByNickname("John", john);

	ASSERT_EQ(std::nullopt, server.getMemberByNickname("John"));
	std::optional<IMember> jane = server.getMemberByNickname("Jane");
	ASSERT_NE(std::nullopt, jane);
	EXPECT_EQ("Jane", jane->getNickname().toStdString());
	EXPECT_EQ(2, server.totalMemberCount());
}

TEST(IMemberGeneral, Composite)
{
	IMember member("John", "doe", "isp.net");
	EXPECT_EQ("John!doe@isp.net", member.getCompsite().toStdString());
}

TEST(IMemberGeneral, Comparison)
{
	IMember member("John", "doe", "isp.net");

	// Operator == is case insensitive
	EXPECT_TRUE(member == "JOHN");

	EXPECT_TRUE(member == member);
}


TEST(IChannelGeneral, Comparison)
{
	IChannel channel("#Helpdesk");

	// Operator == is case insensitive
	EXPECT_TRUE(channel == "#HELPDESK");

	EXPECT_TRUE(channel == channel);
}

