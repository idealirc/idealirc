/*
 * IdealIRC - Internet Relay Chat client
 * Copyright (C) 2019  Tom-Andre Barstad
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef ISERVER_H
#define ISERVER_H

#include "IMember.h"
#include "IChannel.h"
#include <QObject>
#include <QList>
#include <QString>
#include <memory>
#include <optional>
#include <QHash>

namespace IRCv3 {
constexpr auto* USERHOST_IN_NAMES = "userhost-in-names";
constexpr auto* MULTI_PREFIX = "multi-prefix";
} // namespace IRCv3Capabilites

class IServer : public QObject
{
	Q_OBJECT
	friend class NicklistModel;
	friend class IRCReader;

public:
	IServer();
	void resetAll();

	static QStringList IRCv3Capabilites;

	std::shared_ptr<IChannel> addChannel(const QString& channel);
	void addMember(const QString& channel, const QString& userinfo);
	void addMember(std::shared_ptr<IMember> member);
	void addMemberList(const QString& channel, const QStringList& memberlist);
	void delChannel(const QString& channel);
	void delMember(const QString& channel, const QString& userinfo);
	int memberCount(const QString& channel) const;
	int totalMemberCount() const;
	int channelCount() const;

	bool hasIRCv3Capability(const QString& capability) const;
	bool isChannelType(QChar prefix) const;

	const QString& getMyNickname() const { return m_nickname; }
	void setIRCv3Capabilities(const QStringList& caps) { m_ircv3 = caps; }
	void parseISupport(const QString& key, const QString& value);
	const QString& getISupport_Network() const { return isupport.network; }
	char channelModeType(QChar mode) const;

	std::optional<IMember> getMemberByNickname(const QString& nickname);
	bool updateMemberByNickname(const QString& nickname, const IMember& data);

	std::optional<IChannel> getChannelByName(const QString& name);
	bool updateChannelByName(const QString &name, const IChannel& data);
	bool isUserModePrefix(QChar letter) const;
	bool isUserMode(QChar mode) const;
	QChar mostSignificantUserModePrefix(QString prefixes) const;
	QStringList getMemberChannels(const QString& nickname) const;
	QString toMemberPrefix(const QString& modes) const;

	bool lessThan(const QString& left, const QString& right);

signals:
	void memberListReloaded(QString channel);
	void memberListClearedForAll();
	void memberAdded(QString channel, std::shared_ptr<IMember> member);
	void memberChanged(QString nickname, std::shared_ptr<IMember> member);
	void memberRemoved(QString channel, std::shared_ptr<IMember> member);

private:
	struct ISupport {
		QString chanModesA;
		QString chanModesB;
		QString chanModesC;
		QString chanModesD;
		QString chanTypes;
		QString network;
		QHash<QChar,QChar> prefix; //!< mode,prefix
		QString prefixModeOrder; //!< contains prefixes (@%+ etc)
		QHash<QChar,int> maxList;
		int maxModes;

		ISupport() { reset(); }
		void reset();
	} isupport;

	std::shared_ptr<IMember> getMember(const QString& userinfo);
	std::shared_ptr<IChannel> getChannel(const QString& channel) const;
	void clearOrphanedMembers();
	void clearIfOrphaned(std::shared_ptr<IMember> member);

	QList<std::shared_ptr<IMember>> m_members; //!< List of all known members across all channels we are on
	QList<std::shared_ptr<IChannel>> m_channels; //!< List of all channels we are on

	QString m_nickname;
	QStringList m_ircv3;

};

#endif // ISERVER_H
