#ifndef PARSER_H
#define PARSER_H

#include <QString>
#include <QVariant>
#include <vector>
#include <functional>

using PredicateList = std::vector<std::function<QVariant(int& idx, const QString &line)>>;

class CommandData
{
public:
	CommandData(const QString& line, PredicateList argp, bool repeatLastPredicate = false);
	int size() const;
	QString name() const;
	QVariant operator[](int idx);
	QString joinString(QChar sep) const;

private:
	QVariant at(int idx);
	QString m_name;
	QVariantList m_l;
};

#endif // PARSER_H
