#include "CommandData.h"

CommandData::CommandData(const QString& line, PredicateList argp, bool repeatLastPredicate)
{
	m_name = line.split(' ')[0];
	int i = m_name.length();
	if (m_name[0] == '/')
		m_name = m_name.mid(1);

	size_t predicate = 0;
	for (; i < line.length(); ++i) {
		QChar c = line[i];
		if (c == ' ')
			continue;
		else {
			if (predicate >= argp.size() && at(static_cast<int>(predicate)-1).isValid()) {
				if (repeatLastPredicate)
					predicate = argp.size()-1;
				else
					break;
			}
			QVariant parameter;
			parameter = argp[predicate](i, line);
			m_l.push_back(parameter);
			++predicate;
			if (!parameter.isValid())
				--i;
		}
	}
}

int CommandData::size() const
{
	return m_l.size();
}

QString CommandData::name() const
{
	return m_name;
}

QVariant CommandData::operator[](int idx)
{
	return at(idx);
}

QString CommandData::joinString(QChar sep) const
{
	QString ret;
	for (const QVariant& data : m_l) {
		if (!ret.isEmpty())
			ret += sep;
		if (data.isValid())
			ret += data.toString();
	}
	return ret;
}

QVariant CommandData::at(int idx)
{
	if (idx < 0 || idx >= m_l.size())
		return QVariant();
	return m_l[idx];
}
