#include "ConfigMgr.h"
#include "config.h"
#include <QHashIterator>

ConfigMgr& ConfigMgr::instance()
{
	static ConfigMgr inst;
	return inst;
}

// Constructor is private, use static instance()
ConfigMgr::ConfigMgr()
{
	load();
}

void ConfigMgr::load()
{
	IniFile ini(LOCAL_PATH+"/iirc.ini");
	loadGeometry(ini);
	loadConnection(ini);
	loadCommon(ini);
	loadColor(ini);
	loadPrefixColor(ini);
	loadLogging(ini);
}

void ConfigMgr::save()
{
	IniFile ini(LOCAL_PATH+"/iirc.ini");
	saveGeometry(ini);
	saveConnection(ini);
	saveCommon(ini);
	saveColor(ini);
	savePrefixColor(ini);
	saveLogging(ini);
	emit saved();
}

QString ConfigMgr::geometry(const QString& key) const
{
	return geometryData.value(key, "");
}

QString ConfigMgr::connection(const QString& key) const
{
	return connectionData.value(key, "");
}

QString ConfigMgr::common(const QString& key) const
{
	return commonData.value(key, "");
}

QString ConfigMgr::color(const QString& key) const
{
	return colorData.value(key, "");
}

std::optional<QColor> ConfigMgr::prefixColor(const QChar prefix) const
{
	for (const auto& pair : prefixColorData)
		if (pair.first == prefix)
			return std::make_optional(pair.second);
	return std::nullopt;
}

QHash<QString, QString> ConfigMgr::color() const
{
	return colorData;
}

QVector<std::pair<QChar, QColor> > ConfigMgr::prefixColor() const
{
	return prefixColorData;
}

QString ConfigMgr::logging(const QString& key) const
{
	return loggingData.value(key, "");
}

void ConfigMgr::setGeometry(const QString& key, const QString& value)
{
	if (geometryData.contains(key))
		geometryData.insert(key, value);
}

void ConfigMgr::setConnection(const QString& key, const QString& value)
{
	if (connectionData.contains(key))
		connectionData.insert(key, value);
}

void ConfigMgr::setCommon(const QString& key, const QString& value)
{
	if (commonData.contains(key))
		commonData.insert(key, value);
}

void ConfigMgr::setLogging(const QString& key, const QString& value)
{
	if (loggingData.contains(key))
		loggingData.insert(key, value);
}

void ConfigMgr::setColorPalette(const QHash<QString, QString>& palette)
{
	colorData = palette;
}

void ConfigMgr::setPrefixColorPalette(const QVector<std::pair<QChar, QColor>>& palette)
{
	prefixColorData = palette;
}

void ConfigMgr::loadGeometry(IniFile& ini)
{
	geometryData.insert("X", ini.value("Geometry", "X", "-1"));
	geometryData.insert("Y", ini.value("Geometry", "Y", "-1"));
	geometryData.insert("Width", ini.value("Geometry", "Width", "1000"));
	geometryData.insert("Height", ini.value("Geometry", "Height", "768"));
}

void ConfigMgr::loadConnection(IniFile& ini)
{
	connectionData.insert("Realname", ini.value("Connection", "Realname"));
	connectionData.insert("Username", ini.value("Connection", "Username"));
	connectionData.insert("Nickname", ini.value("Connection", "Nickname"));
	connectionData.insert("AltNickname", ini.value("Connection", "AltNickname"));
	connectionData.insert("Server", ini.value("Connection", "Server"));
	connectionData.insert("Password", ini.value("Connection", "Password"));
	connectionData.insert("SSL", ini.value("Connection", "SSL", "0"));
}

void ConfigMgr::loadCommon(IniFile& ini)
{
#if defined(Q_OS_WIN32) | defined(Q_OS_WIN64)
	QString defaultFontName = "Fixedsys";
#else
	QString defaultFontName = "Monospace";
#endif
	commonData.insert("ShowOptions", ini.value("Common", "ShowOptions", "1"));
	commonData.insert("Reconnect", ini.value("Common", "Reconnect", "0"));
	commonData.insert("RejoinChannelsOnConnect", ini.value("Common", "RejoinChannelsOnConnect", "0"));
	commonData.insert("ShowWhoisActiveWindow", ini.value("Common", "ShowWhoisActiveWindow", "1"));
	commonData.insert("ShowModeInMessage", ini.value("Common", "ShowModeInMessage", "1"));
	commonData.insert("TrayNotify", ini.value("Common", "TrayNotify", "1"));
	commonData.insert("TrayNotifyDelay", ini.value("Common", "TrayNotifyDelay", "5"));
	commonData.insert("ShowTimestamp", ini.value("Common", "ShowTimestamp", "1"));
	commonData.insert("TimestampFormat", ini.value("Common", "TimestampFormat", "[HH:mm]"));
	commonData.insert("QuitMessage", ini.value("Common", "QuitMessage"));
	commonData.insert("Font", ini.value("Common", "Font", defaultFontName));
	commonData.insert("FontSize", ini.value("Common", "FontSize", "12"));
	commonData.insert("BgImageEnabled", ini.value("Common", "BgImageEnabled", "0"));
	commonData.insert("BgImagePath", ini.value("Common", "BgImagePath"));
	commonData.insert("BgImageOpacity", ini.value("Common", "BgImageOpacity", "100"));
	commonData.insert("BgImageScaling", ini.value("Common", "BgImageScaling"));
	commonData.insert("SSLSelfsigned", ini.value("Common", "SSLSelfSigned", "0"));
	commonData.insert("SSLExpired", "0");
	commonData.insert("SSLCNMismatch", ini.value("Common", "SSLCNMismatch", "0"));
}

void ConfigMgr::loadColor(IniFile& ini)
{
	colorData.insert("Action", ini.value("Color", "Action", "#840084"));
	colorData.insert("CTCP", ini.value("Color", "CTCP", "#FF0000"));
	colorData.insert("Highlight", ini.value("Color", "Highlight", "#848400"));
	colorData.insert("Invite", ini.value("Color", "Invite", "#008400"));
	colorData.insert("Join", ini.value("Color", "Join", "#008400"));
	colorData.insert("Kick", ini.value("Color", "Kick", "#008400"));
	colorData.insert("Mode", ini.value("Color", "Mode", "#008400"));
	colorData.insert("Nick", ini.value("Color", "Nick", "#008400"));
	colorData.insert("Normal", ini.value("Color", "Normal", "#000000"));
	colorData.insert("Notice", ini.value("Color", "Notice", "#840000"));
	colorData.insert("OwnText", ini.value("Color", "OwnText", "#008484"));
	colorData.insert("Part", ini.value("Color", "Part", "#008400"));
	colorData.insert("ProgramInfo", ini.value("Color", "ProgramInfo", "#000084"));
	colorData.insert("Quit", ini.value("Color", "Quit", "#000084"));
	colorData.insert("ServerInfo", ini.value("Color", "ServerInfo", "#008400"));
	colorData.insert("Topic", ini.value("Color", "Topic", "#008400"));
	colorData.insert("Wallops", ini.value("Color", "Wallops", "#FF0000"));
	colorData.insert("TextviewBackground", ini.value("Color", "TextviewBackground", "#FFFFFF"));
	colorData.insert("InputBackground", ini.value("Color", "InputBackground", "#FFFFFF"));
	colorData.insert("InputForeground", ini.value("Color", "InputForeground", "#000000"));
	colorData.insert("ListboxBackground", ini.value("Color", "ListboxBackground", "#FFFFFF"));
	colorData.insert("ListboxForeground", ini.value("Color", "ListboxForeground", "#000000"));
	colorData.insert("Links", ini.value("Color", "Links", "#0000FF"));
}

void ConfigMgr::loadPrefixColor(IniFile& ini)
{
	QString defaultColor = color("ListboxForeground");
	const int ItemCount = ini.countItems("PrefixColor");
	for (int i = 0; i < ItemCount; ++i) {
		int key = ini.key("PrefixColor", i).toInt(); // Stored as ascii-value of given prefix
		QColor color(ini.value("PrefixColor", i, defaultColor));
		QChar prefix = static_cast<char>(key);
		prefixColorData.push_back(std::make_pair(prefix, color));
	}
}

void ConfigMgr::loadLogging(IniFile &ini)
{
	loggingData.insert("Channels", ini.value("Logging", "Channels", "0"));
	loggingData.insert("Privates", ini.value("Logging", "Privates", "0"));
	loggingData.insert("Path", ini.value("Logging", "Path"));
}

void ConfigMgr::saveGeometry(IniFile& ini)
{
	QHashIterator<QString,QString> it(geometryData);
	while (it.hasNext()) {
		it.next();
		ini.write("Geometry", it.key(), it.value());
	}
}

void ConfigMgr::saveConnection(IniFile& ini)
{
	QHashIterator<QString,QString> it(connectionData);
	while (it.hasNext()) {
		it.next();
		ini.write("Connection", it.key(), it.value());
	}
}

void ConfigMgr::saveCommon(IniFile& ini)
{
	QHashIterator<QString,QString> it(commonData);
	while (it.hasNext()) {
		it.next();
		ini.write("Common", it.key(), it.value());
	}
}

void ConfigMgr::saveColor(IniFile& ini)
{
	QHashIterator<QString,QString> it(colorData);
	while (it.hasNext()) {
		it.next();
		ini.write("Color", it.key(), it.value());
	}
}

void ConfigMgr::savePrefixColor(IniFile& ini)
{
	for (const auto& pair : prefixColorData) {
		int keynum = pair.first.toLatin1();
		ini.write("PrefixColor", QString::number(keynum), pair.second.name());
	}
}

void ConfigMgr::saveLogging(IniFile& ini)
{
	QHashIterator<QString,QString> it(loggingData);
	while (it.hasNext()) {
		it.next();
		ini.write("Logging", it.key(), it.value());
	}
}
