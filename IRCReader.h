/*
 * IdealIRC - Internet Relay Chat client
 * Copyright (C) 2019  Tom-Andre Barstad
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef IRCREADER_H
#define IRCREADER_H

#include "IServer.h"
#include "IRCPrefix.h"
#include "IWin/IWin.h"
#include <QObject>
#include <QString>
#include <QStringList>
#include <optional>

class IWin;
class IWinStatus;
class IConnection;

class IRCReader : public QObject
{
	Q_OBJECT
	friend class IConnection;

public:
	IRCReader(IServer* server, IConnection* connection);
	void parseLine(const QString& line);
	void setConnectingNickname(const QString& nickname) { connectingNickname = nickname; }

private:
	void parseCommand(const std::optional<IRCPrefix>& prefix, const QString& command, const QStringList& params, const QString& message);
	void parseNumeric(const QString& numeric, const QStringList& params, const QString& message);
	void parseIRCv3CAP(const QString& subcommand, const QStringList& params);
	void parseCTCPRequest(const IRCPrefix& prefix, const QString& target, const QString& message);
	void parseCTCPReply(const IRCPrefix& prefix, const QString& message);
	void sendCTCPReply(const IRCPrefix& target, const QString& command, const QString& message);

	void parseISupportList(const QStringList& params);
	void parseChannelMode(const QString& channel, const QString& modes);
	void rejoinChannels();
	QString formatResponse(const QStringList& params, const QString& message);
	void printResponseToStatus(const QStringList& params, const QString& message);
	void printResponseToActive(const QString& numeric, const QStringList& params, const QString& message);
	void printChatMessage(const QString& format, const IRCPrefix& prefix, const QString& target, const QString& message, const PrintType ptype);

	inline void sockwrite(const QString& line);
	inline IWinStatus* status() const;

	constexpr static char CTCPflag { 0x01 };
	IServer* server;
	IConnection* connection;
	QString connectingNickname; /* Nickname used during connection registration. It serves no effect if socket is disconnected. */
	QStringList namesList;

	bool receivingNames{ false };
	bool receivingWhois{ false };
};

#endif // IRCREADER_H
