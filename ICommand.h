#ifndef ICOMMAND_H
#define ICOMMAND_H

#include "ICommand/CommandData.h"
#include <QObject>

class IConnection;
class MdiManager;

class ICommand : QObject
{
	Q_OBJECT

public:
	ICommand(IConnection* connection);

	void parse(QString text);

private:
	bool tryParseIRC(const QString& cmdLine);
	bool tryParseInternal(const QString& cmdLine);
	void print_notEnoughParameters();
	void print_invalidCommandSwitch();
	void print_invalidWindowTypeForCommand();

	/* Predicates for parsing commands */
	static QVariant prd_AnyWord(int& idx, const QString& line);
	static QVariant prd_NotSwitch(int& idx, const QString& line);
	static QVariant prd_Switch(int& idx, const QString& line);
	static QVariant prd_Message(int& idx, const QString& line);

	constexpr static char CTCPflag { 0x01 };
	IConnection* connection;
};

#endif // ICOMMAND_H
