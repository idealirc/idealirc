#include "ICommand.h"
#include "Commands.h"
#include "IConnection/IConnection.h"
#include "IWin/IWinStatus.h"
#include "MdiManager.h"

ICommand::ICommand(IConnection* connection)
	: connection(connection)
{
}

void ICommand::parse(QString text)
{
	if (text.isEmpty())
		return;

	if (text[0] == '/')
		text = text.mid(1);

	if (tryParseIRC(text) || tryParseInternal(text))
		return;
	// else if (tryParseScriptCommand(text))
	//	return;
	else {
		const int nextSpace = text.indexOf(' ');
		if (nextSpace == -1)
			connection->sockwrite(text.toUpper());
		else {
			QString command = text.mid(0, nextSpace).toUpper();
			QString out = QString("%1 %2")
						  .arg(command)
						  .arg(text.mid(nextSpace + 1));
			connection->sockwrite(out);
		}
	}
}

bool ICommand::tryParseIRC(const QString& cmdLine)
{
	int spaceidx = cmdLine.indexOf(' ');
	const QString command = cmdLine.mid(0, (spaceidx > -1) ? spaceidx : cmdLine.length()).toUpper();

	using namespace Command::IRC;
	if (command == PASS) {
		CommandData argv(cmdLine, { &ICommand::prd_Message });
		if (!argv[0].isValid())
			print_notEnoughParameters();
		else
			connection->sockwrite(QStringLiteral("%1 :%2").arg(PASS).arg(argv[0].toString()));
		return true;
	}

	else if (command == NICK) {
		CommandData argv(cmdLine, { &ICommand::prd_Message });
		if (!argv[0].isValid())
			print_notEnoughParameters();
		else {
			if (!connection->isOnline())
				connection->reader->setConnectingNickname(argv[0].toString());
			connection->sockwrite(QStringLiteral("%1 :%2").arg(NICK).arg(argv[0].toString()));
		}
		return true;
	}

	else if (command == OPER) {
		CommandData argv(cmdLine, { &ICommand::prd_AnyWord, &ICommand::prd_AnyWord });
		if (argv.size() < 2)
			print_notEnoughParameters();
		else
			connection->sockwrite(QStringLiteral("%1 %2 %3").arg(OPER).arg(argv[0].toString()).arg(argv[1].toString()));
		return true;
	}

	else if (command == MODE) {
		CommandData argv(cmdLine, { &ICommand::prd_NotSwitch, &ICommand::prd_Switch, &ICommand::prd_Message });
		if (!argv[0].isValid() || !argv[1].isValid()) {
			print_notEnoughParameters();
			return true;
		}
		connection->sockwrite(QStringLiteral("%1 %2").arg(MODE).arg(argv.joinString(' ')));
		return true;
	}

	else if (command == QUIT) {
		CommandData argv(cmdLine, { &ICommand::prd_Message });
		connection->expectDisconnect();
		connection->sockwrite(QStringLiteral("%1 :%2").arg(QUIT).arg(argv[0].toString()));
		return true;
	}

	else if (command == SQUIT) {
		CommandData argv(cmdLine, { &ICommand::prd_AnyWord, &ICommand::prd_Message });
		if (!argv[0].isValid()) {
			print_notEnoughParameters();
			return true;
		}
		connection->sockwrite(QStringLiteral("%1 %2 :%3").arg(SQUIT)
								.arg(argv[0].toString())
								.arg(argv[1].toString()));
		return true;
	}

	else if (command == JOIN) {
		CommandData argv(cmdLine, { &ICommand::prd_AnyWord, &ICommand::prd_AnyWord });
		if (!argv[0].isValid()) {
			print_notEnoughParameters();
			return true;
		}
		connection->sockwrite(QStringLiteral("%1 %2").arg(JOIN)
								.arg(argv[0].toString())
								.arg(argv[1].toString()));
		return true;
	}

	else if (command == PART) {
		CommandData argv(cmdLine, { &ICommand::prd_AnyWord, &ICommand::prd_Message });
		if (!argv[0].isValid()) {
			print_notEnoughParameters();
			return true;
		}

		connection->sockwrite(QStringLiteral("%1 :%2").arg(PART)
								.arg(argv[0].toString())
								.arg(argv[1].toString()));
		return true;
	}

	else if (command == TOPIC) {
		CommandData argv(cmdLine, { &ICommand::prd_Switch, &ICommand::prd_AnyWord, &ICommand::prd_Message });
		if (!argv[1].isValid()) {
			print_notEnoughParameters();
			return true;
		}

		QString channel = argv[1].toString();
		QString sw = argv[0].toString();
		QString topic = argv[2].toString();
		if (sw == "-c") {
			connection->sockwrite(QStringLiteral("%1 %2 :").arg(TOPIC).arg(channel));
			return true;
		}
		connection->sockwrite(QStringLiteral("%1 %2 :%3").arg(TOPIC).arg(channel).arg(topic));
		return true;
	}

	else if (command == NAMES) {
		CommandData argv(cmdLine, { &ICommand::prd_AnyWord });
		if (!argv[0].isValid()) {
			print_notEnoughParameters();
			return true;
		}
		connection->sockwrite(QStringLiteral("%1 %2").arg(NAMES).arg(argv[0].toString()));
		return true;
	}

	else if (command == LIST) {
		CommandData argv(cmdLine, { &ICommand::prd_AnyWord });
		if (!argv[0].isValid()) {
			print_notEnoughParameters();
			return true;
		}
		connection->sockwrite(QStringLiteral("%1 %2").arg(LIST).arg(argv[0].toString()));
		return true;
	}

	else if (command == INVITE) {
		CommandData argv(cmdLine, { &ICommand::prd_AnyWord, &ICommand::prd_AnyWord });
		if (!argv[0].isValid() || !argv[1].isValid()) {
			print_notEnoughParameters();
			return true;
		}
		connection->sockwrite(QStringLiteral("%1 %2 %3").arg(INVITE)
								.arg(argv[0].toString())
								.arg(argv[1].toString()));
		return true;
	}

	else if (command == KICK) {
		CommandData argv(cmdLine, { &ICommand::prd_AnyWord, &ICommand::prd_AnyWord, &ICommand::prd_Message });
		if (!argv[0].isValid() || !argv[1].isValid()) {
			print_notEnoughParameters();
			return true;
		}
		connection->sockwrite(QStringLiteral("%1 %2 %3").arg(KICK)
								.arg(argv[0].toString())
								.arg(argv[1].toString())
								.arg(argv[3].toString()));
		return true;
	}

	else if (command == PRIVMSG) {
		CommandData argv(cmdLine, { &ICommand::prd_AnyWord, &ICommand::prd_Message });
		if (!argv[0].isValid()) {
			print_notEnoughParameters();
			return true;
		}
		connection->sockwrite(QStringLiteral("%1 %2 :%3").arg(PRIVMSG)
								.arg(argv[0].toString())
								.arg(argv[1].toString()));
		return true;
	}

	else if (command == NOTICE) {
		CommandData argv(cmdLine, { &ICommand::prd_AnyWord, &ICommand::prd_Message });
		if (!argv[0].isValid()) {
			print_notEnoughParameters();
			return true;
		}
		connection->sockwrite(QStringLiteral("%1 %2 :%3").arg(NOTICE)
								.arg(argv[0].toString())
								.arg(argv[1].toString()));
		return true;
	}

	else if (command == MOTD) {
		CommandData argv(cmdLine, { &ICommand::prd_AnyWord });
		if (argv[0].isValid())
			connection->sockwrite(QStringLiteral("%1 %2").arg(MOTD)
									.arg(argv[0].toString()));
		else
			connection->sockwrite(MOTD);
		return true;
	}

	else if (command == LUSERS) {
		CommandData argv(cmdLine, { &ICommand::prd_AnyWord });
		if (argv[0].isValid())
			connection->sockwrite(QStringLiteral("%1 %2").arg(LUSERS)
									.arg(argv[0].toString()));
		else
			connection->sockwrite(LUSERS);
		return true;
	}

	else if (command == VERSION) {
		CommandData argv(cmdLine, { &ICommand::prd_AnyWord });
		if (argv[0].isValid())
			connection->sockwrite(QStringLiteral("%1 %2").arg(VERSION)
									.arg(argv[0].toString()));
		else
			connection->sockwrite(VERSION);
		return true;
	}

	else if (command == STATS) {
		CommandData argv(cmdLine, { &ICommand::prd_AnyWord });
		if (argv[0].isValid())
			connection->sockwrite(QStringLiteral("%1 %2").arg(STATS)
									.arg(argv[0].toString()));
		else
			connection->sockwrite(STATS);
		return true;
	}

	else if (command == LINKS) {
		CommandData argv(cmdLine, { &ICommand::prd_AnyWord });
		if (argv[0].isValid())
			connection->sockwrite(QStringLiteral("%1 %2").arg(LINKS)
									.arg(argv[0].toString()));
		else
			connection->sockwrite(LINKS);
		return true;
	}

	else if (command == TIME) {
		CommandData argv(cmdLine, { &ICommand::prd_AnyWord });
		if (argv[0].isValid())
			connection->sockwrite(QStringLiteral("%1 %2").arg(TIME)
									.arg(argv[0].toString()));
		else
			connection->sockwrite(TIME);
		return true;
	}

	else if (command == CONNECT) {
		CommandData argv(cmdLine, { &ICommand::prd_AnyWord });
		if (!argv[0].isValid()) {
			print_notEnoughParameters();
			return true;
		}
		connection->sockwrite(QStringLiteral("%1 %2").arg(CONNECT)
									.arg(argv[0].toString()));
		return true;
	}

	else if (command == TRACE) {
		CommandData argv(cmdLine, { &ICommand::prd_Message });
		if (argv[0].isValid())
			connection->sockwrite(QStringLiteral("%1 %2").arg(TRACE)
									.arg(argv[0].toString()));
		else
			connection->sockwrite(TRACE);
		return true;
	}

	else if (command == ADMIN) {
		CommandData argv(cmdLine, { &ICommand::prd_AnyWord });
		if (argv[0].isValid())
			connection->sockwrite(QStringLiteral("%1 %2").arg(ADMIN)
									.arg(argv[0].toString()));
		else
			connection->sockwrite(ADMIN);
		return true;
	}

	else if (command == INFO) {
		CommandData argv(cmdLine, { &ICommand::prd_AnyWord });
		if (argv[0].isValid())
			connection->sockwrite(QStringLiteral("%1 %2").arg(INFO)
									.arg(argv[0].toString()));
		else
			connection->sockwrite(INFO);
		return true;
	}

	else if (command == SERVLIST) {

	}

	else if (command == SQUERY) {

	}

	else if (command == WHO) {
		CommandData argv(cmdLine, { &ICommand::prd_Message });
		if (argv[0].isValid())
			connection->sockwrite(QStringLiteral("%1 %2").arg(WHO)
									.arg(argv[0].toString()));
		else
			connection->sockwrite(WHO);
		return true;
	}

	else if (command == WHOIS) {
		CommandData argv(cmdLine, { &ICommand::prd_AnyWord });
		if (!argv[0].isValid()) {
			print_notEnoughParameters();
			return true;
		}
		connection->sockwrite(QStringLiteral("%1 %2").arg(WHOIS)
									.arg(argv[0].toString()));
		return true;
	}

	else if (command == WHOWAS) {
		CommandData argv(cmdLine, { &ICommand::prd_AnyWord });
		if (!argv[0].isValid()) {
			print_notEnoughParameters();
			return true;
		}
		connection->sockwrite(QStringLiteral("%1 %2").arg(WHOWAS)
									.arg(argv[0].toString()));
		return true;
	}

	else if (command == KILL) {
		CommandData argv(cmdLine, { &ICommand::prd_AnyWord, &ICommand::prd_Message });
		if (!argv[0].isValid()) {
			print_notEnoughParameters();
			return true;
		}
		connection->sockwrite(QStringLiteral("%1 %2 :%3").arg(KILL)
									.arg(argv[0].toString())
									.arg(argv[1].toString()));
		return true;
	}

	else if (command == PING) {

	}

	else if (command == PONG) {

	}

	else if (command == AWAY) {
		CommandData argv(cmdLine, { &ICommand::prd_Message });
		if (!argv[0].isValid()) {
			print_notEnoughParameters();
			return true;
		}
		connection->sockwrite(QStringLiteral("%1 :%2").arg(AWAY)
									.arg(argv[0].toString()));
		return true;
	}

	else if (command == REHASH) {

	}

	else if (command == DIE) {

	}

	else if (command == RESTART) {

	}

	else if (command == SUMMON) {

	}

	else if (command == USERS) {

	}

	else if (command == WALLOPS) {
		CommandData argv(cmdLine, { &ICommand::prd_Message });
		if (!argv[0].isValid()) {
			print_notEnoughParameters();
			return true;
		}
		connection->sockwrite(QStringLiteral("%1 :%2").arg(WALLOPS)
									.arg(argv[0].toString()));
		return true;
	}

	else if (command == USERHOST) {

	}

	else if (command == ISON) {

	}

	return false;
}

bool ICommand::tryParseInternal(const QString& cmdLine)
{
	int spaceidx = cmdLine.indexOf(' ');
	const QString command = cmdLine.mid(0, (spaceidx > -1) ? spaceidx : cmdLine.length()).toUpper();

	using namespace Command::Internal;
	using namespace Command::IRC;
	if (command == CTCP) {
		CommandData argv(cmdLine, { &ICommand::prd_AnyWord, &ICommand::prd_AnyWord, &ICommand::prd_Message });
		if (!argv[0].isValid() || !argv[1].isValid()) {
			print_notEnoughParameters();
			return true;
		}

		QString msg = argv[1].toString();
		if (argv[2].isValid())
			msg += " " + argv[2].toString();
		connection->sockwrite(QStringLiteral("%1 %2 :%3%4%3").arg(PRIVMSG)
									.arg(argv[0].toString()).arg(CTCPflag).arg(msg));
		return true;
	}

	else if (command == ME) {
		IWin* cw = connection->getStatus()->getActiveWindow();
		if (cw->getType() != IWin::Type::Channel && cw->getType() != IWin::Type::Private) {
			print_invalidWindowTypeForCommand();
			return true;
		}

		CommandData argv(cmdLine, { &ICommand::prd_Message });
		if (!argv[0].isValid()) {
			print_notEnoughParameters();
			return true;
		}
		QString target = cw->getButtonText();
		QString mynick = connection->getServer()->getMyNickname();
		QString msg = argv[0].toString();
		if (cw->getType() == IWin::Type::Channel) {
			QString mymodes = connection->server->getMemberByNickname(mynick)->getChannelModes(target);
			if (!mymodes.isEmpty()) {
				QString pf = connection->server->toMemberPrefix(mymodes);
				QChar mode = connection->server->mostSignificantUserModePrefix(pf);
				mynick.prepend(mode);
			}
		}
		connection->sockwrite(QStringLiteral("%1 %2 :%3ACTION %4%3").arg(PRIVMSG)
									.arg(target).arg(CTCPflag).arg(msg));
		cw->print(PrintType::Action, QStringLiteral("%1 %2")
						.arg(mynick)
						.arg(msg));
		return true;
	}

	else if (command == ECHO) {
		CommandData argv(cmdLine, { &ICommand::prd_Message });
		if (!argv[0].isValid()) {
			print_notEnoughParameters();
			return true;
		}
		IWinStatus* status = connection->getStatus();
		status->getMdiManager().currentWindow()->print(PrintType::Normal, argv[0].toString());
		return true;
	}

	return false;
}

void ICommand::print_notEnoughParameters()
{
	connection->status->printToActive(PrintType::ProgramInfo, tr("Not enough parameters."));
}

void ICommand::print_invalidCommandSwitch()
{
	connection->status->printToActive(PrintType::ProgramInfo, tr("Invalid command switch."));
}

void ICommand::print_invalidWindowTypeForCommand()
{
	connection->status->printToActive(PrintType::ProgramInfo, tr("Invalid window type for that command."));
}

QVariant ICommand::prd_AnyWord(int& idx, const QString& line)
{
	QString ret;
	for (; idx < line.count(); ++idx) {
		const QChar c = line[idx];
		if (c == ' ')
			break;
		ret += c;
	}
	if (ret.isEmpty())
		return QVariant();
	return ret;
}

QVariant ICommand::prd_NotSwitch(int& idx, const QString& line)
{
	if (line[idx] == '-' || line[idx] == '+')
		return QVariant();
	return prd_AnyWord(idx, line);
}

QVariant ICommand::prd_Switch(int& idx, const QString& line)
{
	if (line[idx] != '-' && line[idx] != '+')
		return QVariant();
	return prd_AnyWord(idx, line);
}

QVariant ICommand::prd_Message(int& idx, const QString& line)
{
	QString ret = line.mid(idx);
	if (ret.isEmpty())
		return QVariant();
	return ret;
}
