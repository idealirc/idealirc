#ifndef CONFIGMGR_H
#define CONFIGMGR_H

#include "IniFile.h"
#include <QObject>
#include <QString>
#include <QHash>
#include <QVector>
#include <QColor>
#include <optional>

class ConfigMgr : public QObject
{
	Q_OBJECT

public:
	static ConfigMgr& instance();
	void load();
	void save();

	// One getter per section
	QString geometry(const QString& key) const;
	QString connection(const QString& key) const;
	QString common(const QString& key) const;
	QString color(const QString& key) const;
	std::optional<QColor> prefixColor(const QChar prefix) const;
	QHash<QString,QString> color() const;
	QVector<std::pair<QChar,QColor>> prefixColor() const;
	QString logging(const QString& key) const;

	void setGeometry(const QString& key, const QString& value);
	void setConnection(const QString& key, const QString& value);
	void setCommon(const QString& key, const QString& value);
	void setLogging(const QString& key, const QString& value);
	void setColorPalette(const QHash<QString,QString>& palette);
	void setPrefixColorPalette(const QVector<std::pair<QChar,QColor>>& palette);

private:
	ConfigMgr();
	void loadGeometry(IniFile& ini);
	void loadConnection(IniFile& ini);
	void loadCommon(IniFile& ini);
	void loadColor(IniFile& ini);
	void loadPrefixColor(IniFile& ini);
	void loadLogging(IniFile& ini);
	void saveGeometry(IniFile& ini);
	void saveConnection(IniFile& ini);
	void saveCommon(IniFile& ini);
	void saveColor(IniFile& ini);
	void savePrefixColor(IniFile& ini);
	void saveLogging(IniFile& ini);

	QHash<QString,QString> geometryData;
	QHash<QString,QString> connectionData;
	QHash<QString,QString> commonData;
	QHash<QString,QString> colorData;
	QVector<std::pair<QChar,QColor>> prefixColorData;
	QHash<QString,QString> loggingData;

signals:
	void saved();
};

#endif // CONFIGMGR_H
