/*
 * IdealIRC - Internet Relay Chat client
 * Copyright (C) 2019  Tom-Andre Barstad
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "IServer.h"
#include <QDebug>

QStringList IServer::IRCv3Capabilites {
	IRCv3::USERHOST_IN_NAMES,
	IRCv3::MULTI_PREFIX
};

IServer::IServer()
{
	resetAll();
}

void IServer::resetAll()
{
	m_channels.clear();
	m_members.clear();
	m_nickname.clear();
	m_ircv3.clear();
	isupport.reset();

	/*
	 * Even though the lists are being reset, the pointers will
	 * still exist within their respective channels and resets via
	 * the signal memberListClearedForAll().
	 * The pointers will be deleted and re-made when we re-join the given channels.
	 * Closing the channel window(s) will also delete the pointers.
	*/
	m_channels.clear();
	m_members.clear();
	emit memberListClearedForAll();
}


void IServer::ISupport::reset()
{
	chanModesA = "b";
	chanModesB = "k";
	chanModesC = "l";
	chanModesD = "imnpstr";
	chanTypes = "#";
	network.clear();
	prefix = {{'o','@'}, {'v','+'}};
	prefixModeOrder = "ov";
	maxList.clear();
	maxModes = 3;
}

void IServer::parseISupport(const QString& key, const QString& value)
{
	if (key == "CHANMODES") {
		QStringList types = value.split(',');
		isupport.chanModesA = types[0];
		isupport.chanModesB = types[1];
		isupport.chanModesC = types[2];
		isupport.chanModesD = types[3];
	}

	else if (key == "NETWORK") {
		isupport.network = value;
	}

	else if (key == "PREFIX") {
		int prefixStart = value.indexOf(')');
		QString modes = value.mid(1, prefixStart - 1);
		QString prefixes = value.mid(prefixStart + 1);
		isupport.prefixModeOrder = prefixes;
		isupport.prefix.clear();
		for (int i = 0; i < modes.length(); ++i) {
			const QChar mode = modes[i];
			const QChar prefix = prefixes[i];
			isupport.prefix.insert(mode, prefix);
		}
	}

	else if (key == "MAXLIST") {
		isupport.maxList.clear();
		QStringList elems = value.split(',');
		for (const QString& el : elems) {
			QStringList keyval = el.split(':');
			QChar key = keyval[0][0];
			int val = keyval[1].toInt();
			isupport.maxList.insert(key, val);
		}
	}

	else if (key == "MODES") {
		isupport.maxModes = value.toInt();
	}
}

char IServer::channelModeType(QChar mode) const
{
	if (isupport.chanModesA.contains(mode))
		return 'A';
	if (isupport.chanModesB.contains(mode))
		return 'B';
	if (isupport.chanModesC.contains(mode))
		return 'C';
	if (isupport.chanModesD.contains(mode))
		return 'D';
	return '0';
}

std::shared_ptr<IChannel> IServer::addChannel(const QString& channel)
{
	auto existingChannelptr = getChannel(channel);
	if (!existingChannelptr) {
		auto channelptr = std::make_shared<IChannel>(channel);
		m_channels.push_back(channelptr);
		return channelptr;
	}
	else
		return existingChannelptr;
}

void IServer::addMember(const QString& channel, const QString& userinfo)
{
	auto chanEntry = getChannel(channel);
	if (!chanEntry) {
		qWarning() << "Tried to add member" << userinfo << "to channel" << channel << "- but the channel does not exist!";
		return;
	}

	QString modes;
	for (QChar c : userinfo) {
		if (isUserModePrefix(c))
			modes += c;
		else
			break;
	}

	auto member = getMember(userinfo.mid(modes.length()));
	for (QChar c : modes) {
		QChar m = isupport.prefix.key(c, '?');
		member->setChannelMode(channel, m);
	}

	chanEntry->addMember(member);
	emit memberAdded(channel, member);
}

void IServer::addMember(std::shared_ptr<IMember> member)
{
	m_members.push_back(member);
}

void IServer::addMemberList(const QString& channel, const QStringList& memberlist)
{
	auto chanEntry = getChannel(channel);
	if (!chanEntry) {
		qWarning() << "Tried to add a member list to channel" << channel << "- but the channel does not exist!";
		return;
	}

	for (QString userinfo : memberlist) {
		QString prefixes;
		while (isUserModePrefix(userinfo[0])) {
			prefixes += userinfo[0];
			userinfo.remove(0, 1);
		}

		auto member = getMember(userinfo);
		chanEntry->addMember(member);
		for (QChar prefix : prefixes)
			member->setChannelMode(channel, isupport.prefix.key(prefix, '?'));
	}

	emit memberListReloaded(channel);
}

void IServer::delChannel(const QString& channel)
{
	auto channelptr = getChannel(channel);
	if (!channelptr)
		return;

	for (auto member : m_members)
		member->clearChannelMode(channel);
	channelptr->reset();
	m_channels.removeAll(channelptr);
	clearOrphanedMembers();
	emit memberListReloaded(channel);
}

void IServer::delMember(const QString& channel, const QString& userinfo)
{
	auto ptr = getChannel(channel);
	if (!ptr)
		return;

	auto member = getMember(userinfo);
	emit memberRemoved(channel, member);
	member->clearChannelMode(channel);
	ptr->delMember(member);

	clearIfOrphaned(member);
}

int IServer::totalMemberCount() const
{
	return m_members.count();
}

int IServer::memberCount(const QString& channel) const
{
	auto ptr = getChannel(channel);
	if (!ptr)
		return 0;
	return ptr->count();
}

int IServer::channelCount() const
{
	return m_channels.count();
}

bool IServer::hasIRCv3Capability(const QString& capability) const
{
	return m_ircv3.contains(capability, Qt::CaseInsensitive);
}

bool IServer::isChannelType(QChar prefix) const
{
	return isupport.chanTypes.contains(prefix);
}

std::optional<IMember> IServer::getMemberByNickname(const QString& nickname)
{
	for (auto member : m_members) {
		if (member->getNickname() == nickname) {
			IMember memcopy = *member;
			return std::make_optional(memcopy);
		}
	}
	return std::nullopt;
}

bool IServer::updateMemberByNickname(const QString& nickname, const IMember& data)
{
	for (auto member : m_members) {
		if (member->getNickname() == nickname) {
			*member = data;
			emit memberChanged(nickname, member);
			return true;
		}
	}
	return false;
}

std::optional<IChannel> IServer::getChannelByName(const QString& name)
{
	for (auto channel : m_channels) {
		if (channel->getName() == name)
			return *channel;
	}
	return std::nullopt;
}

bool IServer::updateChannelByName(const QString& name, const IChannel& data)
{
	for (auto channel : m_channels) {
		if (channel->getName() == name) {
			*channel = data;
			return true;
		}
	}
	return false;
}

bool IServer::isUserModePrefix(QChar prefix) const
{
	constexpr QChar InvalidPrefix = '0';	// A prefix is a symbol. They always have a letter counterpart.
											// Thus it can never be a number character.

	QChar p = isupport.prefix.key(prefix, InvalidPrefix);
	return p != InvalidPrefix;
}

bool IServer::isUserMode(QChar mode) const
{
	constexpr QChar InvalidMode = '0';	// A mode is a letter. They always have a symbol counterpart.
										// Thus it can never be a number character.

	QChar p = isupport.prefix.value(mode, InvalidMode);
	return p != InvalidMode;
}

QChar IServer::mostSignificantUserModePrefix(QString prefixes) const
{
	QChar mode;
	if (prefixes.isEmpty())
		return mode;

	int lowest = 100;
	for (QChar m : prefixes) {
		int pos = isupport.prefixModeOrder.indexOf(m);
		if (pos < lowest)
			lowest = pos;
	}
	return isupport.prefixModeOrder[lowest];
}

QStringList IServer::getMemberChannels(const QString& nickname) const
{
	QStringList ret;
	for (auto channel : m_channels) {
		if (channel->hasNickname(nickname))
			ret.push_back(channel->getName());
	}
	return ret;
}

QString IServer::toMemberPrefix(const QString& modes) const
{
	QString prefixes;
	for (QChar m : modes)
		prefixes += isupport.prefix.value(m);
	return prefixes;
}

bool IServer::lessThan(const QString& left, const QString& right)
{
	bool leftHasPrefix = isUserModePrefix(left[0]);
	bool rightHasPrefix = isUserModePrefix(right[0]);

	if (!leftHasPrefix && !rightHasPrefix)
		return left.compare(right, Qt::CaseInsensitive) < 0;

	if (leftHasPrefix && !rightHasPrefix)
		return true;

	if (!leftHasPrefix && rightHasPrefix)
		return false;

	// From this point it is given that both nicknames have a prefix.

	if (left[0] == right[0])
		return left.compare(right, Qt::CaseInsensitive) < 0;

	return isupport.prefixModeOrder.indexOf(left[0]) < isupport.prefixModeOrder.indexOf(right[0]);
}

std::shared_ptr<IMember> IServer::getMember(const QString& userinfo)
{
	QString nickname;
	if (userinfo.contains('!'))
		nickname = userinfo.split('!')[0];
	else
		nickname = userinfo;

	for (auto member : m_members) {
		if (*member == nickname)
			return member;
	}

	// If not found, we really want to make a new and insert to list. addMember() only adds to the channel list.
	auto ptr = std::make_shared<IMember>(userinfo);
	m_members.push_back(ptr);
	return ptr;
}

std::shared_ptr<IChannel> IServer::getChannel(const QString& channel) const
{
	for (auto entry : m_channels) {
		if (*entry == channel)
			return entry;
	}
	return nullptr;
}

void IServer::clearOrphanedMembers()
{
	QList<std::shared_ptr<IMember>> copyOfMembers = m_members;
	for (auto mptr : copyOfMembers)
		clearIfOrphaned(mptr);
}

void IServer::clearIfOrphaned(std::shared_ptr<IMember> member)
{
	bool isOrphan = true;
	for (auto cptr : m_channels) {
		isOrphan = !cptr->hasNickname(member->getNickname());
		if (!isOrphan)
			break;
	}
	if (isOrphan)
		m_members.removeOne(member);
}
