/*
 *   IdealIRC - Internet Relay Chat client
 *   Copyright (C) 2019  Tom-Andre Barstad
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License along
 *   with this program; if not, write to the Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * ***
 * This class is from the 0.x version and is quite ready for re-writing.
 * TODO
 * Refactor to use const-references
 * Refactor to use tabs instead of spaces
 * -> In fact, rewrite this entire class. There's so much wrong with it! <-
 * ***
*/

#ifndef INIFILE_H
#define INIFILE_H

#include <QFile>

class IniFile
{
public:
	explicit IniFile(QString filename);
    ~IniFile() { file->deleteLater(); }
	QString value(QString Section, QString Item, QString defaultValue = "");
	QString value(QString Section, int ItemPos, QString defaultValue = "");
	QString section(int SectionPos, QString defaultSectionName = "");
	QString key(QString Section, int ItemPos, QString defaultKey = "");
	bool write(QString Section, QString Item, QString Value);
	int countItems(QString section);
	int countSections();
	bool delSection(QString Section);
	bool delItem(QString Section, QString Item);
	bool sectionExists(QString section);
	bool appendSection(QString Section);
	bool renameSection(QString OldName, QString NewName);

private:
    void clearNewline(char *data);
    QFile *file;

};

#endif // INIFILE_H
