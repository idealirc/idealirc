/*
 * IdealIRC - Internet Relay Chat client
 * Copyright (C) 2019  Tom-Andre Barstad
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "IRCReader.h"
#include "IWin/IWinStatus.h"
#include "IWin/IWinChannel.h"
#include "Commands.h"
#include "Numeric.h"
#include "MdiManager.h"
#include "ConfigMgr.h"
#include "config.h"
#include <QDebug>
#include <QDateTime>

IRCReader::IRCReader(IServer* server, IConnection* connection)
	: server(server)
	, connection(connection)
{
}

void IRCReader::parseLine(const QString& line)
{
	int pastPrefix = 0;

	std::optional<IRCPrefix> prefix = std::nullopt;
	bool messageExplicitlySet = false;
	if (line[0] == ':') {
		pastPrefix = line.indexOf(' ') + 1;
		QString prefixText = line.mid(1, pastPrefix - 2);
		prefix = IRCPrefix(prefixText);
		messageExplicitlySet = true;
	}

	int msgStart = 0;
	QStringList tokens = line.split(' ', QString::SplitBehavior::SkipEmptyParts);
	if (prefix.has_value()) {
		msgStart = tokens[0].length() + 1;
		tokens.removeFirst();
	}

	QStringList params;
	for (const QString& token : tokens) {
		if (token[0] == ':') {
			++msgStart;
			break;
		}

		msgStart += token.length() + 1;
		params.push_back(token);
	}

	QString message;
	if (msgStart > 0) { // Note: a message will never start on position zero, so this is safe.
		message = line.mid(msgStart);
	}

	QString command = params[0];
	params.removeFirst();

	if (message.isEmpty() && !messageExplicitlySet)
		message = params.back();

	bool isNumeric = false;
	command.toInt(&isNumeric);
	if (isNumeric)
		parseNumeric(command, params, message);
	else
		parseCommand(prefix, command, params, message);
}

void IRCReader::parseCommand(const std::optional<IRCPrefix>& prefix, const QString& command, const QStringList& params, const QString& message)
{
	using namespace Command::IRC;
	if (command == NICK) {
		const QString& newNick = message;

		/* We changed nickname */
		if (server->getMyNickname() == prefix->getNickname()) {
			server->m_nickname = newNick; // Changes IServer
			connection->getStatus()->print(PrintType::Nick, tr("Your nickname changed to %1").arg(newNick));
			connection->getStatus()->printToTypes(IWin::Type::Channel, PrintType::Nick, tr("%1 is now known as %2")
																							.arg(prefix->getNickname())
																							.arg(newNick));
		}

		/* Someone else changed nickname */
		else {
			std::optional<IMember> member = server->getMemberByNickname(prefix->getNickname());
			if (member == std::nullopt) {
				qWarning() << "I do not have" << prefix->getNickname() << "registered in my IAL! They changed nickname to" << newNick;
				connection->getStatus()->print(PrintType::Nick, tr("%1 is now known as %2")
																	.arg(prefix->getNickname())
																	.arg(newNick));
			}
			else {
				member->setNickname(newNick);
				server->updateMemberByNickname(prefix->getNickname(), *member);
				connection->getStatus()->printToTypes(IWin::Type::Channel, PrintType::Nick, tr("%1 is now known as %2")
																								.arg(prefix->getNickname())
																								.arg(newNick));
			}

			IWin* subwin = status()->getMdiManager().findWindow(status(), prefix->getNickname());
			if (subwin) {
				subwin->setButtonText(newNick);
				subwin->refreshWindowTitle();
				status()->getMdiManager().renameWindowButton(subwin, newNick);
				subwin->print(PrintType::Nick, tr("%1 is now known as %2")
												.arg(prefix->getNickname())
												.arg(newNick));
			}
		}
	}

	else if (command == MODE) {
		/* Channel mode */
		if (server->isChannelType(params[0][0])) {
			QStringList modeparam = params;
			modeparam.removeFirst();
			QString modemsg = modeparam.join(' ');
			parseChannelMode(params[0], modemsg);

			status()->printTo(params[0], PrintType::Mode, tr("%1 sets mode %2")
									.arg(prefix->toString())
									.arg(modemsg));
		}

		/* Own mode */
		else {
			status()->print(PrintType::Mode, tr("Your mode sets to %1").arg(message));
		}
	}

	else if (command == QUIT) {
		QStringList channels = server->getMemberChannels(prefix->getNickname());
		for (const QString& channel : channels) {
			if (message.isEmpty())
				status()->printTo(channel, PrintType::Quit, tr("Quits: %1 (%2@%3)")
																	.arg(prefix->getNickname())
																	.arg(prefix->getUser())
																	.arg(prefix->getHost()));
			else
				status()->printTo(channel, PrintType::Quit, tr("Quits: %1 (%2@%3) (%4)")
																	.arg(prefix->getNickname())
																	.arg(prefix->getUser())
																	.arg(prefix->getHost())
																	.arg(message));
			server->delMember(channel, prefix->toString());
		}
		IWin* subwin = status()->getMdiManager().findWindow(status(), prefix->getNickname());
		if (subwin) {
			subwin->print(PrintType::Quit, tr("Quits: %1 (%2@%3) (%4)")
												.arg(prefix->getNickname())
												.arg(prefix->getUser())
												.arg(prefix->getHost())
												.arg(message));
		}
	}

	else if (command == JOIN) {
		if (prefix->getNickname() == server->getMyNickname()) {
			IWinChannel* subwin = status()->createChannelWindow(message);
			subwin->print(PrintType::ServerInfo, tr("Now talking in %1").arg(message));
			subwin->refreshWindowTitle();
		}
		else {
			status()->printTo(message, PrintType::Join, tr("Joins: %1 (%2@%3)")
									.arg(prefix->getNickname())
									.arg(prefix->getUser())
									.arg(prefix->getHost()));

			server->addMember(message, prefix->compositeUserinfo());
		}
	}

	else if (command == PART) {
		const QString& channel = params[0];
		if (prefix->getNickname() == server->getMyNickname()) {
			IWinChannel* subwin = static_cast<IWinChannel*>(status()->getMdiManager().findWindow(status(), channel));
			if (subwin)
				subwin->print(PrintType::ServerInfo, tr("You have left %1").arg(channel));

			server->delChannel(channel);
		}
		else {
			status()->printTo(channel, PrintType::Part, tr("Parts: %1 (%2@%3)")
									.arg(prefix->getNickname())
									.arg(prefix->getUser())
									.arg(prefix->getHost()));

			server->delMember(channel, prefix->getNickname());
		}
	}

	else if (command == TOPIC) {
		std::optional<IChannel> channel = server->getChannelByName(params[0]);
		if (channel == std::nullopt) {
			qWarning() << "Received TOPIC change for" << params[0] << "but we don't have the channel in our IAL!";
			if (message.isEmpty())
				status()->print(PrintType::ServerInfo, tr("%1 removed topic from %2")
														.arg(prefix->toString())
														.arg(params[0]));
			else
				status()->print(PrintType::ServerInfo, tr("%1 set topic in %2: %3")
														.arg(prefix->toString())
														.arg(params[0])
														.arg(message));
		}
		else {
			channel->setTopic(message);
			server->updateChannelByName(params[0], *channel);
			IWin* subwin = status()->getMdiManager().findWindow(status(), params[0]);
			subwin->refreshWindowTitle();

			if (message.isEmpty())
				status()->printTo(params[0], PrintType::Topic, tr("%1 removed channel topic")
														.arg(prefix->toString()));
			else
				status()->printTo(params[0], PrintType::Topic, tr("%1 sets topic to: %2")
														.arg(prefix->toString())
														.arg(message));
		}
	}

	else if (command == INVITE) {
		status()->printToActive(PrintType::Invite, tr("%1 invited you to join %2")
														.arg(prefix->toString())
														.arg(message));
		status()->getMdiManager().showTrayInfo(tr("Invitation from %1").arg(prefix->toString()), tr("You were invited to join %1").arg(message));
	}

	else if (command == KICK) {
		const QString& channel = params[0];
		const QString& target = params[1];

		/* We were kicked */
		if (target == server->getMyNickname()) {
			server->delChannel(channel);
			status()->printTo(channel, PrintType::Kick, tr("You were kicked out by %1 (%2)")
															.arg(prefix->toString())
															.arg(message));
			status()->getMdiManager().showTrayInfo(tr("Kicked from %1 by %2").arg(channel).arg(prefix->toString()), IIRCView::stripTags(message));
		}

		/* Someone else were kicked */
		else {
			server->delMember(channel, target);
			status()->printTo(channel, PrintType::Kick, tr("%1 kicked %2 (%3)")
															.arg(prefix->toString())
															.arg(target)
															.arg(message));
		}
	}

	else if (command == PRIVMSG) {
		if (message[0] != CTCPflag)
			printChatMessage(QStringLiteral("<%1> %2"), *prefix, params[0], message, PrintType::Normal);
		else
			parseCTCPRequest(*prefix, params[0], message);
	}

	else if (command == NOTICE && message[0] != CTCPflag) {
		status()->printToActive(PrintType::Notice, QStringLiteral("-%1- %2")
								.arg(prefix->toString())
								.arg(message));
		status()->getMdiManager().showTrayInfo(tr("Notice from %1").arg(prefix->toString()), IIRCView::stripTags(message));
	}

	else if (command == NOTICE && message[0] == CTCPflag) {
		parseCTCPReply(*prefix, message);
	}

	else if (command == KILL) {
		status()->printToActive(PrintType::ServerInfo, tr("You were forcibly disconnected by %1 (%2)")
														.arg(prefix->toString())
														.arg(message));
	}

	else if (command == PING) {
		sockwrite(QString("%1 %2").arg(PONG).arg(message));
	}

	else if (command == PONG) {
		// TODO  maybe use this for a manual lag check, as used in version 0.x
	}

	else if (command == ERROR) {
		status()->print(PrintType::ServerInfo, tr("Error from server: %1").arg(message));
	}

	else if (command == WALLOPS) {
		status()->print(PrintType::Wallops, QStringLiteral("!%1! %2")
												.arg(prefix->toString())
												.arg(message));
	}

	else if (command == Command::IRCv3::CAP) {
		QString subcommand = params[1]; // params[0] would be our nickname (or an asterisk). We don't need that now.
		QStringList capParams = message.split(' ');
		parseIRCv3CAP(subcommand, capParams);
	}

	else {
		QString prefixMsg;
		if (prefix)
			prefixMsg = prefix->toString() + " ";

		QString paramMsg;
		if (!params.isEmpty())
			paramMsg = params.join(' ');

		QString messageMsg;
		if (!message.isEmpty())
			messageMsg = QStringLiteral(" : %1").arg(message);

		status()->print(PrintType::Normal, QStringLiteral("[%1] %2%3%4")
											.arg(command)
											.arg(prefixMsg)
											.arg(paramMsg)
											.arg(messageMsg));
	}
}

void IRCReader::parseNumeric(const QString& numeric, const QStringList& params, const QString& message)
{
	if (numeric == Numeric::RPL_WHOISUSER)
		receivingWhois = true;

	if (receivingWhois && ConfigMgr::instance().common("ShowWhoisActiveWindow") == "1") {
		printResponseToActive(numeric, params, message);
		return;
	}

	if (numeric == Numeric::RPL_ENDOFWHOIS)
		receivingWhois = false;

	else if (numeric == Numeric::RPL_ISUPPORT) {
		parseISupportList(params);
	}

	else if (numeric == Numeric::RPL_NAMREPLY && server->getChannelByName(params[2])) {
		namesList.append( message.split(' ', QString::SplitBehavior::SkipEmptyParts) );
	}

	else if (numeric == Numeric::RPL_ENDOFNAMES) {
		const QString& channel = params[1];
		server->addMemberList(channel, namesList);
		namesList.clear();
	}

	else if (numeric == Numeric::RPL_ENDOFMOTD && server->getMyNickname().isEmpty()) {
		/* Empty nickname at this point indicates successful server registration. */
		if (ConfigMgr::instance().common("RejoinChannelsOnConnect") == "1")
			rejoinChannels();
		connection->dontExpectDisconnect();
		auto hostport = connection->hostPortPair();
		status()->getMdiManager().showTrayInfo(tr("Connected to server"), QStringLiteral("%1:%2").arg(hostport.first).arg(hostport.second));
		server->m_nickname = params[0]; // Changes IServer
		connectingNickname.clear();
		status()->refreshWindowTitle();
	}

	else if (numeric == Numeric::ERR_NOSUCHNICK) {
		QString text = QStringLiteral("%1: %2")
							.arg(params[1])
							.arg(message);
		status()->printToActive(PrintType::Normal, text);
		return;
	}

	else if (numeric == Numeric::RPL_TOPIC) {
		status()->printTo(params[1], PrintType::Topic, QStringLiteral("Topic is: %1").arg(message));
		return;
	}

	else if (numeric == Numeric::RPL_TOPICBY) {
		QString setby = params[2];
		QDateTime dt = QDateTime::fromSecsSinceEpoch(params[3].toInt());
		QString timestamp = dt.toString();
		status()->printTo(params[1], PrintType::Topic, QStringLiteral("Topic set by %1, %2").arg(setby).arg(timestamp));
	}

	else if (numeric == Numeric::ERR_NICKNAMEINUSE && server->getMyNickname().isEmpty()) {
		/* No nickname stored, we're not yet registered with server. */
		ConfigMgr& conf = ConfigMgr::instance();
		QString altNick = conf.connection("AltNickname");
		if (altNick.isEmpty() || connectingNickname == altNick) {
			QString msg;
			if (altNick.isEmpty())
				status()->print(PrintType::ProgramInfo, tr("No alternative nickname provided, provide one."));
			else
				status()->print(PrintType::ProgramInfo, tr("Alternative nickname in use, provide a different one."));

			if (status()->getInputBox().text().isEmpty())
				status()->getInputBox().setText(QStringLiteral("/Nick "));
			else
				status()->print(PrintType::ProgramInfo, tr("Set new nickname by typing: /nick YourNickname"));
		}
		else {
			connectingNickname = altNick;
			sockwrite(QStringLiteral("%1 %2")
						.arg(Command::IRC::NICK)
						.arg(connectingNickname));

		}
	}

	else if (params.count() > 1 && server->isChannelType(params[1][0])) {
		QString joinedParams = params.join(' ');
		qDebug() << "print to" << params[1];
		if (message.isEmpty())
			status()->printTo(params[1], PrintType::Normal, joinedParams);
		else
			status()->printTo(params[1], PrintType::Normal, QStringLiteral("%1: %2").arg(joinedParams).arg(message));
	}

	printResponseToStatus(params, message);
}

void IRCReader::parseIRCv3CAP(const QString& subcommand, const QStringList& params)
{
	if (subcommand == Command::IRCv3::LS) {
		QStringList useCap;
		for (const QString& capability : IServer::IRCv3Capabilites) {
			if (params.contains(capability))
				useCap.push_back(capability);
		}

		if (useCap.isEmpty())
			sockwrite("CAP END");
		else
			sockwrite(QString("CAP REQ :%1").arg(useCap.join(' ')));

		server->setIRCv3Capabilities(useCap);
	}

	else if (subcommand == Command::IRCv3::ACK) {
		sockwrite("CAP END");
	}

	else if (subcommand == Command::IRCv3::NAK) {
		qWarning() << "Server replied NAK on our IRCv3 capabilites, even though it seems it supported them! Resetting and continuing.";
		QStringList empty;
		server->setIRCv3Capabilities(empty);
		sockwrite("CAP END");
	}
}

void IRCReader::parseCTCPRequest(const IRCPrefix& prefix, const QString& target, const QString& message)
{
	QString ctcp = message.mid(1, message.length() - 2);
	int spaceidx = ctcp.indexOf(' ');
	QString cmd = ctcp.mid(0, (spaceidx > -1) ? spaceidx : ctcp.length());
	QString msg = ctcp.mid(cmd.length());
	if (cmd == "ACTION") {
		printChatMessage("%1 %2", prefix, target, msg, PrintType::Action);
		return;
	}
	else if (cmd == "PING") {
		sendCTCPReply(prefix, cmd, msg);
	}
	else if (cmd == "TIME") {
		sendCTCPReply(prefix, cmd, QDateTime::currentDateTime().toString("hh:mm:ss, dddd MMMM d, yyyy"));
	}
	else if (cmd == "VERSION") {
		sendCTCPReply(prefix, cmd, QStringLiteral("IdealIRC %1").arg(VERSION_STRING));
	}
	printChatMessage("[%1 CTCP "+cmd+"] %2", prefix, target, msg, PrintType::CTCP);
}

void IRCReader::parseCTCPReply(const IRCPrefix& prefix, const QString& message)
{
	QString ctcp = message.mid(1, message.length() - 2);
	int spaceidx = ctcp.indexOf(' ');
	QString cmd = ctcp.mid(0, (spaceidx > -1) ? spaceidx : ctcp.length());
	QString msg = ctcp.mid(cmd.length());
	connection->getStatus()->printToActive(PrintType::CTCP, QStringLiteral("[CTCP %1 Reply from %2] %3").arg(cmd).arg(prefix.toString()).arg(msg));
}

void IRCReader::sendCTCPReply(const IRCPrefix& target, const QString& command, const QString& message)
{
	using namespace Command::IRC;
	if (message.isEmpty())
		sockwrite(QStringLiteral("%1 %2 :%3%4%3").arg(NOTICE)
				  .arg(target.toString()).arg(CTCPflag).arg(command));
	else
		sockwrite(QStringLiteral("%1 %2 :%3%4 %5%3").arg(NOTICE)
				  .arg(target.toString()).arg(CTCPflag).arg(command).arg(message));
}


void IRCReader::parseISupportList(const QStringList& params)
{
	for (const QString& param : params) {
		QString key;
		QString value;
		if (!param.contains('='))
			key = param;
		else {
			QStringList keyval = param.split('=');
			key = keyval[0];
			value = keyval[1];
		}
		server->parseISupport(key, value);
	}
}

void IRCReader::parseChannelMode(const QString& channel, const QString& modeMsg)
{
	if (modeMsg[0] != '+' && modeMsg[0] != '-') {
		qWarning() << "Received invalid mode string to parse on" << channel << "- modes are" << modeMsg;
		return;
	}

	QStringList modeparam = modeMsg.split(' ');
	QString modes = modeparam[0];
	modeparam.removeFirst();

	int paramPos = 0;
	QChar action = modes[0];
	for (int i = 1; i < modes.length(); ++i) {
		QChar mode = modes[i];
		QString param;
		if (paramPos < modeparam.length())
			param = modeparam[paramPos];

		char type = server->channelModeType(mode);
		/* A = Mode that adds or removes a nick or address to a list. Always has a parameter. */
		if (type == 'A')
			++paramPos;

		/* B = Mode that changes a setting and always has a parameter. */
		else if (type == 'B')
			++paramPos;

		/* C = Mode that changes a setting and only has a parameter when set. */
		else if (type == 'C') {
			if (action == '+')
				++paramPos;
		}

		/* D = Mode that changes a setting and never has a parameter. */
		else if (type == 'D') {
		}

		/* 0 = (IIRC Internal) This isn't a channel mode but a channel usermode (like o,v) or an unknown mode at all. */
		else if (type == '0') {
			if (!server->isUserMode(mode)) {
				qWarning()	<< "Read an invalid mode set at channel" << channel
							<< "- This _may_ invoke undefined behaviour! Mode are:" << (QString(action) + mode);
				continue;
			}
			++paramPos;

			auto member = server->getMemberByNickname(param); // param is nickname
			if (!member) {
				qWarning() << "Tried to update usermode on" << param << "to" << (QString(action) + mode) << "- but member doesn't exist!";
				continue;
			}
			if (action == '+')
				member->setChannelMode(channel, mode);
			else
				member->delChannelMode(channel, mode);
			server->updateMemberByNickname(param, *member);
		}
	}
}

void IRCReader::rejoinChannels()
{
	auto winlist = status()->getMdiManager().childrenOf(status(), IWin::Type::Channel);
	QStringList targetList;
	for (IWin* win : winlist)
		targetList << win->getButtonText();
	if (targetList.isEmpty())
		return;
	QString targets = targetList.join(',');
	sockwrite(QStringLiteral("%1 %2").arg(Command::IRC::JOIN).arg(targets));
}

QString IRCReader::formatResponse( const QStringList& params, const QString& message)
{
	QStringList printParam = params;
	QString printmsg;
	if (!params.isEmpty()) {
		printParam.removeFirst(); // always our nickname
		printmsg = printParam.join(' ');
	}

	if (!printmsg.isEmpty() && !message.isEmpty())
		printmsg += QStringLiteral(": %1").arg(message);
	else
		printmsg += message;
	return printmsg;
}

void IRCReader::printResponseToStatus(const QStringList& params, const QString& message)
{
	QString printmsg = formatResponse(params, message);
	status()->print(PrintType::Normal, printmsg);
}

void IRCReader::printResponseToActive(const QString& numeric, const QStringList& params, const QString& message)
{
	if (numeric == Numeric::RPL_ENDOFWHOIS)
		receivingWhois = false;

	QString printmsg = formatResponse(params, message);
	status()->printToActive(PrintType::Normal, printmsg);
}

void IRCReader::printChatMessage(const QString& format, const IRCPrefix& prefix, const QString& target, const QString& message, const PrintType ptype)
{
	QString actualTarget = target;
	QString sender = prefix.toString();

	/* Private MSG */
	if (actualTarget == server->getMyNickname()) {
		actualTarget = prefix.getNickname();
		if (ptype != PrintType::CTCP) {
			if (status()->getMdiManager().findWindow(status(), actualTarget) == nullptr)
				status()->createPrivateWindow(actualTarget, false);

			IWin* subwin = status()->getMdiManager().findWindow(status(), actualTarget);
			subwin->refreshWindowTitle();
		}
	}

	/* Channel MSG, and show mode prefix in sender */
	else if (ConfigMgr::instance().common("ShowModeInMessage") == "1") {
		std::optional<IMember> member = server->getMemberByNickname(sender);
		if (member) {
			QString modes = member->getChannelModes(actualTarget);
			if (!modes.isEmpty()) {
				QString prefixes = server->toMemberPrefix(modes);
				QChar mp = server->mostSignificantUserModePrefix(prefixes);
				sender.prepend(mp);
			}
		}
	}

	if (ptype == PrintType::CTCP && server->isChannelType(target[0]))
		sender.append(QStringLiteral(":%1").arg(target));

	const QString printmsg = format.arg(sender).arg(message);

	status()->printTo(target, ptype, printmsg);

	/* Channel MSG */
	if (server->isChannelType(target[0])) {
		if (message.contains(server->getMyNickname(), Qt::CaseInsensitive)) {
			status()->getMdiManager().showTrayInfo(target, IIRCView::stripTags(printmsg));
		}
	}
}

void IRCReader::sockwrite(const QString& line)
{
	connection->sockwrite(line);
}

IWinStatus* IRCReader::status() const
{
	return connection->getStatus();
}
