/*
 * IdealIRC - Internet Relay Chat client
 * Copyright (C) 2019  Tom-Andre Barstad
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "IMember.h"
#include <QStringList>

IMember::IMember(const QString& nickname, const QString& ident, const QString& hostname)
	: m_nickname(nickname)
	, m_ident(ident)
	, m_hostname(hostname)
{
}

IMember::IMember(const QString& userinfo)
{
	if (userinfo.contains('!')) {
		QStringList nickname_identhost = userinfo.split('!');
		QStringList ident_host = nickname_identhost[1].split('@');
		m_nickname = nickname_identhost[0];
		m_ident = ident_host[0];
		m_hostname = ident_host[1];
	}
	else
		m_nickname = userinfo;
}

const QString& IMember::getNickname() const
{
	return m_nickname;
}

const QString& IMember::getIdent() const
{
	return m_ident;
}

const QString& IMember::getHostname() const
{
	return m_hostname;
}

const QString IMember::getCompsite() const
{
	return QString("%1!%2@%3")
			.arg(m_nickname)
			.arg(m_ident)
			.arg(m_hostname);
}

QString IMember::getChannelModes(const QString& channel) const
{
	return m_channelModes.value(channel);
}

void IMember::setChannelMode(const QString& channel, QChar mode)
{
	QString newmodes;
	if (m_channelModes.contains(channel))
		newmodes = m_channelModes[channel];

	if (!newmodes.contains(mode))
		m_channelModes.insert(channel, newmodes+mode);
}

void IMember::delChannelMode(const QString& channel, QChar mode)
{
	if (!m_channelModes.contains(channel))
		return;

	QString newmodes = m_channelModes[channel];
	newmodes.remove(mode);
	if (newmodes.isEmpty())
		m_channelModes.remove(channel);
	else
		m_channelModes.insert(channel, newmodes);
}

bool IMember::hasChannelMode(const QString& channel, QChar mode) const
{
	if (!m_channelModes.contains(channel))
		return false;

	QString modes = m_channelModes[channel];
	return modes.contains(mode);
}

void IMember::clearChannelMode(const QString& channel)
{
	m_channelModes.remove(channel);
}

void IMember::setNickname(const QString& nickname)
{
	m_nickname = nickname;
}

void IMember::setHostname(const QString& hostname)
{
	m_hostname = hostname;
}

bool IMember::operator==(const IMember& other)
{
	return m_nickname.compare(other.m_nickname, Qt::CaseInsensitive) == 0;
}

bool IMember::operator==(const QString& nickname)
{
	return m_nickname.compare(nickname, Qt::CaseInsensitive) == 0;
}

IMember& IMember::operator=(const IMember& other)
{
	m_nickname = other.m_nickname;
	m_ident = other.m_ident;
	m_hostname = other.m_hostname;
	m_channelModes = other.m_channelModes;
	return *this;
}
