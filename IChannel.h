/*
 * IdealIRC - Internet Relay Chat client
 * Copyright (C) 2019  Tom-Andre Barstad
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef ICHANNEL_H
#define ICHANNEL_H

#include "IMember.h"
#include <QString>
#include <QList>
#include <memory>

class IChannel
{
public:
	IChannel(const QString& name);
	const QString& getName() const { return m_name; }
	void addMember(std::shared_ptr<IMember> member, bool notify = true);
	void delMember(std::shared_ptr<IMember> member);
	void setTopic(const QString& topic) { m_topic = topic; }
	const QString& getTopic() const { return m_topic; }
	bool hasNickname(const QString& nickname) const;
	const QList<std::shared_ptr<IMember>>& getMembers() const;
	int count() const { return m_members.count(); }
	void reset();

	bool operator==(const IChannel& other);
	bool operator==(const QString& channelname);

	IChannel& operator=(const IChannel& other);

private:	
	QString m_name;
	QString m_topic;
	QList<std::shared_ptr<IMember>> m_members; //!< List of all members in this channel
};

#endif // ICHANNEL_H
