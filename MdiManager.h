/*
 * IdealIRC - Internet Relay Chat client
 * Copyright (C) 2019  Tom-Andre Barstad
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef MDIMANAGER_H
#define MDIMANAGER_H

#include "IWin/IWin.h"
#include "ButtonbarMgr.h"
#include <QObject>
#include <QToolBar>
#include <QTreeView>
#include <QMdiArea>
#include <QMdiSubWindow>
#include <QSystemTrayIcon>
#include <QList>
#include <QMenu>
#include <memory>

class IWinStatus;

class MdiManager : public QObject
{
	Q_OBJECT

public:
	MdiManager(QMdiArea& mdiArea, QToolBar& buttonBar, QSystemTrayIcon& trayIcon);

	IWin* createSubwindow(IWin* parent, const QString& buttonText, IWin::Type windowType, bool activate = true);
	IWin* currentWindow() const;
	IWinStatus* currentStatus() const;

	IWin* findWindow(IWin* statusParent, const QString& buttonText);
	QMdiSubWindow* toMdiwin(IWin* win);
	QList<QMdiSubWindow*> mdiChildrenOf(const IWin* statusParent, IWin::Type type = IWin::Type::Undefined) const;
	QList<IWin*> childrenOf(const IWin* statusParent, IWin::Type type = IWin::Type::Undefined) const;
	void showTrayInfo(const QString& title, const QString& message);
	void showTrayWarn(const QString& title, const QString& message);
	int connectionsOnlineCount() const;
	void broadcastProgramExit();
	void renameWindowButton(IWin* window, const QString& text);

	void print(IWinStatus* parent, const QString& target, const PrintType ptype, const QString& text);
	void print(const QString& target, const PrintType ptype, const QString& text);
	void printToActive(IWinStatus* parent, const PrintType ptype, const QString& text);
	void printToTypes(IWinStatus* parent, const IWin::Type toType, const PrintType ptype, const QString& text);
	void printToAll(IWinStatus* parent, const PrintType ptype, const QString& text);

private:
	struct ButtonBarEntry {
		explicit ButtonBarEntry(){}
		ButtonBarEntry(QAction* button_, const QString& buttonText, QMdiSubWindow* parent);
		QMdiSubWindow* subwin{ nullptr };
		QAction* button{ nullptr };
		QMenu* menu{ nullptr };
		QAction* menuHead{ nullptr };
		QAction* menuSep{ nullptr };
		QAction* menuClose{ nullptr };
	};
	QMdiArea& m_mdiArea;
	QSystemTrayIcon& trayIcon;
	IWinStatus* m_activeStatus{ nullptr };
	IWin* m_active{ nullptr };
	ButtonbarMgr m_bbMgr;

	void showTray(const QString& title, const QString& message, QSystemTrayIcon::MessageIcon icon);
	void subwinAboutToClose(IWin* who);
	void subwinActivated(QMdiSubWindow *window);

	int nextXY{ 0 };
	int nextXYadjust{ 0 };
	QRect generateSpawnCoordinates();

signals:
	void readyForExit();
	void subwindowSwitched();
	void connectionStateChange();
};

#endif // MDIMANAGER_H
