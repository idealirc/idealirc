# IdealIRC - Internet Relay Chat client
# Copyright (C) 2019  Tom-Andre Barstad
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

QT       += core gui widgets network

TARGET = IdealIRC
TEMPLATE = app

VERSION_MAJOR = 1
VERSION_MINOR = 0
VERSION_PATCH = 0
VERSION_BUILD = 001

# Two different "build types".
# standalone:
# Read config and skeleton from same folder as the executable.
# packaged:
# Used with installers, and reads config and skeleton from a more system-friendly path.
BUILD_TYPE = packaged

QMAKE_SUBSTITUTES += config.h.in
DEFINES += QT_DEPRECATED_WARNINGS
CONFIG += c++17
unix { QMAKE_CXXFLAGS += -std=c++17 }
win32 | win64 { QMAKE_CXXFLAGS += /std:c++17 }

SOURCES += \
		main.cpp \
		IdealIRC.cpp \
		IMember.cpp \
		IConnection/IConnection.cpp \
		IWin/IWin.cpp \
		IChannel.cpp \
		IServer.cpp \
		MdiManager.cpp \
		IWin/IWinStatus.cpp \
		Widgets/IIRCView.cpp \
		IRCReader.cpp \
		IRCPrefix.cpp \
		InputHandler.cpp \
		ICommand.cpp \
		IWin/IWinChannel.cpp \
		IWin/IWinPrivate.cpp \
		IWin/NicklistController.cpp \
		IConfig/IConfig.cpp \
		IConfig/IConfigServers.cpp \
		IConfig/IConfigOptions.cpp \
		IConfig/IConfigLogging.cpp \
		AboutIIRC.cpp \
		IniFile.cpp \
		IConfig/ColorConfig.cpp \
		ConfigMgr.cpp \
		Widgets/ILineEdit.cpp \
		Widgets/IListWidget.cpp \
		IConfig/ServerEditor.cpp \
		IConfig/ServerMgr.cpp \
		IConfig/ServerModel.cpp \
		ButtonbarMgr.cpp \
    ICommand/CommandData.cpp

HEADERS += \
		IdealIRC.h \
		IMember.h \
		IConnection/IConnection.h \
		IWin/IWin.h \
		IChannel.h \
		IServer.h \
		MdiManager.h \
		IWin/IWinStatus.h \
		Widgets/IIRCView.h \
		IRCReader.h \
		IRCPrefix.h \
		Numeric.h \
		Commands.h \
		InputHandler.h \
		ICommand.h \
		IWin/IWinChannel.h \
		IWin/IWinPrivate.h \
		IWin/NicklistController.h \
		IConfig/IConfig.h \
		IConfig/IConfigServers.h \
		IConfig/IConfigOptions.h \
		IConfig/IConfigLogging.h \
		AboutIIRC.h \
		IniFile.h \
		IConfig/ColorConfig.h \
		ConfigMgr.h \
		Widgets/ILineEdit.h \
		Widgets/IListWidget.h \
		IConfig/ServerEditor.h \
		IConfig/ServerMgr.h \
		IConfig/ServerModel.h \
		ButtonbarMgr.h \
    ICommand/CommandData.h

FORMS += \
		IdealIRC.ui \
		IConfig/IConfig.ui \
		IConfig/IConfigServers.ui \
		IConfig/IConfigOptions.ui \
		IConfig/IConfigLogging.ui \
		AboutIIRC.ui \
		IConfig/ServerEditor.ui

RESOURCES += \
		resources.qrc
