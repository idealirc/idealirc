/*
 * IdealIRC - Internet Relay Chat client
 * Copyright (C) 2019  Tom-Andre Barstad
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "IConnection.h"
#include "Commands.h"
#include "IWin/IWinStatus.h"
#include "MdiManager.h"
#include <QDebug>
#include <QHostAddress>
#include <ConfigMgr.h>

IConnection::IConnection(IWinStatus* statusWindow)
	: server(new IServer)
	, reader(new IRCReader(server, this))
	, status(statusWindow)
{
	/* Connections for SSL socket */
	connect(&sslSocket, &QSslSocket::connected,
			this, &IConnection::onConnected);

	connect(&sslSocket, &QSslSocket::disconnected,
			this, &IConnection::onDisconnected);

	connect(&sslSocket, SIGNAL(error(QAbstractSocket::SocketError)),
			this, SLOT(onError(QAbstractSocket::SocketError)));

	connect(&sslSocket, &QSslSocket::readyRead,
			this, &IConnection::onReadyRead);

	connect(&sslSocket, SIGNAL(sslErrors(const QList<QSslError>&)),
			this, SLOT(onSslErrors(const QList<QSslError>&)));

	/* Connections for normal socket */
	connect(&socket, &QTcpSocket::connected,
			this, &IConnection::onConnected);

	connect(&socket, &QTcpSocket::disconnected,
			this, &IConnection::onDisconnected);

	connect(&socket, SIGNAL(error(QAbstractSocket::SocketError)),
			this, SLOT(onError(QAbstractSocket::SocketError)));

	connect(&socket, &QTcpSocket::readyRead,
			this, &IConnection::onReadyRead);
}

IConnection::~IConnection()
{
	server->deleteLater();
	reader->deleteLater();
}

void IConnection::tryConnect(QString host, quint16 port, bool useSSL)
{
	connectionData.emplace(host, port, useSSL);
	reader->connectingNickname = ConfigMgr::instance().connection("Nickname"); // Changes IRCReader

	m_useSSL = useSSL;
	if (useSSL) {
		status->print(PrintType::ProgramInfo, tr("Connecting to %1:%2 (SSL)...").arg(host).arg(port));
		ignoreSSL.clear();
		ConfigMgr& conf = ConfigMgr::instance();
		if (conf.common("SSLSelfsigned") == "1") {
			ignoreSSL.push_back(QSslError::SelfSignedCertificate);
			status->print(PrintType::ProgramInfo, tr("Ignoring SSL Error: Self-signed certificate"));
		}
		if (conf.common("SSLExpired") == "1") {
			ignoreSSL.push_back(QSslError::CertificateExpired);
			status->print(PrintType::ProgramInfo, tr("Ignoring SSL Error: Expired certificate"));
			conf.setCommon("SSLExpired", "0");
		}
		if (conf.common("SSLCNMismatch") == "1") {
			ignoreSSL.push_back(QSslError::HostNameMismatch);
			status->print(PrintType::ProgramInfo, tr("Ignoring SSL Error: Common Name mismatch"));
		}
		sslSocket.ignoreSslErrors(ignoreSSL);
		sslSocket.connectToHostEncrypted(host, port, QIODevice::ReadWrite);
	}
	else {
		status->print(PrintType::ProgramInfo, tr("Connecting to %1:%2...").arg(host).arg(port));
		socket.connectToHost(host, port, QIODevice::ReadWrite);
	}
}

void IConnection::reConnect(QString host, quint16 port, bool useSSL)
{
	reconnectData.emplace(host, port, useSSL);

	if (isOnline())
		quit();
	else if (isConnected())
		close();
	else
		reConnect();
}

void IConnection::quit()
{
	expectDisconnect();
	sockwrite(QStringLiteral("%1 :%2")
			  .arg(Command::IRC::QUIT)
			  .arg(ConfigMgr::instance().common("QuitMessage")));
}

void IConnection::close()
{
	if (m_useSSL)
		sslSocket.close();
	else
		socket.close();
}

void IConnection::sockwrite(const QString& line)
{
	QByteArray data;
	data.append(line + "\r\n");
	if (m_useSSL)
		sslSocket.write(data);
	else
		socket.write(data);
}

bool IConnection::isOnline() const
{
	return !server->getMyNickname().isEmpty();
}

bool IConnection::isConnected() const
{
	return m_isConnected;
}

void IConnection::disconnectForExit()
{
	m_isExiting = true;
	m_expectDisconnect = true;
	quit();
}

void IConnection::expectDisconnect()
{
	m_expectDisconnect = true;
	expectDisconnectTimer.singleShot(10000, [this]{
		m_expectDisconnect = false;
	});
}

void IConnection::dontExpectDisconnect()
{
	m_expectDisconnect = false;
	expectDisconnectTimer.stop();
}

bool IConnection::isExiting() const
{
	return m_isExiting;
}

QPair<QString, quint16> IConnection::hostPortPair() const
{
	if (m_useSSL)
		return { sslSocket.peerAddress().toString(), sslSocket.peerPort() };
	else
		return { socket.peerAddress().toString(), socket.peerPort() };
}

void IConnection::reConnect()
{
	if (!reconnectData.has_value()) {
		qWarning() << "Reconnection was set but no data defined. Ignoring.";
		emit socketStateChange();
		return;
	}

	QString host = reconnectData->host;
	quint16 port = reconnectData->port;
	bool useSSL = reconnectData->useSSL;
	reconnectData.reset();
	tryConnect(host, port, useSSL);
}

void IConnection::onConnected()
{
	if (m_useSSL) {
		qInfo() << "Connected to" << sslSocket.peerAddress() << sslSocket.peerPort();
	}
	else {
		qInfo() << "Connected to" << socket.peerAddress() << socket.peerPort();
	}
	expectDisconnectTimer.stop();
	m_expectDisconnect = true;
	m_isConnected = true;
	emit socketStateChange();
	ConfigMgr& conf = ConfigMgr::instance();
	QString passmsg;

	using namespace Command::IRC;
	if (!conf.connection("Password").isEmpty()) {
		passmsg = QStringLiteral("%1 :%2\r\n")
					.arg(PASS).arg(conf.connection("Password"));
	}

	sockwrite(QStringLiteral("%1 %2\r\n%3%4 :%5\r\n%6 %7 0 * :%8")
				.arg(Command::IRCv3::CAP).arg(Command::IRCv3::LS)
				.arg(passmsg)
				.arg(NICK).arg(reader->connectingNickname)
				.arg(USER).arg(conf.connection("Username")).arg(conf.connection("Realname")));
}

void IConnection::onDisconnected()
{
	if (!m_expectDisconnect)
		reconnectData = connectionData;
	connectionData.reset();

	if (m_useSSL) {
		qInfo() << "Disconnected from" << sslSocket.peerAddress() << sslSocket.peerPort();
		status->getMdiManager().showTrayWarn("Disconnected from server (SSL)", QStringLiteral("%1:%2").arg(sslSocket.peerAddress().toString()).arg(sslSocket.peerPort()));
	}
	else {
		qInfo() << "Disconnected from" << socket.peerAddress() << socket.peerPort();
		status->getMdiManager().showTrayWarn("Disconnected from server", QStringLiteral("%1:%2").arg(socket.peerAddress().toString()).arg(socket.peerPort()));
	}

	m_isConnected = false;
	status->printToAll(PrintType::ProgramInfo, "Disconnected");
	server->resetAll();
	status->refreshWindowTitle();
	emit disconnected();
	emit socketStateChange();

	if (reconnectData.has_value())
		reConnect();

	if (m_isExiting)
		emit readyForExit();
}

void IConnection::onError(QAbstractSocket::SocketError socketError)
{
	if (m_useSSL) {
		if (sslSocket.error() != QAbstractSocket::SslHandshakeFailedError)
			status->print(PrintType::ProgramInfo, sslSocket.errorString());
		emit gotSocketError(socketError, sslSocket.errorString());
	}
	else {
		status->print(PrintType::ProgramInfo, socket.errorString());
		emit gotSocketError(socketError, socket.errorString());
	}
	emit socketStateChange();
}

void IConnection::onReadyRead()
{
	QByteArray data;
	if (m_useSSL)
		data = sslSocket.readAll();
	else
		data = socket.readAll();

	while (!data.isEmpty()) {
		if (!data.contains("\r\n")) {
			lineBuffer += data;
			break;
		}
		else {
			int endpos = data.indexOf("\r\n");
			lineBuffer += data.mid(0, endpos);
			reader->parseLine(lineBuffer);
			lineBuffer.clear();
			data = data.mid(endpos+2);
		}
	}
}

void IConnection::onSslErrors(const QList<QSslError>& errors)
{
	auto isErrorIgnored = [this](QSslError err) {
		for (const QSslError& ie : ignoreSSL)
			if (ie.error() == err.error())
				return true;
		return false;
	};

	bool ignoreCovered = true;
	for (const QSslError& err : errors) {
		status->print(PrintType::ProgramInfo, tr("SSL error: %1").arg(err.errorString()));

		if (ignoreCovered)
			ignoreCovered = isErrorIgnored(err);
	}
	if (ignoreCovered)
		sslSocket.ignoreSslErrors(errors);
}
