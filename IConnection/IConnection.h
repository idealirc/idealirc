/*
 * IdealIRC - Internet Relay Chat client
 * Copyright (C) 2019  Tom-Andre Barstad
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef ICONNECTION_H
#define ICONNECTION_H

#include "IRCReader.h"
#include <QSslSocket>
#include <QTcpSocket>
#include <QTimer>

class IConnection : public QObject
{
	Q_OBJECT
	friend class ICommand;

public:
	IConnection(IWinStatus* statusWindow);
	~IConnection();
	void tryConnect(QString host, quint16 port, bool useSSL = false);
	void reConnect(QString host, quint16 port, bool useSSL = false);
	void quit();
	void close();
	void sockwrite(const QString& line);
	bool isOnline() const;
	bool isConnected() const;
	void disconnectForExit();
	void expectDisconnect();
	void dontExpectDisconnect();
	bool isExiting() const;

	IServer* getServer() const { return server; }
	IRCReader* getReader() const { return reader; }
	IWinStatus* getStatus() const { return status; }
	QPair<QString,quint16> hostPortPair() const;

private:
	struct CnData {
		CnData(const QString& host, quint16 port, bool useSSL)
			: host(host), port(port), useSSL(useSSL) {}
		QString host;
		quint16 port;
		bool useSSL;
	};

	void reConnect();
	void onConnected();
	void onDisconnected();
	void onReadyRead();

	std::optional<CnData> reconnectData;
	std::optional<CnData> connectionData;
	QSslSocket sslSocket;
	QTcpSocket socket;
	QByteArray buffer;
	IServer* server;
	IRCReader* reader;
	IWinStatus* status;
	QString lineBuffer;
	QList<QSslError> ignoreSSL;
	QTimer expectDisconnectTimer;
	bool m_isExiting{ false };
	bool m_useSSL{ false };
	bool m_isConnected{ false };
	bool m_expectDisconnect{ false };

private slots:
	void onSslErrors(const QList<QSslError>& errors);
	void onError(QAbstractSocket::SocketError socketError);

signals:
	void gotSocketError(QAbstractSocket::SocketError socketError, QString errorString);
	void readyForExit();
	void socketStateChange();
	void disconnected();
};

#endif // ICONNECTION_H
