/*
 * IdealIRC - Internet Relay Chat client
 * Copyright (C) 2019  Tom-Andre Barstad
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef IWINCHANNEL_H
#define IWINCHANNEL_H

#include "IWin.h"
#include "IChannel.h"
#include "NicklistController.h"
#include "Widgets/ILineEdit.h"
#include "Widgets/IListWidget.h"
#include <QSplitter>
#include <QVBoxLayout>
#include <QListWidget>
#include <QTableView>
#include <QCloseEvent>
#include <memory>

class IWinStatus;

class IWinChannel : public IWin
{
	Q_OBJECT

public:
	IWinChannel(IWinStatus* statusParent, const QString& channelName);

	bool print(const PrintType ptype, const QString& text) override;
	void sockwrite(const QString& data) override;
	void refreshWindowTitle() override;

	std::shared_ptr<IChannel> getChannelData() { return m_data; }
	void setChannelDataPtr(std::shared_ptr<IChannel> ptr);

private:
	void inputEnter();
	void listboxDoubleclick(QListWidgetItem* item);

	void closeEvent(QCloseEvent* evt) override;

	QSplitter* splitter;
	QVBoxLayout* v_layout;
	IIRCView* view;
	IListWidget* listbox;
	ILineEdit* input;

	NicklistController* nlControl;

	std::shared_ptr<IChannel> m_data;
	IWinStatus* status;

private slots:
	void memberListReloaded(QString channel);
	void memberListCleared();
	void memberListAdd(QString channel, std::shared_ptr<IMember> member);
	void memberListUpdateMember(QString nickname, std::shared_ptr<IMember> member);
	void memberListRemoveMember(QString channel, std::shared_ptr<IMember> member);
};

#endif // IWINCHANNEL_H
