/*
 * IdealIRC - Internet Relay Chat client
 * Copyright (C) 2019  Tom-Andre Barstad
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "IWinPrivate.h"
#include "IWinStatus.h"
#include <ConfigMgr.h>

IWinPrivate::IWinPrivate(IWinStatus* statusParent, const QString& targetName)
	: IWin(IWin::Type::Private, statusParent)
	, status(statusParent)
{
	setButtonText(targetName);

	layout = new QVBoxLayout();
	view = new IIRCView();
	input = new ILineEdit();

	layout->setMargin(0);
	layout->setSpacing(2);
	layout->addWidget(view);
	layout->addWidget(input);
	setLayout(layout);
	setTabOrder(input, view);

	view->setFocusPolicy(Qt::FocusPolicy::NoFocus);

	connect(input, &ILineEdit::returnPressed,
			this, &IWinPrivate::inputEnter);
}

bool IWinPrivate::print(const PrintType ptype, const QString& text)
{
	view->print(ptype, text);
	ConfigMgr& conf = ConfigMgr::instance();
	if (conf.logging("Privates") == "1") {
		QString path = conf.logging("Path");
		if (path.isEmpty())
			return true;
		path += "/" + getButtonText() + ".log";
		QByteArray out;
		out.append(IIRCView::formatType(ptype, text));
		out.append("\n");
		QFile f(path);
		if (!f.open(QIODevice::WriteOnly | QIODevice::Append))
			return true;
		f.write(out);
		f.close();
	}
	return true;
}

void IWinPrivate::sockwrite(const QString& data)
{
	status->sockwrite(data);
}

void IWinPrivate::refreshWindowTitle()
{
	QString target = getButtonText();
	auto ptr = status->getConnection()->getServer()->getMemberByNickname(target);
	if (!ptr)
		setWindowTitle(target);
	else {
		if (ptr->getHostname().isEmpty())
			setWindowTitle(target);
		else
			setWindowTitle(QStringLiteral("%1 (%2@%3)")
							.arg(target)
							.arg(ptr->getIdent())
							.arg(ptr->getHostname()));
	}
}

void IWinPrivate::inputEnter()
{
	QString text = input->text();
	input->clear();

	status->getInputHandler().parse(*this, text);
}
