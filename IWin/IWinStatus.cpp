/*
 * IdealIRC - Internet Relay Chat client
 * Copyright (C) 2019  Tom-Andre Barstad
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "IWinStatus.h"
#include "IWinChannel.h"
#include "MdiManager.h"
#include <QMessageBox>

int IWinStatus::StatusWindowCount = 0;

IWinStatus::IWinStatus(MdiManager& mdiManager, const QString& buttonText)
	: IWin(Type::Status)
	, mdiManager(mdiManager)
	, connection(new IConnection(this))
	, inHndl(connection)
{
	setButtonText(buttonText);
	++StatusWindowCount;

	layout = new QVBoxLayout();
	view = new IIRCView();
	input = new ILineEdit();

	layout->setMargin(0);
	layout->setSpacing(2);
	layout->addWidget(view);
	layout->addWidget(input);
	setLayout(layout);
	setTabOrder(input, view);

	view->setFocusPolicy(Qt::FocusPolicy::NoFocus);

	connect(input, &ILineEdit::returnPressed, this, &IWinStatus::inputEnter);
	connect(connection, &IConnection::socketStateChange, &mdiManager, &MdiManager::connectionStateChange);
}

bool IWinStatus::print(const PrintType ptype, const QString& text)
{
	view->print(ptype, text);
	return true;
}

void IWinStatus::refreshWindowTitle()
{
	if (connection->getServer()->getISupport_Network().isEmpty())
		setWindowTitle(QStringLiteral("Status"));
	else
		setWindowTitle(QStringLiteral("Status (%1)")
					   .arg(connection->getServer()->getISupport_Network()));
}

void IWinStatus::printTo(const QString& target, const PrintType ptype, const QString& text)
{
	mdiManager.print(this, target, ptype, text);
}

void IWinStatus::printToTypes(const IWin::Type toType, const PrintType ptype, const QString& text)
{
	mdiManager.printToTypes(this, toType, ptype, text);
}

void IWinStatus::printToActive(const PrintType ptype, const QString& text)
{
	mdiManager.printToActive(this, ptype, text);
}

void IWinStatus::printToAll(const PrintType ptype, const QString& text)
{
	mdiManager.printToAll(this, ptype, text);
}

void IWinStatus::sockwrite(const QString& data)
{
	connection->sockwrite(data);
}

IWinChannel* IWinStatus::createChannelWindow(const QString& name)
{
	IWin* subwinBase = mdiManager.findWindow(this, name);
	if (!subwinBase)
		subwinBase = mdiManager.createSubwindow(this, name, IWin::Type::Channel);

	IWinChannel* subwin = dynamic_cast<IWinChannel*>(subwinBase);
	auto dataptr = connection->getServer()->addChannel(name);
	subwin->setChannelDataPtr(dataptr);
	return subwin;
}

IWin* IWinStatus::createPrivateWindow(const QString& target, bool setFocus)
{
	IWin* subwinBase = mdiManager.findWindow(this, target);
	if (!subwinBase)
		subwinBase = mdiManager.createSubwindow(this, target, IWin::Type::Private, setFocus);

	return subwinBase;
}

IWin* IWinStatus::getActiveWindow()
{
	if (mdiManager.currentWindow()->getStatusParent() != this)
		return this;
	return mdiManager.currentWindow();
}

QList<IWin*> IWinStatus::subWindows() const
{
	return mdiManager.childrenOf(this);
}

void IWinStatus::closeEvent(QCloseEvent* evt)
{
	if (StatusWindowCount == 1 ||
		(connection->isConnected() && QMessageBox::question(this, tr("Close status window"), tr("This status window has connection. Close?"))
								   == QMessageBox::No)) {
		evt->ignore();
		return;
	}

	if (connection->isConnected()) {
		connect(connection, &IConnection::disconnected, [&](){ closeAllChildren(); mdiManager.toMdiwin(this)->close(); });
		if (connection->isOnline())
			connection->quit();
		else
			connection->close();
		evt->ignore();
		return;
	}

	--StatusWindowCount;

	IWin::closeEvent(evt);
}

void IWinStatus::inputEnter()
{
	QString text = input->text();
	input->clear();

	inHndl.parse(*this, text);
}

void IWinStatus::closeAllChildren()
{
	QList<QMdiSubWindow*> subwinList = mdiManager.mdiChildrenOf(this);
	for (QMdiSubWindow* subwin : subwinList)
		subwin->close();
}
