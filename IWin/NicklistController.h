/*
 * IdealIRC - Internet Relay Chat client
 * Copyright (C) 2019  Tom-Andre Barstad
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef NICKLISTCONTROLLER_H
#define NICKLISTCONTROLLER_H

#include "IServer.h"
#include <QObject>
#include <QListWidget>
#include <memory>

class NicklistController : public QObject
{
	Q_OBJECT

public:
	NicklistController(QListWidget* listbox, IServer* server, std::shared_ptr<IChannel> channel, QObject* parent = nullptr);
	void reload();
	void clear();
	void addMember(std::shared_ptr<IMember> member);
	void updateMember(QString nickname, std::shared_ptr<IMember> member);
	void removeMember(std::shared_ptr<IMember> member);
	void setChannelDataPtr(std::shared_ptr<IChannel> ptr);

private:
	QString makeItemText(std::shared_ptr<IMember> member);
	int findNewItemPosition(const QString& text);
	void updatePrefixColor(QListWidgetItem* item, std::optional<QColor> color);
	void newConfiguration();
	QListWidget* m_listbox;
	IServer* m_server;
	std::shared_ptr<IChannel> m_channel;
	QStringList m_items;
};

#endif // NICKLISTCONTROLLER_H
