/*
 * IdealIRC - Internet Relay Chat client
 * Copyright (C) 2019  Tom-Andre Barstad
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef IWIN_H
#define IWIN_H

#include <QWidget>
#include <unordered_map>
#include <QCloseEvent>
#include <QShowEvent>
#include "Widgets/IIRCView.h" // PrintType comes from here

class IWin : public QWidget
{
	Q_OBJECT

public:
	enum class Type
	{
		Undefined,
		Status,
		Channel,
		Private,
		Custom
	};
	static const QString InvalidWindowTypeString;
	static std::unordered_map<Type,QString> TypeString;

	Type getType() const;
	IWin* getParent() const { return m_parent; }
	IWin* getStatusParent();

	virtual bool print(const PrintType ptype, const QString& text) = 0;
	virtual void sockwrite(const QString& data) = 0;
	virtual void refreshWindowTitle() = 0;

	const QString& getButtonText() const;
	void setButtonText(const QString& text);
	bool operator==(const IWin& other);

protected:
	IWin(Type type, IWin* parentWindow = nullptr);
	void closeEvent(QCloseEvent *) override;

private:
	Type m_type;
	QString m_buttonText;
	IWin* m_parent{ nullptr };
	bool m_firstShow{ true };

signals:
	void aboutToClose(IWin* who);
};

#endif // IWIN_H
