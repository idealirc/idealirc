/*
 * IdealIRC - Internet Relay Chat client
 * Copyright (C) 2019  Tom-Andre Barstad
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "IWin.h"
#include <QDebug>

const QString IWin::InvalidWindowTypeString { "Invalid" };

std::unordered_map<IWin::Type,QString> IWin::TypeString = {
	{ IWin::Type::Status, "Status" },
	{ IWin::Type::Channel, "Channel" },
	{ IWin::Type::Private, "Private" },
	{ IWin::Type::Custom, "Custom" }
};

IWin::Type IWin::getType() const
{
	return m_type;
}

IWin* IWin::getStatusParent()
{
	return (m_type == Type::Status) ? this : m_parent;
}

const QString& IWin::getButtonText() const
{
	return m_buttonText;
}

void IWin::setButtonText(const QString& text)
{
	m_buttonText = text;
}

IWin::IWin(Type type, IWin* parentWindow)
	: m_type(type)
	, m_parent(parentWindow)
{

}

void IWin::closeEvent(QCloseEvent*)
{
	emit aboutToClose(this);
}
