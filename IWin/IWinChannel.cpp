/*
 * IdealIRC - Internet Relay Chat client
 * Copyright (C) 2019  Tom-Andre Barstad
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "IWinChannel.h"
#include "IWinStatus.h"
#include "MdiManager.h"
#include "Commands.h"
#include <ConfigMgr.h>
#include <QHeaderView>

IWinChannel::IWinChannel(IWinStatus* statusParent, const QString& channelName)
	: IWin(IWin::Type::Channel, statusParent)
	, m_data(statusParent->getConnection()->getServer()->addChannel(channelName))
	, status(statusParent)
{
	setButtonText(channelName);

	splitter = new QSplitter();
	v_layout = new QVBoxLayout();
	view = new IIRCView();
	listbox = new IListWidget();
	input = new ILineEdit();

	v_layout->setMargin(0);
	v_layout->setSpacing(2);

	splitter->addWidget(view);
	splitter->addWidget(listbox);
	v_layout->addWidget(splitter);
	v_layout->addWidget(input);
	setLayout(v_layout);
	setTabOrder(input, view);
	setTabOrder(view, listbox);
	splitter->setStretchFactor(0, 8);
	splitter->setStretchFactor(1, 1);

	view->setFocusPolicy(Qt::FocusPolicy::NoFocus);
	listbox->setFocusPolicy(Qt::FocusPolicy::NoFocus);

	IServer* server = statusParent->getConnection()->getServer();
	nlControl = new NicklistController(listbox, server, m_data, this);
	connect(server, &IServer::memberListReloaded,
			this, &IWinChannel::memberListReloaded);

	connect(server, &IServer::memberListClearedForAll,
			this, &IWinChannel::memberListCleared);

	connect(server, &IServer::memberAdded,
			this, &IWinChannel::memberListAdd);

	connect(server, &IServer::memberChanged,
			this, &IWinChannel::memberListUpdateMember);

	connect(server, &IServer::memberRemoved,
			this, &IWinChannel::memberListRemoveMember);

	connect(input, &ILineEdit::returnPressed,
			this, &IWinChannel::inputEnter);

	connect(listbox, &IListWidget::itemDoubleClicked,
			this, &IWinChannel::listboxDoubleclick);
}

bool IWinChannel::print(const PrintType ptype, const QString& text)
{
	view->print(ptype, text);
	ConfigMgr& conf = ConfigMgr::instance();
	if (conf.logging("Channels") == "1") {
		QString path = conf.logging("Path");
		if (path.isEmpty())
			return true;
		path += "/" + getButtonText() + ".log";
		QByteArray out;
		out.append(IIRCView::formatType(ptype, text));
		out.append("\n");
		QFile f(path);
		if (!f.open(QIODevice::WriteOnly | QIODevice::Append))
			return true;
		f.write(out);
		f.close();
	}
	return true;
}

void IWinChannel::sockwrite(const QString& data)
{
	status->sockwrite(data);
}

void IWinChannel::refreshWindowTitle()
{
	QString title;
	if (m_data->getTopic().isEmpty())
		title = getButtonText();
	else
		title = QStringLiteral("%1 (%2)")
					.arg(getButtonText())
					.arg(m_data->getTopic());

	setWindowTitle(title);
}

void IWinChannel::setChannelDataPtr(std::shared_ptr<IChannel> ptr)
{
	m_data = ptr;
	nlControl->setChannelDataPtr(ptr);
}

void IWinChannel::inputEnter()
{
	QString text = input->text();
	input->clear();
	status->getInputHandler().parse(*this, text);
}

void IWinChannel::listboxDoubleclick(QListWidgetItem* item)
{
	if (!item)
		return;

	QString nickname = item->text();
	if (status->getConnection()->getServer()->isUserModePrefix(nickname[0]))
		nickname = nickname.mid(1);

	input->setFocus();
	IWin* subwin = status->createPrivateWindow(nickname);
	subwin->refreshWindowTitle();
}

void IWinChannel::closeEvent(QCloseEvent*evt)
{
	sockwrite(QStringLiteral("%1 %2 :")
			  .arg(Command::IRC::PART)
			  .arg(getButtonText()));
	IWin::closeEvent(evt);
}

void IWinChannel::memberListReloaded(QString channel)
{
	if (channel != getButtonText()) return;
	nlControl->reload();
}

void IWinChannel::memberListCleared()
{
	nlControl->clear();
}

void IWinChannel::memberListAdd(QString channel, std::shared_ptr<IMember> member)
{
	if (channel != getButtonText()) return;
	nlControl->addMember(member);
}

void IWinChannel::memberListUpdateMember(QString nickname, std::shared_ptr<IMember> member)
{
	nlControl->updateMember(nickname, member);
}

void IWinChannel::memberListRemoveMember(QString channel, std::shared_ptr<IMember> member)
{
	if (channel != getButtonText()) return;
	nlControl->removeMember(member);
}
