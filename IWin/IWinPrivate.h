/*
 * IdealIRC - Internet Relay Chat client
 * Copyright (C) 2019  Tom-Andre Barstad
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef IWINPRIVATE_H
#define IWINPRIVATE_H

#include "IWin.h"
#include "Widgets/ILineEdit.h"
#include <QLineEdit>
#include <QVBoxLayout>

class IWinStatus;

class IWinPrivate : public IWin
{
	Q_OBJECT

public:
	IWinPrivate(IWinStatus* statusParent, const QString& targetName);
	bool print(const PrintType ptype, const QString& text) override;
	void sockwrite(const QString& data) override;
	void refreshWindowTitle() override;

private:
	void inputEnter();

	QVBoxLayout* layout;
	IIRCView* view;
	ILineEdit* input;

	IWinStatus* status;
};

#endif // IWINPRIVATE_H
