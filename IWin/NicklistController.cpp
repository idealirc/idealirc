/*
 * IdealIRC - Internet Relay Chat client
 * Copyright (C) 2019  Tom-Andre Barstad
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "NicklistController.h"
#include "ConfigMgr.h"
#include <algorithm>
#include <QDebug>

NicklistController::NicklistController(QListWidget* listbox, IServer* server, std::shared_ptr<IChannel> channel, QObject* parent)
	: QObject(parent)
	, m_listbox(listbox)
	, m_server(server)
	, m_channel(channel)
{
	ConfigMgr& conf = ConfigMgr::instance();
	connect(&conf, &ConfigMgr::saved, this, &NicklistController::newConfiguration);
}

void NicklistController::reload()
{
	m_listbox->clear();
	m_items.clear();

	for (const auto& mem : m_channel->getMembers())
		m_items << makeItemText(mem);

	std::sort(m_items.begin(), m_items.end(), [this](const QString& left, const QString& right){
		return m_server->lessThan(left, right);
	});

	m_listbox->addItems(m_items);
	newConfiguration();
}

void NicklistController::clear()
{
	m_listbox->clear();
}

void NicklistController::addMember(std::shared_ptr<IMember> member)
{
	QString newItem = makeItemText(member);
	int pos = findNewItemPosition(newItem);
	m_items.insert(pos, newItem);
	m_listbox->insertItem(pos, newItem);

	QListWidgetItem* item = m_listbox->item(pos);
	updatePrefixColor(item, std::nullopt);
}

void NicklistController::updateMember(QString nickname, std::shared_ptr<IMember> member)
{
	// Find position of the item in the list.
	// TODO  use this loop also to find new item position.
	int currPos = -1;
	for (int i = 0; i < m_items.count(); ++i) {
		QString item = m_items[i];
		if (m_server->isUserModePrefix(item[0]))
			item = item.mid(1);
		if (item == nickname) {
			currPos = i;
			break;
		}
	}

	// We don't have this nickname in this channel, and it is safe to ignore it silently.
	// This is because when a nickname changes, that user might not be on all channels we're on.
	if (currPos == -1) return;

	QListWidgetItem* lbitem = m_listbox->item(currPos);
	bool isSelected = lbitem->isSelected();
	m_items.removeAt(currPos);
	m_listbox->removeItemWidget(lbitem);
	delete lbitem;
	QString newItemText = makeItemText(member);
	int newPos = findNewItemPosition(newItemText);
	lbitem = new QListWidgetItem(newItemText);
	m_listbox->insertItem(newPos, lbitem);
	m_items.insert(newPos, newItemText);
	lbitem->setSelected(isSelected);
	updatePrefixColor(lbitem, std::nullopt);
}

void NicklistController::removeMember(std::shared_ptr<IMember> member)
{
	QString itemText = makeItemText(member);
	int pos = 0;
	int delpos = -1;
	while (pos < m_items.count()) {
		const QString& item = m_items[pos];
		if (item == itemText) {
			delpos = pos;
			break;
		}
		++pos;
	}

	if (delpos == -1) {
		qWarning() << "Got request to delete" << itemText << "from" << m_channel->getName() << "but it was not found!";
		return;
	}

	m_items.removeAt(delpos);
	QListWidgetItem* lbitem = m_listbox->item(delpos);
	m_listbox->removeItemWidget(lbitem);
	delete lbitem;
}

void NicklistController::setChannelDataPtr(std::shared_ptr<IChannel> ptr)
{
	m_channel = ptr;
}

QString NicklistController::makeItemText(std::shared_ptr<IMember> member)
{
	QString item;
	const QString& cname = m_channel->getName();
	QString modes = member->getChannelModes(cname);
	if (!modes.isEmpty()) {
		QString prefixes = m_server->toMemberPrefix(modes);
		item += m_server->mostSignificantUserModePrefix(prefixes);
	}
	item += member->getNickname();
	return item;
}

int NicklistController::findNewItemPosition(const QString& text)
{
	int pos = 0;
	while (pos < m_items.count()) {
		const QString& item = m_items[pos];
		if (m_server->lessThan(text, item))
			break;

		++pos;
	}

	return pos;
}

void NicklistController::updatePrefixColor(QListWidgetItem* item, std::optional<QColor> color)
{
	if (!color && !m_server->isUserModePrefix(item->text()[0]))
		return;

	ConfigMgr& conf = ConfigMgr::instance();
	if (!color)  {
		color = conf.prefixColor(item->text()[0]);
		if (!color) return;
	}
	item->setTextColor(*color);
}

void NicklistController::newConfiguration()
{
	ConfigMgr& conf = ConfigMgr::instance();
	for (int i = 0; i < m_listbox->count(); ++i) {
		updatePrefixColor(m_listbox->item(i) , QColor(conf.color("ListboxForeground")));
		updatePrefixColor(m_listbox->item(i) , std::nullopt);
	}
}
