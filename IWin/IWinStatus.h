/*
 * IdealIRC - Internet Relay Chat client
 * Copyright (C) 2019  Tom-Andre Barstad
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef IWINSTATUS_H
#define IWINSTATUS_H

#include "IWin.h"
#include "IConnection/IConnection.h"
#include "IRCReader.h"
#include "IServer.h"
#include "InputHandler.h"
#include "Widgets/ILineEdit.h"
#include <QLineEdit>
#include <QVBoxLayout>

class MdiManager;
class IWinChannel;

class IWinStatus : public IWin
{
	Q_OBJECT

public:
	IWinStatus(MdiManager& mdiManager, const QString& buttonText);

	bool print(const PrintType ptype, const QString& text) override;
	void refreshWindowTitle() override;
	void printTo(const QString& target, const PrintType ptype, const QString& text);
	void printToTypes(const IWin::Type toType, const PrintType ptype, const QString& text);
	void printToActive(const PrintType ptype, const QString& text); // TODO
	void printToAll(const PrintType ptype, const QString& text);
	void sockwrite(const QString& data) override;
	IWinChannel* createChannelWindow(const QString& name);
	IWin* createPrivateWindow(const QString& target, bool setFocus = true);
	IWin* getActiveWindow();
	QList<IWin*> subWindows() const;

	IConnection* getConnection() { return connection; }
	InputHandler& getInputHandler() { return inHndl; }
	MdiManager& getMdiManager() { return mdiManager; }
	ILineEdit& getInputBox() { return *input; }

private:
	void closeEvent(QCloseEvent* evt) override;
	void inputEnter();
	void closeAllChildren();

	static int StatusWindowCount;
	QVBoxLayout* layout;
	IIRCView* view;
	ILineEdit* input;

	MdiManager& mdiManager;
	IConnection* connection;
	InputHandler inHndl;
};

#endif // IWINSTATUS_H
