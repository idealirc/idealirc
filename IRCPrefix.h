/*
 * IdealIRC - Internet Relay Chat client
 * Copyright (C) 2019  Tom-Andre Barstad
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef IRCPREFIX_H
#define IRCPREFIX_H

#include <QString>

class IRCPrefix
{
public:
	IRCPrefix(const QString& prefix);
	QString toString() const; //!< Returns either a servername or nickname.

	const QString& getServername() const;
	const QString& getNickname() const;
	const QString& getUser() const;
	const QString& getHost() const;

	QString compositeUserinfo() const;

	enum class Type
	{
		server,
		user
	};

	Type getType() const;

private:
	// Used for :irc.server.name prefix
	QString m_servername;

	/* Used for :nickname!user@host prefix */
	QString m_nickname;
	QString m_user;
	QString m_host;

	Type m_type;
};

#endif // IRCPREFIX_H
